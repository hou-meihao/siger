- 收到 稚辉君 的夸克开发板。
- 先来个数字孪生
  1. 包装 开发板 相对比例 缩略图
  2. 包装盒 四面图
  3. 高清封面
  4. 高清芯片以及IC文字局部放大

<p><img width="706px" src="https://images.gitee.com/uploads/images/2022/0224/003022_2f90fdc5_5631341.jpeg" title="Use quark to build your creative universe."></p>

1. 包装 开发板 相对比例 缩略图

   <p><img width="12.9%" src="https://images.gitee.com/uploads/images/2022/0223/143225_3c297a35_5631341.jpeg"> <img width="12.9%" src="https://images.gitee.com/uploads/images/2022/0223/143443_f11d1aed_5631341.jpeg"> <img width="12.9%" src="https://images.gitee.com/uploads/images/2022/0223/143639_ad6dcd1a_5631341.jpeg"> <img width="12.9%" src="https://images.gitee.com/uploads/images/2022/0223/143706_60b2cc00_5631341.jpeg"> <img width="12.9%" src="https://images.gitee.com/uploads/images/2022/0223/143729_c7317f57_5631341.jpeg"> <img width="12.9%" src="https://images.gitee.com/uploads/images/2022/0223/143756_919a2354_5631341.jpeg"> <img width="12.9%" src="https://images.gitee.com/uploads/images/2022/0223/143825_ccd578f7_5631341.jpeg"></p>

2. 包装盒 四面图

   <p><img width="19%" src="https://images.gitee.com/uploads/images/2022/0223/143903_f33e014e_5631341.jpeg"> <img width="19%" src="https://images.gitee.com/uploads/images/2022/0223/143927_59b6827a_5631341.jpeg"> <img width="19%" src="https://images.gitee.com/uploads/images/2022/0223/144318_405fee5b_5631341.jpeg"> <img width="19%" src="https://images.gitee.com/uploads/images/2022/0223/144337_5a393740_5631341.jpeg"> <img width="19%" src="https://images.gitee.com/uploads/images/2022/0223/144351_0df4c99d_5631341.jpeg"></p>

3. 高清封面

   <p><img width="19%" src="https://images.gitee.com/uploads/images/2022/0223/144417_9fae2b41_5631341.jpeg"> <img width="19%" src="https://images.gitee.com/uploads/images/2022/0223/144429_98049c21_5631341.jpeg"> <img width="19%" src="https://images.gitee.com/uploads/images/2022/0223/145044_9ae4c41c_5631341.jpeg"> <img width="19%" src="https://images.gitee.com/uploads/images/2022/0223/145312_d52383ba_5631341.jpeg"></p>

   - QUANTUM
   - Tiny Linux Development Kit
   - With SoM and Expansion Board
   - 量子计划迷你 Linux 开发板套件
   - 114992462
   - SEED STUDIO MOB210820010
   - Innovate with China 08/24/21
   - [EU] eVatmaster Consulting GmbH
   - Bettinastr. 30,60325 Frankfurt am Main,Germany
   - [REP] contact@evatmaster.com
   - QUANTUM Tiny Board
   - QUARK
   - Use quark to build your
   - creative universe
   - seeed studio The IoT Hardware Enabler
   - ATTENTION
   - !Best to keep the shield
   - away from file.

4. 高清芯片以及IC文字局部放大

   <p><img width="19%" src="https://images.gitee.com/uploads/images/2022/0223/145658_ec3525bd_5631341.jpeg"> <img width="19%" src="https://images.gitee.com/uploads/images/2022/0223/145748_7fe11044_5631341.jpeg"></p>

   - Quark-N
   - 8K209
   - GND 3.3V
   - IMU
   - RIL872380
   - LJCJ8HI GL12
   - ALL WINNER
   - TECH
   - H3
   - M5068BA 95K4
   - OYP15 [M]
   - D9SHD

### [重建夸克石榴派](https://gitee.com/shiliupi/Dummy-Robot/issues/I4UTYF#note_8873829_link)

![输入图片说明](https://images.gitee.com/uploads/images/2021/1029/030918_62545930_5631341.gif "在这里输入图片标题")

15. **[袁老师毕业啦！](https://gitee.com/shiliupi/Project-Quantum/issues/I4FZ68#note_7302482_link)** 

    - [`quark-n-21-1-11.001.lucky-9-LNMP-ruxuan-8-CCB.img` 封贴啦。:pray:](https://gitee.com/shiliupi/Project-Quantum/issues/I4FZ68#note_7304683_link) 

```
sudo dd if=/dev/mmcblk0 of=/dev/mmcblk1 bs=512 count="30277633" & sudo watch -n 5 pkill -USR1 ^dd$
```

> 重温了 Quark 重启画面，桔黄色的小企鹅，这可是 SIGer 封面 BANNER 的一部分呢。找出 发给志辉的 SD 卡镜象，WINIMG 之，上电重启。然后 DD 系统到板子上。关机拔掉SD 卡，再开机，[快快乐乐地](https://images.gitee.com/uploads/images/2022/0225/153627_e06ab59d_5631341.png)完成了 CLONE 夸克石榴派。

### 重温共赴星海的豪情 :pray: 

[![输入图片说明](https://images.gitee.com/uploads/images/2021/1207/131521_9ebe95e8_5631341.png)](第15期%20量子计划%20“稚辉君”%20不负青丝，向善而行.md) [![输入图片说明](https://images.gitee.com/uploads/images/2021/1207/131650_b4928408_5631341.png)](少年/26th%20夸克石榴派实现少年梦.md)

《[“ **稚辉君** ” 不负青丝，向善而行！](第15期%20量子计划%20“稚辉君”%20不负青丝，向善而行.md)》 #15th [#26th](少年/26th%20夸克石榴派实现少年梦.md)  

《[宇宙II](https://gitee.com/flame-ai/siger/blob/master/%E7%AC%AC8%E6%9C%9F%20%E6%98%9F%E8%BE%B0%E5%A4%A7%E6%B5%B7II%20-%20%E5%AE%87%E5%AE%99.md#%E4%BF%9D%E6%8C%81%E7%83%AD%E7%88%B1%E5%A5%94%E8%B5%B4%E6%98%9F%E6%B5%B7%E7%AC%94%E8%AE%B0)》[保持热爱，奔赴星海！](https://gitee.com/link?target=https%3A%2F%2Fwww.bilibili.com%2Fvideo%2FBV1cu411U7Z1)【[笔记](https://gitee.com/shiliupi/Dummy-Robot/issues/I4P6BO)】

<p><img src="https://images.gitee.com/uploads/images/2022/0104/023654_eedb5d31_5631341.png" width="199px"> <img src="https://images.gitee.com/uploads/images/2022/0104/020822_789e76b3_5631341.png" width="199px"> <img src="https://images.gitee.com/uploads/images/2022/0104/012615_f7690081_5631341.png" width="199px"></p>

### 追随 稚辉君 成长的脚步，天才少年前传

B站大鸽子火爆前一直在勤勤恳恳地码字，从他的个站，到知乎新知答主，直到众粉期待，稚辉君才将原本的 视频图床 B站 变成主战场，而转战华为，入选天才少年，也是 B站大火之后啦，甚至感觉 华为蹭流量 :D 

因为石榴派 结缘 稚辉君，同学们也有福了，这不为了鼓励同学们，将不挑食的优良传统继承下去，他特地将自己撸过的板子一并赠送给了同学们，并附上了曾经的字码。嘿嘿，要想撸 夸克，可以先过 K210 这关。任何创造都不是凭空想象的，都是一个个肝代码撸板子的深夜累积的。（还有一个捷径，我结缘稚辉君一周年的日子，SIGer 上踩个脚印，夸克漂流给你 :sunglasses: ）

![输入图片说明](https://images.gitee.com/uploads/images/2022/0225/092045_ad7aad78_5631341.jpeg "v2-3c2d39e0e26c8c8e8102f794aa8d1328_r.jpg")

# 嵌入式AI从入门到放肆【K210篇】-- 硬件与环境

[稚晖](https://www.zhihu.com/people/zhi-hui-64-54)
- 2021 新知答主
- 1,069 人赞同了该文章

### 前言

 **关注我的同学都知道，在搞项目方面我一点也不挑食，有啥玩啥，擅于给自己找麻烦。** 

由于个人本职工作是搞AI的，在不久之前的一个项目中，我用STM32制作了一个小模块，探讨了一下“推理一个实用神经网络最低需要什么样的算力”这个问题。项目的演示视频如下：

[基于Cortex-M的M4CNN模块](https://video.zhihu.com/video/1154763650249121792)

在那个项目中，我成功在 **主频只有不到100MHz，sram只有32K** 的单片机上推理了一个自己训练的CNN-SLR网络模型，效果甚至还不错。

而这一次我准备来一波反向操作：尽可能用上算力最强的MCU，来看看在上面能跑多复杂的网络， **因垂丝汀** 。

> 注意，我这里指明了使用的是 **MCU** ，所以移动设备上的各种SoC就不在讨论范畴了，要说原因的话，是因为 **移动端SoC之间的battle过于激烈** ，这个讨论留作后话，在这一篇里面就暂时按下不表了；另一方面由于二者不在一个价位段，没有可比性。

为了引出本篇要使用的MCU型号，首先我们来讨论一下嵌入式处理器的一个守恒定律，也即  **功耗-价格-性能**  不可兼得，如下图：

![输入图片说明](https://images.gitee.com/uploads/images/2022/0225/114523_991d1ef0_5631341.png "屏幕截图.png")

 _“天下没有免费的处理器”_ 

稍微解释一下上图的话就是，由于物理定律限制我们可以认为：在其他条件一定的情况下，性能和功耗肯定成正比，更强的性能意味着更大的功耗也就会产生更多的热量；而要想提升性能的同时还能保证功耗不变甚至更低的话，就只能改进芯片的架构或者改进制程工艺，这又会带来成本的提高。因而根据不同应用方向，我们可以选择兼顾其中两者的一些硬件平台：

-  **成本低，性能弱，功耗也低：** 代表是各种单片机如STM32/AVR等。
-  **成本低，性能强，但制程不高易发热：** 代表是各种低端SoC比如全志的H系列。
-  **性能强，制程先进，但价格十分不友好，有钱还不一定给卖系列：** 代表是现在智能手机上使用的各种旗舰SoC，比如高通骁龙/海思麒麟/苹果A系。

那难道真的就没有三者兼得的芯片平台了吗？

其实还真有，本篇的主角 **K210** 就勉强算是一个，这也是我这次选择折腾这个项目的主要原因，接下来请听我仔细分析~

### K210是个啥

K210是由一家叫做嘉楠的曾经做 **挖矿芯片** 的公司在去年推出的一款MCU，其特色在于芯片架构中包含了一个自研的神经网络硬件加速器KPU，可以高性能地进行卷积神经网络运算。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0225/114712_481088ef_5631341.png "屏幕截图.png")

可不要以为MCU的性能就一定比不上高端SoC，至少在AI计算方面，K210的算力其实是相当可观的。根据嘉楠官方的描述，K210的KPU算力有0.8 _TOPS_  ，作为对比，拥有128个CUDA单元GPU的英伟达Jetson Nano的算力是0.47 _TFLOPS_  ；而最新的树莓派4只有不到0.1 _TFLOPS_  。

当然了，这个性能跟某些旗舰级别的SoC还是有差距的：A76级别的CPU本身就已经很变态，更何况旗舰SoC上面都会搭载用于AI加速的硬件用于异构运算，比如高通的Hexagon DSP、苹果的Neural Engine、华为的达芬奇架构NPU等等，这些NPU在某些应用下甚至能达到与数百W功耗的桌面级GPU接近的算力（顺便提一下GTX1080Ti的双精度浮点算力是11.3  _TFLOPS_  ），可以说是丧心病狂了。

> 值得注意的是，芯片的算力不一定和模型推理速度成正比，嵌入式AI的另一个核心是inference框架。对于CPU架构来说，是否使用SIMD（ARM从v7开始就支持NEON指令了）、是否使用多核多线程、是否有高效的卷积实现方式、是否有做汇编优化等等都会极大影响模型运行速度；而对DSP/NPU等硬件架构来说，是否对模型进行量化推理、量化的方式、访存的优化等也会有很大影响。

 **K210的其他参数如下：** 

- 双核 64-bit RISC-V RV64IMAFDC (RV64GC) CPU / 400MHz（可超频到600MHz）
- 双精度 FPU
- 8MiB 64bit 片上 SRAM（6MiB通用SRAM+2MiB的AI专用SRAM）
- 神经网络处理器（KPU） / 0.8TFLOPS
- 音频处理器（APU）
- 可编程 IO 阵列 (FPIOA)
- 双硬件512点16位复数FFT
- SPI, I2C, UART, I2S, RTC, PWM, 定时器支持
- AES, SHA256 加速器
- 直接内存存取控制器 (DMAC)

芯片采用BGA144封装，28nm制程工艺，芯片功耗低至0.35W，而成本， **只要20+RMB** 。

想一下一块Arduino的板子要多少钱......Arduino的性能和外设在K210面前就是个 **弟中弟** （摊手）。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0225/114924_f22a4f3f_5631341.png "屏幕截图.png")

 _芯片架构图_ 

 **那么K210又能做哪些事情呢？** 

多到让你怀疑它是否真的只是个单片机......人脸检测、物体识别、播放视频、声场成像、3D渲染，甚至还能在上面跑FC模拟器玩游戏......

可以去搜一下k210的一些应用视频演示，相信看完你会爱了的。

好了芯片就吹到这里，下面介绍一下市面上已有的一些K210开发板。

### 如何选择开发板

如果看完上面的介绍对K210产生了兴趣，那么下面我会推荐几款值得入手的K210开发板（如果不小心打了广告麻烦结一下广告费:D）。

其实我早在去年很早的时候就收到过K210的开发板了，当时是别人送的评测套件，只不过我那时不巧刚毕业，没有太多时间折腾，就一直拖到最近才有空仔细研究。另一方面的原因是，当时K210刚出，软件生态还非常的不成熟，而这个情况现在就好多了，不光有官方的IDE（还是develop版本），甚至还有MicroPython这样的固件的移植，可玩性高了很多，值得折腾一番。

我个人是打算后面自己设计一块K210开发板的，用于满足我个人的各种 **_变态要求_** （主要是要狗迷你）。不过对于普通玩家而言，BGA的芯片可能不太适合自己DIY，所以可以按我下面的推荐进行购买即可，其中很多都是我亲自买过试用的。

 **1. 官方开发板套件-KD233** 

![输入图片说明](https://images.gitee.com/uploads/images/2022/0225/115113_96c7ccc3_5631341.png "屏幕截图.png")

这个是嘉楠官方的评估板套件，好处呢在于可以和官方IDE中的例程项目无缝对接（当然如果不打算用官方IDE就无所谓了，这个后面软件篇会介绍），IDE中下载的例程都可以直接烧录运行。同时板子上的资源还是挺多的，摄像头、麦克风、LCD屏幕之类的都有，而且可以看到芯片周围有一圈跳线帽，把所有的IO都引出来了，也可以自己取掉跳线帽用杜邦线连接其他硬件。

缺点也很明显，这板子实在是太爱爱爱大了.....反正我是看不顺眼，虽然买了但是只是用来验证官方给的原理图是否无误，调试的时候还是用其他的板子。

价格偏贵，对于想自己设计硬件的同学可以购买，其他的就不推荐了。

 **2. Widora-AIRV2/BITK210开发板套件** 

![输入图片说明](https://images.gitee.com/uploads/images/2022/0225/115220_4a67d780_5631341.png "屏幕截图.png")

与第一个板子完全相反，这个是目前市面上能买到的最小的k210开发板，而且价格非常实惠。

这个开发板采用了核心板和底板分离的设计方式，适合那些想自己做硬件但是又无法设计核心电路的同学。核心板使用的封装是NGFF（Mini-PCIE）接口，把芯片最小系统和电源IC集成在了核心板，剩下的功能全部由金手指引出至底板。

> 可以看到核心板背面在K210的正下方布置有很多滤波电容，因为在这么高的运行主频下对电源的稳定性由很高要求，基本就是按照ARM CPU的Layout方式来设计PCB的。

板子也有带摄像头和LCD，但是默认摄像头的型号和官方开发板有点区别，官方的是OV5640，而这个带的是OV2640。当然了，这两个摄像头是完全pin-to-pin的，可以自己更换，修改一下软件驱动就行。屏幕也换成了2.4寸的小一点的ST7789驱动LCD，我个人还是觉得这屏幕太大（傲娇）。

还有一个缺点就是，这个板子的配套软件资源很少（可以说没有），当然了只要是K210的板子其实各家的软件都是可以用的，只不过由于硬件上的细微差别，需要自己修改一点源码（比如GPIO编号之类的）。

总的来说非常推荐，性价比也非常的高，适合在玩熟了之后用作以后项目的主力产品。

 **3. Sipeed Maixduino 开发板** 

Sipeed就是之前做荔枝派的那家公司了，他们团队也推出了一系列的K210开发板，足以满足各种需求，Sipeed Maixduino就是其中一款。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0225/115323_b54689b3_5631341.png "屏幕截图.png")

看这个开发板的名字里面还带个`duino`，该不会支持Arduino编程吧？？

 **是的，但是目前只支持一丢丢。** 

官网WiKi上的接口目前基本全是空白，有兴趣的同学得再等一等， **暂时先收起你那大胆的想法** 。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0225/115416_1a6400e5_5631341.png "屏幕截图.png")

 _只有IO类的API有相关实现_ 

Maixduino显然是冲着兼容Arduino接口设计的，板子是标准的UNO型，相比与上面的两个开发板多了一个ESP32模块（话说ESP32也是一个网红硬件），官方开发板有的功能它都有，体型还挺小巧。

值得一提的是这个板子上面搭载的邮票孔核心板是可以单独购买的，叫做M1 Module，下面会进行介绍。

 **4. Sipeed M1 开发板套件** 

![输入图片说明](https://images.gitee.com/uploads/images/2022/0225/115451_6199c03a_5631341.png "屏幕截图.png")

这个板子使用的就是上面的那个M1 Module，采用核心板加底板的方式，比上面的要小巧很多，同时所有IO都有引出，可以看作一个最小系统洞洞板，我个人比较喜欢这种设计，相比于Widora的板子黑色沉金也更好看一些。

板子不算太贵，Sipeed 系列的K210板子最大的优势是，软件资源做得非常好，官网WiKi、教程、源码的开放都很到位，考虑到硬件和软件双方面的条件，Sipeed 的开发板是我最建议大家购买的。

他家还有其他型号的K210开发板，我就不一一介绍了，大家感兴趣的可以自己去某宝搜。

### 编程环境

K210是支持好几种编程环境的，从最基本的`cmake命令行开发环境`，到`IDE开发环境`，到`Python脚本式`开发环境都支持，下面会进行分别介绍。

这几种开发方式没有优劣之分，有的人喜欢用命令行+vim，有的人喜欢IDE图形界面，也有的人根本不关心编译环境觉得人生苦短只想写Python。

一般来说越基础的开发方式比如C语言+官方库会自由度越大，能充分发挥出芯片的各种外设功能，但是开发难度比较高，过程很繁琐；越顶层的开发方式比如写脚本，虽然十分地便捷，甚至连下载程序的过程都不需要了，但是程序功能的实现极度依赖于MicroPython的API更新，且很多高级系统功能无法使用。

### 1. 命令行开发环境

首先说一下，K210的官方SDK支持两种开发模式： **FreeRTOS** 和 **Standalone（裸机）** 。

具体选择哪种模式取决于个人喜好，如果你之前用过FreeRTOS的话，应该可以很快熟悉SDK的相关接口。我个人对于单片机是更愿意使用裸机开发模式，因为如果要跑OS的话，我会倾向于使用能跑完整Linux系统的开发板，比如这个我自己做的[Linux-Card](https://zhuanlan.zhihu.com/p/77585297)。

 **用命令行开发的话建议在Linux环境下（Windows下用命令行总感觉怪怪的），下面介绍环境搭建方式。** 

 **1.1 下载SDK** 

在官网的资源下载页面下载相应的SDK：

> https://kendryte.com/downloads/  
> ​kendryte.com/downloads/

![输入图片说明](https://images.gitee.com/uploads/images/2022/0225/115809_4b07ad8c_5631341.png "屏幕截图.png")

 **1.2 安装工具链** 

安装 `build-essential` 以获取 `make` 工具


```
$ sudo apt install build-essential
```

安装`cmake`


```
$ sudo apt install cmake
```

从[勘智官网](https://kendryte.com/downloads/)下载Ubuntu版本工具链，放到`/opt`目录并解压缩。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0225/115953_65176db8_5631341.png "屏幕截图.png")


```
$ sudo mv kendryte-toolchain-ubuntu-amd64-8.2.0.tar.gz /opt$ cd /opt$ sudo tar -zxvf kendryte-toolchain-ubuntu-amd64-8.2.0.tar.gz
```

打开`~/.bashrc`文件，在文件末尾添加如下一行，将 `/opt/kendryte-toolchain/bin` 目录添加到 _PATH_ 环境变量


```
export PATH=$PATH:/opt/kendryte-toolchain/bin
```

使修改生效


```
$ source ~/.bashrc
```

 **1.3 编译 hello world 工程** 

从[Kendryte Github](https://github.com/kendryte/kendryte-standalone-sdk)下载`kendryte-standalone-sdk`


```
$ git clone git@github.com:kendryte/kendryte-standalone-sdk.git
```

hello world工程在`kendryte-standalone-sdk/src/hello_world`目录下。

创建一个build目录并进入：


```
$ mkdir build && cd build
```

运行 _cmake_ 


```
$ cmake .. -DPROJ=hello_world -DTOOLCHAIN=/opt/kendryte-toolchain/bin
```

编译


```
$ make
```

会在build目录下生成.bin文件，接下来把这个文件烧录进芯片就ok了。

 **1.4 烧录固件** 

K210使用的是串口ISP进行下载程序（也支持J-Link调试不过个人感觉不太必要，这里就不介绍了）。

IO_16 用于boot 模式选择，上电复位时，拉高进入FLASH 启动，拉低进入ISP 模式。复位后，IO_0、IO_1、IO_2、IO_3 为JTAG 引脚，IO_4、IO_5 为ISP也就是UART0引脚。

上面介绍的K210的开发板都会板载USB-TTL串口的芯片，所以板子直接连上电脑，选择好串口号，就可以下载程序了，跟Arduino一样的体验。只不过下载过程需要用到一个下载工具，叫做 **K-Flash** 。

K-Flash的软件同样可以在上面的官网地址进行下载，软件操作界面很简单，选择bin文件、板子型号、串口号，点击下载就可以了。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0225/120335_f8041a5e_5631341.png "屏幕截图.png")

 **1.5 打包Kfpkg固件** 

K210 的固件包主要有两种格式： .bin 和 .kfpkg。

.kfpkg可以包含多个.bin文件或者模型文件，可以方便地把程序固件和网络模型文件打包到一起一次性烧录，烧录方式是和bin文件一样的，用K-Flash即可。这里介绍如何制作kfpkg文件以及使用：

 **创建自己的 .kfpkg 文件** 

.bin文件是固件内容，作为参数传给烧录软件，软件会默认烧录到flash的0地址，完成后重启即可运行。

但是有时候我们需要烧录其它二进制文件到flash，比如模型、文件系统、或者自己定义的其它数据，这时需要指定烧录的地址，光有 .bin文件烧录工具并不知道我们想把数据烧录到flash的哪里，打包一个.kfpkg格式的文件则是为了解决这个问题的。

kfpkg由3部分组成：

- flash-list.json文本文件：用于储存.bin文件列表以及烧录地址等信息
- .bin固件
- 其他文件(二进制文件)

比如我们想同时下载名为XXX.bin的固件，以及YYY.bin的其它文件到Flash的0xA00000地址，则需要写一个 flash-list.json 文件，内容如下：


```
{
  "version": "0.1.0",
  "files": [
    {
      "address": 0,
      "bin": "XXX.bin",
      "sha256Prefix": true
    },
    {
      "address": 0x00A00000,
      "bin": "YYY.bin",
      "sha256Prefix": false
    }
  ]
}
```

注意sha256Prefix选项， 固件需要校验，所以为true， 而其它数据（比如模型数据）不需要，所以为false。

最后，将这三个文件（XXX.bin， YYY.bin， flash-list.json）压缩成zip文件，然后改后缀名为.kfpkg 即可被烧录工具识别，并按照指定地址烧录到flash。

### 2. IDE开发环境

对于想在Windows下开发的同学，可以使用官方提供的IDE，这也是我最常用的开放方式。

官方的IDE是基于 **Visual Studio Code** 开发的，非常好用， **最主要是要有代码自动补全:D** 。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0225/121132_48442db0_5631341.png "屏幕截图.png")

 **2.1 下载IDE** 

在这个地址下载：

> Kendryte IDE Downloads  
> ​kendryte-ide.s3-website.cn-northwest-1.amazonaws.com.cn/

![输入图片说明](https://images.gitee.com/uploads/images/2022/0225/121208_faa90103_5631341.png "屏幕截图.png")

下载完启动会自动联网更新组件，按下面的顺序点击下载官方例程：

![输入图片说明](https://images.gitee.com/uploads/images/2022/0225/121217_b1ce853f_5631341.png "屏幕截图.png")

IDE的好处就是可以自动化执行很多依赖项操作，这样操作：

![输入图片说明](https://images.gitee.com/uploads/images/2022/0225/121228_8315f538_5631341.png "屏幕截图.png")

然后就可以编译下载了：

![输入图片说明](https://images.gitee.com/uploads/images/2022/0225/121235_0408bf48_5631341.png "屏幕截图.png")

如果编译报错的话，首先确认已经点了 **安装所有依赖** ，另外记得先点一下编译左边的垃圾桶图标进行项目清理，再尝试编译。

> IDE下面这一排按钮其实就是顶部菜单栏的 **Kendryte** 选项下的内容。  
> 另外值得注意的是，如果使用IDE的 **Flash Manager** 下载文件，一定要记得勾选交换大小端，否则烧录进去的格式是有问题的，切记！

### 3. MicroPython开发环境

上面介绍的两种开发环境都是编写代码，然后编译下载，而这里介绍的这种方式只需要下载一次固件，然后就可以用串口的方式进行Python交互，或者也可以把脚本存在SD卡里然后开机运行。

这种脚本式的交互固件式基于一个叫[MicroPython](http://micropython.org/)的开源项目做的。

> MicroPython 是基于 Python3 的语法做的一款解析器，包含了 Python3 的大多数基础语法， 主要运行在性能和内存有限的嵌入式芯片上（最早开始火的是STM32上），注意 Micropython 不包含 Python3 的所有语法。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0225/121435_24c5baf4_5631341.png "屏幕截图.png")

 **MaixPy** 是将[Micropython](http://micropython.org/)移植到[K210](https://kendryte.com/)的一个项目， 支持 MCU 常规操作， 也集成了机器视觉和麦克风阵列等模块， 以快速开发智能应用。

MaixPy项目的开源地址：

> sipeed/MaixPy  
> ​github.com/sipeed/MaixPy

Micropython 可以让我们在 K210 上编程更加简单快捷，比如我们需要寻找 **I2C** 总线上的设备，只需要使用如下代码即可实现：


```
from machine import I2C

i2c = I2C(I2C.I2C0, freq=100000, scl=28, sda=29)
devices = i2c.scan()
print(devices)
```

同样，实现一个 **呼吸灯** ，只需要如下代码：


```
from machine import Timer,PWM
import time

tim = Timer(Timer.TIMER0, Timer.CHANNEL0, mode=Timer.MODE_PWM)
ch = PWM(tim, freq=500000, duty=50, pin=board_info.LED_G)
duty=0
dir = True
while True:
    if dir:
        duty += 10
    else:
        duty -= 10
    if duty>100:
        duty = 100
        dir = False
    elif duty<0:
        duty = 0
        dir = True
    time.sleep(0.05)
    ch.duty(duty)
```

 **实时拍照：** 


```
import sensor
import image
import lcd

lcd.init()
sensor.reset()
sensor.set_pixformat(sensor.RGB565)
sensor.set_framesize(sensor.QVGA)
sensor.run(1)
while True:
    img=sensor.snapshot()
    lcd.display(img)
```

总之就是非常简单易用，感兴趣的同学可以移步官方WiKi和教程网站：

> 关于MaixPy · MaixPy 文档  
> ​maixpy.sipeed.com/zh/

### 总结

本文从芯片架构，到开发板选型，再到软件开发环境的搭建介绍了关于K210的基础ABC，这块KPU其实有很多有意思的应用，我会在后面的文章中进行更多介绍，包括SDK中各个模块的使用方式，以及如何将自己的AI模型部署到K210上面去运行。

 **同时我自己也在设计新的开发板，届时项目也会进行开源的，大家可以期待一下~感兴趣的同学赶紧关注啦。** 

 **19.9.12更新，A-Eye已经开源了：** 

> peng-zhihui/A-Eye  
> ​github.com/peng-zhihui/A-Eye

 **也欢迎关注我的B站账号，我的视频资源会在B站上进行分享。** 

> 哔哩哔哩 @稚晖君  
> ​space.bilibili.com/20259914/?share_source=copy_link&share_medium=iphone&bbid=CC6CEA56-7660-4B15-A6F5-91C2187DFDB221539infoc&ts=1568050002


### 相关回答和文章：

- 稚晖：[树莓派（以及各种派）使用指南](https://zhuanlan.zhihu.com/p/77585297)
3159 赞同 · 95 评论文章

- [你有什么有单片机或开源硬件做的有意思的作品吗？](https://www.zhihu.com/question/265112599/answer/712474064)
5573 赞同 · 284 评论回答

编辑于 2020-05-02 01:08
- 嵌入式开发
- 人工智能
- AI技术
​赞同 1069​

---

- 作为一个相当嵌入式工程师的医生。我现在只能把你当成我的精神支柱了
- 说起来你可能不行 IDE是我们公司的一个工程师维护的，真的只有一个人。
- K210的KPU算力有0.8TOPS  
英伟达Jetson Nano的算力是0.47TFLOPS  
一个是TOPS，一个是TFLOPS，这样好比较么？
  - 因为k210是跑量化模型，而jetsonnano的gpu只能跑浮点，所以只能在这个维度比较了
- 最近刚刚开始研究K210，非常佩服大佬！求教一下，我用的micropython进行开发，一些第三方库(pytorch，opencv等)如何能装在板子上呢？
- 另外值得注意的是，如果使用IDE的Flash Manager下载文件，一定要记得勾选交换大小端，否则烧录进去的格式是有问题的，切记！---- 这段在什么地方设置？
- 大佬，在k210上部署自己的模型还出不[耶]
  - 会出，等我晚些有空
- 喜欢这句话，搞项目不挑食，爱给自己找麻烦，有啥玩啥
- 厉害，可以把硬件制作部分详细写一下不？应该不少人也非常感兴趣这部分的技能
  - 硬件部分比较复杂，刚发了个视频可以可参考 https://b23.tv/av70319783
- 期待之后的网络实战测试
- 作者视频里面那个摄像头分辨率是多少啊，是先把摄像头的数据模糊成显示屏那个样子，再进行AI算法识别的吗?还是只是屏幕像素问题，实际上摄像头数据的像素没那么低。
- 人脸检测，，，才发现这篇文章的措辞是多么严谨，我跑哪个模型之前一直以为是人脸识别，以至于被老师问了一下才发现这个问题
- 去年9月用过这个芯片，开发文档太简陋了…….就放弃了
- 楼主，我最近也在做CNN 一维的，想请教一下如果你要做training 的话 多少的training 量是比较好的，因为数据不太能拿。耗钱又耗时。
- 看你的视频容易自闭[捂脸][捂脸][捂脸]
- Cortex-M的M4CNN模块，这个有开源吗[大笑]
- 答主自己写的ai算法么？还是用别人的框架呢？
​  - 框架都是已有的（tf、torch等），算法是自己写的
- 这玩意有两个半遗憾点，首先是手册简陋，没有寄存器说明，只能用库。其次是没有以太网，本来它这个性能加上硬件FFT很适合继保的。那半个是因为最快的SPI电平是1.8V。
- 用了官方提供的sdk做完交叉编译和下载工作之后，就不需要在板子上先跑os就能跑算法？
- 可以开源ad工程吗？
  - 后面会开源的，最近比较忙还没有整理
  - 你用的摄像头应该是ov5640吧？不知道你跑的多大分辨率？如果是320*240的话是不是用ov2640更好？
  - 是因为ov5640自动对焦？
- 您好，我的KendryteIDE非得有网络才能启动，咋办呢，我拿离线包覆盖原来的文件直接把程序搞坏了啊
- @yuandj 源来这里是大本营。 (2021-03-10)
- 大佬，本来打算用派3b+做毕设人脸识别的，那我还是买个K210来做比较好？画面比较流畅啥的？有大佬可以解惑解惑嘛
- 求问各位大佬，如果想在代码里调用MicroPython不支持的标准库的话，比如pytorch和OpenCV，那要怎么办呢？
- 作者视频里面那个摄像头分辨率是多少啊，是先把摄像头的数据模糊成显示屏那个样子，再进行AI算法识别的吗?还是只是屏幕像素问题，实际上摄像头数据的像素没那么低。
- 人脸检测，，，才发现这篇文章的措辞是多么严谨，我跑哪个模型之前一直以为是人脸识别，以至于被老师问了一下才发现这个问题