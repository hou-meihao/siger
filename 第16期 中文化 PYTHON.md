《[中文化 PYTHON](https://gitee.com/yuandj/siger/issues/I3GFMC)》【[正文](https://gitee.com/yuandj/siger/issues/I3GFMC#note_8198017_link)】
- [重温初见木兰编程语言的那个战场](https://gitee.com/yuandj/siger/issues/I3GFMC#note_4752627_link)
- [python语言支持中文(函数)吗?](https://gitee.com/yuandj/siger/issues/I3GFMC#note_4752653_link)
- [欢迎访问草蟒（Python 中文版）3.8.0 源代码](https://gitee.com/yuandj/siger/issues/I3GFMC#note_8176606_link)

[用草蟒写一个中文爬虫抓个网页](https://gitee.com/yuandj/siger/issues/I3GFMC#note_8176804_link) ([2](https://gitee.com/yuandj/siger/issues/I3GFMC#note_8188384_link)) 【[正文](https://gitee.com/yuandj/siger/issues/I3GFMC#note_8198381_link)】
- 用 [PIP](https://gitee.com/yuandj/siger/issues/I3GFMC#note_8179779_link) [安装 python + selenium 环境](https://gitee.com/yuandj/siger/issues/I3GFMC#note_8176823_link) ([2](https://gitee.com/yuandj/siger/issues/I3GFMC#note_8179488_link))【[正文](https://gitee.com/yuandj/siger/issues/I3GFMC#note_8204815_link)】
- [解决 Python 抓取内容乱码问题](https://gitee.com/yuandj/siger/issues/I3GFMC#note_8188069_link)【[正文](https://gitee.com/yuandj/siger/issues/I3GFMC#note_8206324_link)】

[从中文编程到自然语言编程](https://gitee.com/yuandj/siger/issues/I3GFMC#note_8189156_link) 【[正文](https://gitee.com/yuandj/siger/issues/I3GFMC#note_8206585_link)】
- [CodeBERT: 面向编程语言和自然语言的预训练模型](https://gitee.com/yuandj/siger/issues/I3GFMC#note_8190386_link)
- [GANCoder自动编程：从自然语言到代码](https://gitee.com/yuandj/siger/issues/I3GFMC#note_8190449_link)

[![输入图片说明](https://images.gitee.com/uploads/images/2022/0107/022535_fb2abcfe_5631341.jpeg "斐波那契999.jpg")](https://images.gitee.com/uploads/images/2022/0107/022513_ab9ac636_5631341.jpeg)

> 刚刚完成《宇宙》的专题 “对宇宙的终极真理的追求精神” 感染的每一位观众中就有了我。这不被斐波那契吸引的我，找到了终极连接 —— 黄金分割，这里不需转载，一众关键词足以吸睛无数，而最需要说明的就是封面的螺线与五角星的 1.618 啦，“当人体以 维特鲁威人 姿态伸展，五角星的边成上臂平展时，臂长是1，则臂长+躯干=1.618，躯干/臂长=黄金分割，这就是精美的人体结构。” 关于黄金分割的种种神奇，我就不需要介绍了。封面的所有元素均来自网络。

> 就在一筹莫展 中文化 + Python 的关键词很少索引数时，Chinese localizasion + Python 更是少之甚少时，[斐波那契.py](https://gitee.com/Program-in-Chinese/overview/blob/master/%E7%A4%BA%E4%BE%8B%E4%BB%A3%E7%A0%81/%E6%96%90%E6%B3%A2%E9%82%A3%E5%A5%91.py) 映入眼帘，这打开了一扇窗，千年不变的 Hello world 的中文版，世界，你好！虽然也有。但都不及 斐波那契 引人入胜，这才有了本期封面，而才思泉涌的我将在接下来的时间里为 中文化 Python 贡献自己的一份力量 :pray: 惊喜留待以后再分享。

  - 黄金分割作为股神的宝典 则只为吸睛
  - 咖啡馆里撸 Python 代码 营造了轻松惬意
  - 斐波那契2.py 产生的169阶序列（黑白双色螺旋，是星系双螺旋的映射）
  - [斐波那契.py](https://gitee.com/Program-in-Chinese/overview/blob/master/%E7%A4%BA%E4%BE%8B%E4%BB%A3%E7%A0%81/%E6%96%90%E6%B3%A2%E9%82%A3%E5%A5%91.py) 源码，在黄金分割裸线绘制的 美纹纸 上 做背景是对 @zhishi 的敬意

  # 《[中文化 PYTHON](https://gitee.com/yuandj/siger/issues/I3GFMC)》

本期专题的缘起是 SIGer 早期被 开源社区 拥抱《[开源指北 1.0](https://zhuanlan.zhihu.com/p/347701292)》的功劳，时至今日 GItee 推荐关注栏目还是时长有 吴老师的推荐，而中文单词加空格也是受到《指北》中关于中英文混排的技巧 “前后加空格” 的影响。真正的牵挂则是 石榴派发布时构建的 中间层 中文 Python 是和 JAVA 与 FCscript 同等地位的。石榴派就要发布 1 周年啦，中文 Python 是一定要上的。而中文化就是早期石榴派的选择方向，[草蟒](https://gitee.com/shiliupi/grasspy380) 也是当时 FORK 和 收录的。

本节源自 @[吴烜](https://gitee.com/zhishi) 老师的 中文编程专栏
- https://zhuanlan.zhihu.com/p/31598712
- https://zhuanlan.zhihu.com/codeInChinese
- https://gitee.com/Program-in-Chinese

摘要来自以下三个主题：

### [Python3选择支持非ASCII码标识符的缘由](https://www.python.org/dev/peps/pep-3131/)

Python2并不支持非ASCII码标识符. PEP的全称是Python Enhancement Proposal, 即Python增强提案. 这个3131提案创建于2007年5月. Python3于2008年12月发布. Rationale(依据)一节开篇明义, 指出用母语命名标识符对代码清晰度和可维护性的提高.

下面列出了一些质疑和回应. 其中:

People claim that they will not be able to use a library if to do so they have to use characters they cannot type on their keyboards. However, it is the choice of the designer of the library to decide on various constraints for using the library: people may not be able to use the library because they cannot get physical access to the source code (because it is not published), or because licensing prohibits usage, or because the documentation is in a language they cannot understand. A developer wishing to make a library widely available needs to make a number of explicit choices (such as publication, licensing, language of documentation, and language of identifiers). It should always be the choice of the author to make these decisions - not the choice of the language designers.


简要翻译: 有声音表示库如果是其他语言命名, 不懂这一语言的使用者就不会输入API名了. 回应是库开发者有权根据需要进行设计, 这与其他制约因素(版权,文档是外文等等)类似. 开发者如果想要库被最广泛地使用, 自然会考虑到所有这些因素. 而这, 应该是开发者的决定, 而不是语言设计者的.

### [开源项目必须用英文命名标识符吗](https://my.oschina.net/u/4552012/blog/5005465)？

看到庄表伟的《[开源社区应该选择什么语言？](https://my.oschina.net/oscpyaqxylk/blog/4996695)》一文，其中建议一刀切地使用英文命名标识符：

> 我们将一个源代码文件，看做一篇完整的文章。在这篇文章中：中英文夹杂，甚至英文加汉语拼音混杂都是严重影响阅读体验的 包命名、文件名、函数名、变量名等等，都严重建议一律使用英文

不得不来发表些个人观点。在《Gitee 开源指北》[第 5 小节：有关开源的常见误区](https://gitee.com/opensource-guide/guide/%E7%AC%AC%E4%B8%80%E9%83%A8%E5%88%86%EF%BC%9A%E5%88%9D%E8%AF%86%E5%BC%80%E6%BA%90/%E7%AC%AC%205%20%E5%B0%8F%E8%8A%82%EF%BC%9A%E6%9C%89%E5%85%B3%E5%BC%80%E6%BA%90%E7%9A%84%E5%B8%B8%E8%A7%81%E8%AF%AF%E5%8C%BA/#%E5%BC%80%E6%BA%90%E9%A1%B9%E7%9B%AE%E5%BF%85%E9%A1%BB%E7%94%A8%E8%8B%B1%E6%96%87%E5%91%BD%E5%90%8D%E6%A0%87%E8%AF%86%E7%AC%A6%E5%90%97) 中，有与本文同题的一节进行阐述。三个月前还围绕此部分进行了一场 [持续数日、来回数十回合的论辩](https://gitee.com/gitee-community/opensource-guide/pulls/228)。有兴趣的可以细看，此文仅分享一个两年半前的开源合作实践经历。

 _**可见中文命名对于鼓励新手参与开源项目的作用。**_ 

回忆一下，无论是开源还是闭源项目，过去几周有没有碰到如下情况之一：

- 翻自己之前写的代码，发现某个标识符不知所谓
- 看别人的代码，不懂某个标识符
- 同事来问你某个标识符啥意思

补：最常见的问题之一是："用中文命名之后，国外开发者如何参与"? 对此在《Gitee 开源指北》中也有提及，可以另开文章详述。

### [Gitee《开源指北 1.0》之——开源项目必须用英文命名标识符吗](https://zhuanlan.zhihu.com/p/347701292)？

下面是一些常见的疑虑：

- 用中文命名出了问题怎么办？
  像任何技术一样，在使用推广中必然有各种问题出现。如果是编程语言或是开源框架对中文命名的支持问题，可以向它们的开发组反映，往往能得到解决，先例有 Vue.js、Hibernate、pip 等等。另外，也可在开源中国社区求助，协力解决。毕竟，鲁迅先生说过：“走的人多了，也便成了路”。


- 框架限制必须用英文怎么办？
  比如 JavaBeans 规范通常要求使用 set/get 前缀。即便如此，仍然可以使用中英混合的命名，只要开发者感觉更易读即可，比如 getCostOutcomeRatioByClientGroup 相比 get投入产出比By客户组。类似地，一些常用的英文术语或简写，如果没想到合适中文对应术语，大可以暂时保留这部分英文命名。


- 中英文混输的效率会低吗？
  大多数情况下，读代码的时间远超过写代码的时间。即便仅仅讨论写代码的效率，也要考虑推敲英文命名甚至查字典的时间，何况很多领域业务相关命名连恰当英文表达都很难找到。另外，随着中文命名实践的普及，相应的 IDE 辅助功能也在不断涌现，比如 JetBrains 和 VS Code 的中文代码补全插件。


- 用拼音效果一样吗？
  从代码可读性看，使用拼音不如中文。比如 shipin，就有食品/视频/饰品等等理解。即便可以根据代码上下文进行猜测，无疑也是额外的负担。拼音缩写的可读性更差，像 xmjl，到底是姓名记录、项目奖励、项目监理还是项目经理呢？如果需要额外中文注释才能避免误解，还不如将直接将中文注释的内容用于命名。输入效率来说，输入全部拼音的速度也往往不如输入中文，因为大多数中文词语不需输入全部拼音即可选词。更不用说用拼音命名更容易出现细节错误如翘平舌、前后鼻音等等。


- 项目已经是英文命名的，没法转了？
  如果某一部分的英文命名经常在开发组中引起误解或者新手甚至自己也难以看懂，可以尝试从这部分先开始中文化，看看效果后再逐步在其他部分采用。与任何未曾尝试过的技术一样，渐进增量式的应用可以减小风险、在使用中逐渐适应。


- 用了中文命名之后，怎样为国外用户服务？
  如果需要面向国外用户，首先要对用户界面和使用文档进行国际化。如果是库，用户界面就是 API。技术上，可以开发中英两套 API。如果想更进一步，鼓励国外开发者参与开发项目，当然可以逐步将命名英文化，但也应综合考虑是否值得。

  总之，澄清这个误区的目的，绝不是排斥英文命名，而是让更多开源作者意识到可以视项目性质和自身情况因地制宜、具体情况具体分析，灵活选择何时、何处进行中文命名实践，也希望中文开源社区能以开放宽容的心态看待中文命名标识符这一并不新的“新技术”。

最近 RISCV 社区的中坚力量 中科院计算所的 [香山处理器](https://gitee.com/flame-ai/siger/blob/master/RISC-V/Shinning%20Star.md)，就采用的是中文拼音命名的方式，最新版本 [南湖](https://gitee.com/flame-ai/siger/blob/master/RISC-V/RV4kids%2020th%20-%20Nan%20Hu.md) 对中国自主之路是有特殊意义的，而相应模块则用 [HuanCun](https://github.com/OpenXiangshan/huancun) 来实现。用母语思考，才是中文化的灵魂。

### [重温初见木兰编程语言的那个战场](https://zhuanlan.zhihu.com/p/265091649)

- 1 月 16 日 不急
- 1 月 17 日 波澜不惊
- 1 月 18 日 急转直下
- 1 月 19 日 快，快！
- 1 月 20 日 汉芯也着了道？！
- 1 月 21 日 没消息是好消息
- 1 月 22 日 必须冷静
- 1 月 23 日 拼了
- 1 月 24 日 猝不及防的尾声
- 1 月 25 日 过年

[吴烜](https://gitee.com/zhishi)，投笔于太平洋时间 2020 年 10 月 11 日 半夜  
编辑于 2020-12-29  
【[全文转载](https://gitee.com/yuandj/siger/issues/I3GFMC#note_4752627_link)】

> 这就像是一部话剧的一幕幕场景切换，颇为戏剧性，而故事的讲述者就是 @[吴烜](https://gitee.com/zhishi) 老师，之后他选择接棒，并将用中文编程的旗帜牢牢地扛在了肩头 :pray: 

# [用草蟒写一个中文爬虫抓个网页](https://gitee.com/yuandj/siger/issues/I3GFMC#note_8176804_link)

这节标题是为了给我凌乱的笔记做一个会聚，以不至于读者被我的笔记整得没有兴趣，结构规范，思维缜密，工作细致都是一个系统蓬勃发展的根基，这不能是凌乱的笔记呈现给读者的理由，因此，补上这节更清晰的展示学习的过程，以此反面例子警示读者。（以下提纲经过重新整理）

### 【专题】[石榴派 # 16 “吴烜” 《中文化 PYTHON》](https://gitee.com/yuandj/siger/issues/I3GFMC)

- @[吴烜](https://gitee.com/zhishi) [老师的专栏文章](https://gitee.com/yuandj/siger/issues/I3GFMC#note_4752571_link)

  - [已有 IntelliJ 系列 IDE 下的中文命名补全辅助插件实现了此功能：](https://gitee.com/tuchg/ChinesePinyin-CodeCompletionHelper) 下载已破十三万。
  - [另有 VS Code 下类似插件，集成了第三方拼音输入法 API，可以在声明变量时也用拼音输入不需完整中文](https://gitee.com/Program-in-Chinese/vscode_Chinese_Input_Assistant)：
  - [与现实业务语义相关的部分用母语命名的优势尤其明显，一些实例请见](https://www.zhihu.com/question/437644040/answer/1660325367)：
  - [你参与的开源项目选个出来，一道把标识符中文化后对比一下再讨论吧，就像在此论辩中这样](https://gitee.com/gitee-community/opensource-guide/pulls/228#note_3881031)：

- [「木兰」编程语言有什么特色](https://www.zhihu.com/question/366509495)？ 【[笔记](https://gitee.com/yuandj/siger/issues/I3GFMC#note_4752626_link)】

  “鸿蒙木兰相声选段”方舟也被调侃，只能说这是肿么啦，要吐槽到这种地步，这就是中文化之路的实景。

- [重温初见木兰编程语言的那个战场](https://zhuanlan.zhihu.com/p/265091649) 【[笔记](https://gitee.com/yuandj/siger/issues/I3GFMC#note_4752627_link)】

- [木兰编程语言(ulang)v0.2.2官方版](https://gitee.com/yuandj/siger/issues/I3GFMC#note_4752643_link) 【界面】1.[直接进行编译](https://images.gitee.com/uploads/images/2021/0406/011618_32411714_5631341.png) / 2.[和Python一样简单好用](https://images.gitee.com/uploads/images/2021/0406/011625_5f9d60b3_5631341.png)

  > 中国科学院计算技术研究所是我国计算机事业的摇篮，是第一个专门从事计算机科学技术综合性研究的学术机构。研制出我国的第一台通用电子计算机、首枚通用CPU，并孵化出联想、曙光等高新技术企业，为国家做出了诸多载入史册的贡献。党的十九大报告中规划了建设世界科技强国和实现社会主义现代化强国目标的蓝图，并提出了实现两个一百年的目标。计算机技术作为当今信息时代的最重要支撑技术，是实现科技创新和产业弯道超车的基础和关键。中科院计算所作为该领域的科研国家队，不仅在学术成就和信息产业化成果方面取得了突出的成就，也在青少年科技人才培养的教学方法和面向青少年的人工智能教育、信息技术教育方面上贡献着力量。

  - 软件优势有三：可爱与详细的界面设计，形象简单的编程方式，多样化学习。
  - 软件特色：Python 是一种跨平台的计算机程序设计语言。是一种面向对象的动态类型语言，最初被设计用于编写自动化脚本(shell)，随着版本的不断更新和语言新功能的添加，越来越多被用于独立的、大型项目的开发。

    > 而 ulang 就是运用 Python 进行运算的，不过一直说的 ulang 可以中文编程，小编测试完后发现软件并不能实现这个功能，输入中文后会自动闪退。

    - [“木兰”是用带着“自主”旗号的项目骗了国家的钱](https://baijiahao.baidu.com/s?id=1656202601843737746)？ - 瞭望智库 20-01-20 07:51

- [python支持中文函数么_python语言支持中文吗](https://gitee.com/yuandj/siger/issues/I3GFMC#note_4752653_link) 【[笔记](https://blog.csdn.net/weixin_36162235/article/details/111980498)】 **9个月前** 

  这是第一直觉有没有中文版的 Python ，才有了后面的草蜢 380 的关注。
  - [缺少的是 木兰 编程的 HELLO WORLD 实践](https://gitee.com/yuandj/siger/issues/I3GFMC#note_8117923_link)。  **8天前** 

### Python 中文支持 与 [草蟒](https://www.grasspy.cn/) Python 汉化版

```
Python 3.10.1 
(tags/v3.10.1:2cd268a, Dec  6 2021, 19:10:37) 
[MSC v.1929 64 bit (AMD64)] on win32
>>> print("hello,world!")
hello,world!
>>> print("世界，你好！")
世界，你好！
>>>
```

- [安装最新的 PYTHON](https://gitee.com/yuandj/siger/issues/I3GFMC#note_8176520_link)

  - [print("hello,world!") & print("世界，你好！") @ Python 3.10.1](https://gitee.com/yuandj/siger/issues/I3GFMC#note_8176597_link)
  - [20M 在线安装还需要一些时间！](https://gitee.com/yuandj/siger/issues/I3GFMC#note_8176593_link)

<img src="https://images.gitee.com/uploads/images/2022/0105/232207_a2e744d9_5631341.png" height="136px"> <img src="https://images.gitee.com/uploads/images/2022/0105/234232_9ea8658b_5631341.png" height="136px">

```
Python 3.8.0 
(heads/master-dirty:8547772, Apr  6 2021, 22:26:22) 
[MSC v.1924 64 bit (AMD64)] on win32
>>> 打印（“你好，世界！”）
  文件 "<stdin>", 行 1
    打印（“你好，世界！”）
               ^
SyntaxError: 标识符中有无效字符
>>> 打印("你好，世界！")
你好，世界！
>>>
```

- 欢迎访问[草蟒（Python 中文版）](https://www.grasspy.cn)3.8.0 源代码 【[笔记](https://gitee.com/yuandj/siger/issues/I3GFMC#note_8176606_link)】

  - 查看 宝典，学会一招儿 [在线添加路径.](https://gitee.com/yuandj/siger/issues/I3GFMC#note_8176683_link) 【图】[1](https://images.gitee.com/uploads/images/2022/0106/001038_04e4234d_5631341.png) / [2](https://images.gitee.com/uploads/images/2022/0106/001105_1bdf3ca2_5631341.png) / [3](https://images.gitee.com/uploads/images/2022/0106/001122_8b694562_5631341.png) / [4](https://images.gitee.com/uploads/images/2022/0106/001145_f21a4256_5631341.png) 
  - 2、[草蟒解释器 3.8.0 绿色免安装版](https://www.grasspy.cn/download/grasspy38064_20211204.rar)（78M）及《[草蟒宝典](https://www.grasspy.cn/download/%E8%8D%89%E8%9F%92%EF%BC%88Python%E4%B8%AD%E6%96%87%E7%89%88%EF%BC%89%E5%AE%9D%E5%85%B81.1.pdf)》 【[笔记](https://gitee.com/yuandj/siger/issues/I3GFMC#note_8176611_link)】
  - [打赏赞助](https://www.grasspy.cn/sponsor.html) [QR](https://images.gitee.com/uploads/images/2022/0106/003314_9ec1d014_5631341.png) （支持了 ￥6.9，宝典文档写的好） 
  - 老吴建议大家下载 [sublime](https://www.sublimetext.com/) 工具，这是一个轻量级编辑器，【[笔记](https://gitee.com/yuandj/siger/issues/I3GFMC#note_8176701_link)】
    - [DOWNLOAD FOR WINDOWS](https://download.sublimetext.com/sublime_text_build_4126_x64_setup.exe) 18.3M

  [存档文章](https://www.grasspy.cn/articles.html)【[笔记](https://gitee.com/yuandj/siger/issues/I3GFMC#note_8176735_link)】
 
  1. [中文编程语言新探之“〇”和“々”](https://zhuanlan.zhihu.com/p/425606482)
  1. [利用 pyright 实现 MicroPython/Python 中文编程和中英互译](https://zhuanlan.zhihu.com/p/415336605)
  1. [如何汉化并编译 Python 源代码](https://blog.csdn.net/qq_33292758/article/details/103021546)
  1. [关于 Python 3.8 关键字汉化的几点说明](https://zhuanlan.zhihu.com/p/91713639)
  1. [MicroPython 汉化初探](https://www.grasspy.cn/articles/mpy_first_attempt.html)
  1. [Blender 中嵌入的 Python 3.7.7 的汉化和编译](https://www.grasspy.cn/articles/for_blender.html)
  1. [睡觉排序大法](https://blog.csdn.net/qq_33292758/article/details/112215243?spm=1001.2014.3001.5502)
  1. [草蟒“逗号表”模块介绍及中文命名随想](https://blog.csdn.net/qq_33292758/article/details/111980759?spm=1001.2014.3001.5502)
  1. [谈谈 python os 模块的中文化](https://blog.csdn.net/qq_33292758/article/details/112436421?spm=1001.2014.3001.5502)
  1. [草蟒编程语言新增“自动访问”库](https://www.oschina.net/news/116497/grasspy-updated)
  1. [草蟒中文编程：“自动访问”库（selenium）基本使用](https://my.oschina.net/grasspylaowu/blog/4313814)
  1. [草蟒语言重大更新：错误提示以中文展示](https://www.oschina.net/news/120364)
  1. [为 Python 3.9 版本添加中文关键字 — 写在草蟒 39x 发布之前](https://www.oschina.net/news/121009/grasspy-updated)
  1. [草蟒 3.8.0 十月更新版发布及支持 blender 脚本编程的草蟒 3.7.7 试编译](https://my.oschina.net/grasspylaowu/blog/4681693)
  1. [草蟒 12 月更新：核心功能文档和坑属性/方法完成中文化](https://www.oschina.net/news/123404/grasspy--dec-updated)
  1. [草蟒2021年1月更新已发布](https://www.oschina.net/news/126844)
  1. [汉化经验总结（有些已过时，亟需重写）](https://www.grasspy.cn/articles/%E6%B1%89%E5%8C%96%E7%BB%8F%E9%AA%8C%E6%80%BB%E7%BB%93.html)

### [实时同步 LudiiGames 数据库 by Python](https://gitee.com/blesschess/LuckyLudii/issues/I4PJP4) (立 FLAG:)

- 爬虫
- 绘制棋盘

以上两个应用，均会使用中文草蟒 PY 进行示范。 【[笔记](https://gitee.com/yuandj/siger/issues/I3GFMC#note_8176800_link)】

- [python写一个自动在网页搜索资源并下载的程序](https://blog.csdn.net/hujiahao_ros/article/details/102075542) 【[笔记](https://gitee.com/yuandj/siger/issues/I3GFMC#note_8176804_link)】
  - [测试PY31 发现不支持汉语UNICODE 包括注释](https://gitee.com/yuandj/siger/issues/I3GFMC#note_8181790_link)

- [Python + Selenium -Python 3.6 3.7 安装 PyKeyboard PyMouse](https://www.cnblogs.com/gqv2009/p/13527563.html) 【[笔记](https://gitee.com/yuandj/siger/issues/I3GFMC#note_8176823_link)】
  1. [Selenium](https://www.selenium.dev) 【[笔记](https://gitee.com/yuandj/siger/issues/I3GFMC#note_8179113_link)】[Downloads](https://www.selenium.dev/downloads/)
  2. [Google Chrome 版本通过 HELP 查看](https://gitee.com/yuandj/siger/issues/I3GFMC#note_8179279_link) （96.0.4664.110）（64 位）WebDrivers:
     - If you are using Chrome version 97, please download [ChromeDriver 97.0.4692.71](https://chromedriver.storage.googleapis.com/index.html?path=97.0.4692.71/)
     - If you are using Chrome version 96, please download [ChromeDriver 96.0.4664.45](https://chromedriver.storage.googleapis.com/index.html?path=96.0.4664.45/)
     - If you are using Chrome version 95, please download [ChromeDriver 95.0.4638.69](https://chromedriver.storage.googleapis.com/index.html?path=95.0.4638.69/)

  3. [Selenium(3)：python+selenium 环境安装](https://gitee.com/yuandj/siger/issues/I3GFMC#note_8179488_link)
     - 准备工具如下 : [Python](https://www.python.org/getit/PyCharm) | [Pycharm](http://www.jetbrains.com/pycharm/download/Selenium) | [Selenium](https://pypi.python.org/pypi/selenium)
     - Driver驱动程序: 【[笔记](https://gitee.com/yuandj/siger/issues/I3GFMC#note_8180182_link)】

       > 当Selenium2.x提出了WebDriver的概念之后，它提供了完全另外的一种方式与浏览器交互。那就是利用浏览器原生的API，封装成一套更加面向对象的SeleniumWebDriverAPI，直接操作浏览器页面里的元素，甚至操作浏览器本身（截屏，窗口大小，启动，关闭，安装插件，配置证书之类的）。由于使用的是浏览器原生的API，速度大大提高，而且调用的稳定交给了浏览器厂商本身，显然是更加科学。然而带来的一些副作用就是，不同的浏览器厂商，对Web元素的操作和呈现多少会有一些差异，这就直接导致了SeleniumWebDriver要分浏览器厂商不同，而提供不同的实现。例如Firefox就有专门的geckoDriver，Chrome就有专门的ChromeDriver等等
         - [`pip install pyhook` error](https://gitee.com/yuandj/siger/issues/I3GFMC#note_8181625_link)
         - [`pip install PyUserInput pymouse pykeyboard` success](https://gitee.com/yuandj/siger/issues/I3GFMC#note_8181672_link) 
         - `ModuleNotFoundError: No module named 'win32api'` [如何解决](https://www.cnblogs.com/SH170706/p/9640110.html)？【[笔记](https://gitee.com/yuandj/siger/issues/I3GFMC#note_8181698_link)】  
           **幸福都是奋斗出来的，努力奋斗才能梦想成真。坚持自律，约束自我，克制弱点，坚持努力，遇见更好的自己。**
         - [`pip install pypiwin32` & `import win32api` success](https://gitee.com/yuandj/siger/issues/I3GFMC#note_8181720_link)

     - **webdriver配置（以chromedriver为例）：** 配置方式一 & 配置方式二

       - [PY 配置方式一：driver=webdriver.Chrome() 报错](https://gitee.com/yuandj/siger/issues/I3GFMC#note_8181836_link)  **ERROR** 
       - [换浏览器](https://gitee.com/yuandj/siger/issues/I3GFMC#note_8182197_link) [FIREFOX](https://images.gitee.com/uploads/images/2022/0106/135049_ba71dbdd_5631341.png)
         - [没有对应的版本号，就用最新的吧。](https://gitee.com/yuandj/siger/issues/I3GFMC#note_8182567_link)
         - [`browser = webdriver.Firefox()` selenium.common.exceptions](https://gitee.com/yuandj/siger/issues/I3GFMC#note_8182612_link)
         - [解决“selenium.common.exceptions.SessionNotCreatedException:](https://gitee.com/yuandj/siger/issues/I3GFMC#note_8182716_link) ...  
           说明firefox浏览器版本和浏览器驱动版本不匹配
       - [异次元软件 Chrome 介绍 Windows 版下载:](https://gitee.com/yuandj/siger/issues/I3GFMC#note_8183993_link) 最新版本：[v97.0.4692.71](https://images.gitee.com/uploads/images/2022/0106/152346_667450c5_5631341.png)
         - [配置方式一：& 配置方式二：](https://gitee.com/yuandj/siger/issues/I3GFMC#note_8184410_link)再次尝试
         - [`driver=webdriver.Chrome()` 'chromedriver.exe' executable needs to be in PATH.](https://gitee.com/yuandj/siger/issues/I3GFMC#note_8184419_link) 
     - 环境安装步骤 （ [1](https://images.gitee.com/uploads/images/2022/0106/104801_f0b20a0b_5631341.png) / [2](https://images.gitee.com/uploads/images/2022/0106/104809_649f14e2_5631341.png) / [3](https://images.gitee.com/uploads/images/2022/0106/104816_7c620dbd_5631341.png) / [4](https://images.gitee.com/uploads/images/2022/0106/104822_3d9ff457_5631341.png) / [5](https://images.gitee.com/uploads/images/2022/0106/104832_59876e5e_5631341.png)） 别忘记新建项目测试。 ( **[chrome 驱动的 Taobao 镜像](http://npm.taobao.org/mirrors/chromedriver/)** )

  4. [selenium 4.1.0](https://gitee.com/yuandj/siger/issues/I3GFMC#note_8179578_link) `pip install selenium` Installing & Drivers

### [PIP](https://gitee.com/yuandj/siger/issues/I3GFMC#note_8179779_link) [Getting Started](https://pip.pypa.io/en/stable/getting-started/)

- my version

```
C:\Users\Administrator\Desktop\2022\Py31>py --version
Python 3.10.1

C:\Users\Administrator\Desktop\2022\Py31>py -m pip --version
pip 21.2.4 from C:\Users\Administrator\Desktop\2022\Py31\lib\site-packages\pip (python 3.10)
```

- [这里输入代码 `pip install selenium`](https://gitee.com/yuandj/siger/issues/I3GFMC#note_8179940_link)
- [`python.exe -m pip install --upgrade pip`](https://gitee.com/yuandj/siger/issues/I3GFMC#note_8179975_link)
- [Common tasks](https://gitee.com/yuandj/siger/issues/I3GFMC#note_8180129_link)

  > 任何人都可以上传，也是为了方便下载，这就是GIT的开源精神！
  > PY中文之路，必须有PY科普在前，这时文化的趋同必定成为事实，这不是二选一的过程。 :pray:

### [python 爬虫的 4 个实例](https://blog.csdn.net/weixin_42515907/article/details/88083440) 【[笔记](https://gitee.com/yuandj/siger/issues/I3GFMC#note_8184919_link)】

```
import requests
url = "https://item.jd.com/3112072.html"
try:
    r = requests.get(url)
    r.raise_for_status()
    #查看状态信息，返回的是200，说明返回信息正确并且已经获得该链接相应内容。
    r.encoding = r.apparent_encoding
    #查看编码格式，这个格式是jbk，说明我们从http的头部分已经可以解析出网站信息。
    print(r.text[:1000])
except:
    print("爬取失败")
```

1. [replace the url success.](https://gitee.com/yuandj/siger/issues/I3GFMC#note_8187121_link) [`url = "https://ludii.games/library.php"`](https://ludii.games/library.php?region=0&period=0&category=0&gameName=&onlyLudiiGames=0&onlyDLPGames=0)
2. [output all txt success. `print(r.text)`](https://gitee.com/yuandj/siger/issues/I3GFMC#note_8187178_link)
3. 查语法：[py（ for 循环）](https://gitee.com/yuandj/siger/issues/I3GFMC#note_8187234_link)| [py 命令行参数](https://gitee.com/yuandj/siger/issues/I3GFMC#note_8187264_link) | [py string to int](https://gitee.com/yuandj/siger/issues/I3GFMC#note_8187297_link)

```
import sys
import requests
url = "https://ludii.games/library.php"
f = open(sys.argv[3], 'w+')
len = int(sys.argv[1]);
step = int(sys.argv[2]);
print( len, step );
try:
    r = requests.get(url)
    r.raise_for_status()
    #查看状态信息，返回的是200，说明返回信息正确并且已经获得该链接相应内容。
    r.encoding = r.apparent_encoding
    #查看编码格式，这个格式是jbk，说明我们从http的头部分已经可以解析出网站信息。
    
    for i in range(len):
         print(r.text[(step*i):(step*i+step)],file=f)

except:
    print("爬取失败")
```

4. **BUG:** [通过命令行循环递增输出的网页尺寸有了瓶颈 上限是15.](https://gitee.com/yuandj/siger/issues/I3GFMC#note_8187326_link)

   - [将输出文件的操作 放到 之后](https://gitee.com/yuandj/siger/issues/I3GFMC#note_8187510_link)  **错误信息是文件写入错。** 
   - [解决python3 UnicodeEncodeError: 'gbk' codec can't encode character '\xXX' in position XX](https://blog.csdn.net/jim7424994/article/details/22675759) 【[笔记](https://gitee.com/yuandj/siger/issues/I3GFMC#note_8187826_link)】
   - [转码就成拉。](https://gitee.com/yuandj/siger/issues/I3GFMC#note_8187866_link) 居然能发现语法错误 `Did you mean: 'encode'?`
   - [继续改 ENCODE](https://gitee.com/yuandj/siger/issues/I3GFMC#note_8187897_link) `print(htmlBytes.encode('utf-8'),file=f );`  **回车都消失拉。** 


```
RES=r.text
RES=RES.encode('utf-8')

wfile=open(f,r'wb')
wfile.write(RES)
wfile.close()

print(r.text);
```

5. [Python解决抓取内容乱码问题（decode和encode解码）](https://blog.csdn.net/w_linux/article/details/78370218) 【[笔记](https://gitee.com/yuandj/siger/issues/I3GFMC#note_8188069_link)】

   - [转成 UTF-8 后 写入二进制文件就可以拉。](https://gitee.com/yuandj/siger/issues/I3GFMC#note_8188069_link)

6. [`grasspy spider.py ludiigames.txt`  **成功** ](https://gitee.com/yuandj/siger/issues/I3GFMC#note_8188384_link)

7. **[替换中文关键字](https://gitee.com/yuandj/siger/issues/I3GFMC#note_8188532_link) 成功** 

8. **[替换变量名](https://gitee.com/yuandj/siger/issues/I3GFMC#note_8188744_link) 成功** 

   - [中间的插曲，参数的变量名没有使用，导致新变量未生成。](https://gitee.com/yuandj/siger/issues/I3GFMC#note_8188759_link)
   - `读 = requests.get(url)` [为啥没有提示 url 没有发现呢？](https://gitee.com/yuandj/siger/issues/I3GFMC#note_8188764_link)

```
导入 sys
导入 requests
网址 = "https://ludii.games/library.php"
文件名 = sys.argv[1]
试:
    读 = requests.get(网址)
    读.raise_for_status()
    #查看状态信息，返回的是200，说明返回信息正确并且已经获得该链接相应内容。
    读.encoding = 读.apparent_encoding
    #查看编码格式，这个格式是jbk，说明我们从http的头部分已经可以解析出网站信息。

捕:
    打印("爬取失败")

临时=读.text
临时=临时.encode('utf-8')

写入文件=打开(文件名,r'wb')
写入文件.write(临时)
写入文件.close()

打印(读.text);
```

# [从中文编程到自然语言编程](https://gitee.com/yuandj/siger/issues/I3GFMC#note_8189156_link)

这个成果距离自然语言编程自然还有相当的距离，在中文编程主打的教学场景，也多少有些磕磕绊绊，有种 “假洋鬼子” 的嫌疑，的确存在这样一英文语言思考的问题，这和英语教学是异曲同工的。多年码字的结果，已经渐渐能使用编程关键字来梳理设计思路，并能多少用英文表达一些情绪。这就是潜移默化的文化熏染。文化是要有载体的。

标题的感慨还是以教学场景来思考的，也就是在完全没有英语基础只有自然语言母语的中国儿童，如何能用自然语言编程（或者说控制机器）应该是我们（目前还只是我自己）的一个想法，就好比编程思维的培养，本身就是一个伪命题，多少和逻辑思维混淆了。在智力运动圈儿就有同样的类似的问题，比如挫折教育，比如逻辑思维，大家都在打这个牌，而多少人会站在儿童的角度思考，真正的教育角度。经常顾左右而言他的，突出什么 “棍棒教育” “虎爸狼妈” 什么的。教育效果是一个技术活儿，而思考它时，出发点和方向决定了我们能思考到多大的深度。

当我们能够用自然语言描述前面的程序的时候，并且，机器还能通过自然语言接口转变成可以直接控制它的指令，我想编程教育就可以终结它的使命拉。我们这些码农也可以收起锄头，等着机器人给咱们收庄家拉。所以，我将下面的学习笔记，定位到自然语言编程的范畴。当然，我对前面的代码片段的改进意见，还是有的，就是函数名，对象描述中文化。

如果是一个全新的库或者对象，在定义的时候，就使用了中文变量名和关键字，就会好很多，但像这样在前人基础上盖楼的情况，中英混杂的确是个辣手的问题，我想借助IDE实现这个方案，能够自动翻译，而不是提示，并且在编译过程提示方面，将中英互译的各种情况，尽量地反映出来，当用户量到了一定水平，一定可以达到一个很好的中间状态，用中文关键字思考，这样距离中文自然语言编程就更进一步拉。这或许可以成为当下“中文编程”的一个阶段使命。

水平有限，时间有限，抛砖引玉，后面转入自然语言编程的研学之旅。给 @[吴烜](https://gitee.com/zhishi) 老师的礼物（虽然迟了些），正式开始准备中...

- [bing `中文自然语言编程`](https://gitee.com/yuandj/siger/issues/I3GFMC#note_8189230_link)

  - [用中文实现计算机语言，真的无法实现吗?](https://www.zhihu.com/question/38398662)【[笔记](https://gitee.com/yuandj/siger/issues/I3GFMC#note_8189500_link)】

    > 有一个设想，请专业人士批评。
设计中文的计算机语言，包括26个英文字母和数字，标点符号，汉字只挑选单字，而且发音尽量不同，给这些特定的汉字定义。比如始，终，从，到，加，减等等。能表述同样意思的汉字尽量选用发音不同的，因为输入的时候，我们用专门的汉字编程键盘。
汉字编程键盘，自带拼音输入法，优先输入特定的编程汉字。比如打jia直接出来""加""这个汉字。
标点统一用英文标点，省去切换标点符号的麻烦。

  - **L脚本语言** ：[面向中文的自然语言编程](https://blog.csdn.net/itmes/article/details/45458023) 【[笔记](https://gitee.com/yuandj/siger/issues/I3GFMC#note_8189837_link)】

    | C语言代码 | L脚本语言 |
    |---|---|
    | int num ;<br>num =10;<br>num=num+1;<br>printf(“%d”,num); | 定义一个整数num;<br>给整数num赋值为10;<br>将num的值加1重新赋值给num;<br>打印输出num; |

- [bing `自然语言编程`](https://gitee.com/yuandj/siger/issues/I3GFMC#note_8190256_link)

  - [科学网—GANCoder自动编程：从自然语言到代码 - 张岩峰的博文](https://blog.sciencenet.cn/blog-3370725-1203983.html) 【[笔记](https://gitee.com/yuandj/siger/issues/I3GFMC#note_8190449_link)】[个人公众号：大数据学术文摘](https://mp.weixin.qq.com/s/VRTqz9CuuQF2SjCuWKc54w)

    > 2019-10-29 · 1 自动编程“自动编程”这个名字听起来就让人浮想联翩，难道人工智能真的要走程序员的路，让程序员无路可走？本文的“自动编程”是一个从自然语言到编程语言的翻译任务，也就是

    **作者：[张岩峰](http://faculty.neu.edu.cn/cc/zhangyf/)** ，东北大学教授，博士生导师，东北大学与美国麻省州立大学联合培养博士，中国计算机学会CCF高级会员，数据库专委会委员，大数据专委会通讯委员。主要研究方向为大数据处理与挖掘、大规模机器学习系统、并行与分布式系统。曾于2012年5月-9月在惠普实验室（HP Labs）做实习研究员，于2016年12月-2017年12月赴美国俄亥俄州立大学做访问学者。承担国家自然科学基金、国家重点研发计划子课题、辽宁省重点研发计划等多项国家和省部级科研项目，承担CCF-华为数据库创新研究计划、阿里巴巴达摩院创新研究计划、华为中央研究院项目等校企合作项目。在SIGMOD、VLDB、ICDE、PPoPP、SOCC、ICDCS、HPDC、《TPDS》、《TKDE》等计算机学会推荐A/B类期刊会议上发表论文多篇。曾获云计算顶级国际会议ACM SOCC的优秀论文奖、全国数据库学术会议NDBC最佳论文奖、CCF大数据学术会议最佳学生论文奖、辽宁省科技进步奖等奖励，指导学生获全国高校云计算创新应用大赛一等奖/最佳指导教师奖、辽宁省优秀硕士学位论文等奖励。

    - [GANCoder自动编码：从自然语言到代码_zhuiyunzhugang的 ...](https://blog.sciencenet.cn/blog-3370725-1203983.html)

      > 2019-10-22 · 1自动编程“自动编程”这个名字听起来就让人浮想联翩，难道人工智能真的要走程序员的路，让程序员无路可走？本文的“自动编程”是一个从自然语言到编程语言的翻译任务，也就是...

  - [CodeBERT: 面向编程语言和自然语言的预训练模型|代码](https://www.163.com/dy/article/FPSO1ISA0511DPVD.html) ... 【[笔记](https://gitee.com/yuandj/siger/issues/I3GFMC#note_8190386_link)】
  
    > 2020-10-26 · 自然语言处理领域预训练模型的成功，也推动了多模态预训练的发展，比如ViLBERT (Lu et al., 2019), VideoBERT (Sun et al., 2019)等。在本文中，我们提出了CodeBERT，

    在本工作中，我们提出了第一个面向编程语言和自然语言的预训练模型，并且在下游的自然语言代码检索，代码文档生成任务上，我们的模型均取得了SOTA的效果。另外，我们构造了第一个NL-PL Probing数据集来研究预训练模型学到了哪种类型的知识。虽然我们的模型已经取得了很好的效果，但也有很多潜在的方向值得进一步研究，比如在预训练过程加入与生成相关的目标函数，加入编程语言的AST结构信息等。

### 彩蛋

- [Berkeley观点：人工智能系统研究的挑战](https://mp.weixin.qq.com/s/JOA5w_Fy_TxcVTD58pLOJQ) 【笔记】
  - [DavidPatterson](https://images.gitee.com/uploads/images/2022/0106/203228_1df081e3_5631341.png), RISC（精简指令集计算机）的发明者，美国科学院工程院两院院士，计算机历史博物馆成员，ACM/IEEE/AAAS Fellow，ACM杰出服务奖得主。
- [达摩院王刚](https://images.gitee.com/uploads/images/2022/0106/203547_ade15485_5631341.png)离职的这一刻起，[AI落地创业](https://images.gitee.com/uploads/images/2022/0106/203526_8d8cd948_5631341.png)的新时代启幕 ([小蛮驴](https://images.gitee.com/uploads/images/2022/0106/203502_ac046101_5631341.png)) 【[全文](https://mp.weixin.qq.com/s/KKnFbbgclzXj0910_5JLMQ)】
- [华工安鼎加入欧拉开源社区](https://mp.weixin.qq.com/s/PEXs9wIkGtVKPfiWgkt4LA)

### 斐波那契2.py

- [斐波那契.py](https://gitee.com/Program-in-Chinese/overview/blob/master/%E7%A4%BA%E4%BE%8B%E4%BB%A3%E7%A0%81/%E6%96%90%E6%B3%A2%E9%82%A3%E5%A5%91.py)

```
# 运行: $ python3 斐波那契2.py

n = 1
n1 = 1

def 斐波那契(v,v1):
    n2 = v+v1;
    return n2;

print(n);
print(n1);

for 索引 in range(1,169):
    n2 = 斐波那契(n,n1);
    print(n2);

    n = n1;
    n1 = n2;
```

- [斐波那契2.py 输出结果](https://gitee.com/yuandj/siger/issues/I3GFMC#note_8193155_link) 被作为本期封面元素。

- 提纲 [编辑工作开始](https://gitee.com/yuandj/siger/issues/I3GFMC#note_8193173_link) 【[封面](https://images.gitee.com/uploads/images/2022/0107/022513_ab9ac636_5631341.jpeg)】

  《[中文化 PYTHON](https://gitee.com/yuandj/siger/issues/I3GFMC)》【[正文](https://gitee.com/yuandj/siger/issues/I3GFMC#note_8198017_link)】
  - [重温初见木兰编程语言的那个战场](https://gitee.com/yuandj/siger/issues/I3GFMC#note_4752627_link)
  - [python语言支持中文(函数)吗?](https://gitee.com/yuandj/siger/issues/I3GFMC#note_4752653_link)
  - [欢迎访问草蟒（Python 中文版）3.8.0 源代码](https://gitee.com/yuandj/siger/issues/I3GFMC#note_8176606_link)

  [用草蟒写一个中文爬虫抓个网页](https://gitee.com/yuandj/siger/issues/I3GFMC#note_8176804_link) ([2](https://gitee.com/yuandj/siger/issues/I3GFMC#note_8188384_link)) 【[正文](https://gitee.com/yuandj/siger/issues/I3GFMC#note_8198381_link)】
  - 用 [PIP](https://gitee.com/yuandj/siger/issues/I3GFMC#note_8179779_link) [安装 python + selenium 环境](https://gitee.com/yuandj/siger/issues/I3GFMC#note_8176823_link) ([2](https://gitee.com/yuandj/siger/issues/I3GFMC#note_8179488_link))【[正文](https://gitee.com/yuandj/siger/issues/I3GFMC#note_8204815_link)】
  - [解决 Python 抓取内容乱码问题](https://gitee.com/yuandj/siger/issues/I3GFMC#note_8188069_link)【[正文](https://gitee.com/yuandj/siger/issues/I3GFMC#note_8206324_link)】

  [从中文编程到自然语言编程](https://gitee.com/yuandj/siger/issues/I3GFMC#note_8189156_link) 【[正文](https://gitee.com/yuandj/siger/issues/I3GFMC#note_8206585_link)】
  - [CodeBERT: 面向编程语言和自然语言的预训练模型](https://gitee.com/yuandj/siger/issues/I3GFMC#note_8190386_link)
  - [GANCoder自动编程：从自然语言到代码](https://gitee.com/yuandj/siger/issues/I3GFMC#note_8190449_link)