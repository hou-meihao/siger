![输入图片说明](https://images.gitee.com/uploads/images/2021/0719/182908_84e60a85_5631341.jpeg "新青年编辑手册.jpg")

向前辈们致敬！[新青年，新时代！](https://gitee.com/yuandj/siger/issues/I3MXUP#note_5895977_link)
 :pray:  :pray: :pray: :pray: :pray: :pray: :pray: :pray: :pray:

《SIGer 编辑手册》目录

- [寄语未来](#%E5%AF%84%E8%AF%AD%E6%9C%AA%E6%9D%A5)
- [小编示范](#%E5%B0%8F%E7%BC%96%E7%A4%BA%E8%8C%83-%E5%A6%82%E4%B8%8B)
- [选题制度](#%E9%80%89%E9%A2%98%E5%88%B6%E5%BA%A6)

---

## 寄语未来

 > 这两天，我会写两篇 SIGer 编辑手册，以及 FSF 自由软件 精神和开源治理的 专题。为石榴派学习社区提供学习方法和北向指引的内容。

这是我写给 @xilongpei 老师的工作计划，其中一篇已经打好腹稿 以 RMS 《[自由软件与教育的关系](https://gitee.com/yuandj/siger/issues/I3DLD8#note_5110108_link)》为题。而 SIGer 编辑手册，就是分享我个人的 SIGer 编委的心路，为新青年们提供一点点借鉴。看着 @ouyanghaitao 阳光海涛的《[自荐信](https://gitee.com/yuandj/siger/issues/I3MXUP#note_5014726_link)》时光恍如昨夜，同一时刻 海涛 也发出了 石榴派 的第一个 PR "!1 [提交石榴派PC开发文档](https://gitee.com/shiliupi/shiliupi99/pulls/1)"。而提到的两位，都是《[南征北战](https://gitee.com/yuandj/siger/blob/master/%E7%AC%AC10%E6%9C%9F%20%E5%8D%97%E5%BE%81%E5%8C%97%E6%88%98%EF%BC%88%E4%B8%8A12%EF%BC%89.md)》专题的主人公。为我见证 “[用 RISC-V 赋予的全栈能力，打破条条框框，轻装前进！](https://gitee.com/yuandj/siger/blob/master/%E7%AC%AC10%E6%9C%9F%20%E5%8D%97%E5%BE%81%E5%8C%97%E6%88%98%EF%BC%88%E4%B8%8B456%EF%BC%89.md#%E5%85%AD%E7%94%A8-risc-v-%E8%B5%8B%E4%BA%88%E7%9A%84%E5%85%A8%E6%A0%88%E8%83%BD%E5%8A%9B%E6%89%93%E7%A0%B4%E6%9D%A1%E6%9D%A1%E6%A1%86%E6%A1%86%E8%BD%BB%E8%A3%85%E5%89%8D%E8%BF%9B)”。

我的计划就是从 “选题” 表入手，从我的涉猎开始介绍如何将个人学习和分享结合起来，成长为一个合格的 SIGer 编委。我想它一定要在未来的某个时刻，完成 “实习期” 正式成为一名真正的 开源人。

> 我们共同的理想就是， _所有人都具有分享之精神，“ **一切皆可开源**  :pray: ”_ 

![0](https://images.gitee.com/uploads/images/2021/0719/120518_0528a321_5631341.png "屏幕截图.png") ![1](https://images.gitee.com/uploads/images/2021/0719/120555_39967ecb_5631341.png "屏幕截图.png") ![4](https://images.gitee.com/uploads/images/2021/0719/120629_a691c9cf_5631341.png "屏幕截图.png")  
![6](https://images.gitee.com/uploads/images/2021/0719/121218_2dab20f1_5631341.png "屏幕截图.png") ![9](https://images.gitee.com/uploads/images/2021/0719/131239_46371f75_5631341.png "屏幕截图.png") ![10](https://images.gitee.com/uploads/images/2021/0719/120742_81a966ca_5631341.png "屏幕截图.png")  
![18](https://images.gitee.com/uploads/images/2021/0719/131354_79de92b2_5631341.png "屏幕截图.png") ![RV1](https://images.gitee.com/uploads/images/2021/0719/131603_5d5a92a9_5631341.png "屏幕截图.png") ![RV2](https://images.gitee.com/uploads/images/2021/0719/131733_82d442dd_5631341.png "屏幕截图.png")  
![rv3](https://images.gitee.com/uploads/images/2021/0719/131825_41ef2967_5631341.png "屏幕截图.png") ![rv4](https://images.gitee.com/uploads/images/2021/0719/131941_b2ac111a_5631341.png "屏幕截图.png") ![rv5](https://images.gitee.com/uploads/images/2021/0719/132024_50997dd9_5631341.png "屏幕截图.png")  
![rv5.2](https://images.gitee.com/uploads/images/2021/0719/132301_db9bba6e_5631341.png "屏幕截图.png") ![rv5.3](https://images.gitee.com/uploads/images/2021/0719/132421_cfe5426f_5631341.png "屏幕截图.png") ![rv6](https://images.gitee.com/uploads/images/2021/0719/132633_1d49dd25_5631341.png "屏幕截图.png")  
![rv7](https://images.gitee.com/uploads/images/2021/0719/132919_8ac0a820_5631341.png "屏幕截图.png") ![rv8](https://images.gitee.com/uploads/images/2021/0719/182703_a74dd553_5631341.png "屏幕截图.png") ![rv9](https://images.gitee.com/uploads/images/2021/0719/235151_236f31b9_5631341.png "屏幕截图.png")

一共16幅作品（作业），加上《开源共治》《SIGer编辑手册》正好18幅，和新青年的刊号18暗合，袁老师也毕业了，这是我们共同的成人礼。既然是作业，老师就是同学们啦。我来细说下每份作业中流出的成长轨迹，也就是我的学习心得啦。用我前天给火焰棋实验室的新青年分享的主题：未来不再分师生，只有共同进步的同学，年龄有差别，共同奋进的心是可以没有差别的，袁老师只是一个称呼，多少体现了好为人师的我，但我手中擎着的 “石榴派” 就是我最好的作业，它是开源社区共同孵化出的成果，也将成为载体，实现开源科普的使命。

> “唯有发心，动念，获得了所有人的支持，才能成功，  
> 而这份成功，将属于社区的每一个人，  
> 因为这是所有人愿力的合集，代表了社区的方向。”   
> ———— 送给未来的 “开源人”

---

[Hello, openEuler!](https://gitee.com/flame-ai/siger/blob/master/%E7%AC%AC0%E6%9C%9F%20Hello,%20openEuler!%20%E6%8B%A5%E6%8A%B1%E6%9C%AA%E6%9D%A5.md) 我要感谢这份邀请，让我与开源邂逅，软件里的巨擎 OS 的开源就是表率。而受邀的原因就是我对 民族棋 文化的执着，现在看来有了答案。我用 [创刊第0期](https://gitee.com/flame-ai/siger/blob/master/%E7%AC%AC0%E6%9C%9F%20Hello,%20openEuler!%20%E6%8B%A5%E6%8A%B1%E6%9C%AA%E6%9D%A5.md) 暗合 混元之初，两极互绕的图形，而视角变换则与另一份意义 佛眼双鱼 的智慧关联。万事万物本就相关，只是身在其中的我们并不自知。

开篇即以《[迎接我们未来的生活方式](https://gitee.com/flame-ai/siger/blob/master/%E7%AC%AC1%E6%9C%9F%20%E8%BF%8E%E6%8E%A5%E6%88%91%E4%BB%AC%E6%9C%AA%E6%9D%A5%E7%9A%84%E7%94%9F%E6%B4%BB%E6%96%B9%E5%BC%8F.md)》为主题开启了 SIGer 的探讨，快乐生活 [“趣” 无尽](https://talk.quwj.com/topic/2119)，让我邂逅了 “一个人就是一个团队” 的[稚辉君](https://gitee.com/yuandj/siger/issues/I3ETSD)，他是 [石榴派](https://gitee.com/shiliupi/shiliupi99) 还愿之旅的目的地，因为他的旧作成为了 [石榴派](https://gitee.com/shiliupi/shiliupi99) 私有云的原型。而从 [Better RISC-V For Better Life](https://gitee.com/flame-ai/siger/blob/master/%E7%AC%AC4%E6%9C%9F%20Better%20RISC-V%20For%20Better%20Life.md) 的发愿开始，一直到受邀 RISC-V 中国峰会，如愿见到了他，并获赠他的 NanoPI，生活处处有惊喜。《[我们的未来是星辰大海](https://gitee.com/yuandj/siger/issues/I3C4HI)》也是所有人一直在书写的 “去远方” 的主题，SIGer 也不能免俗，就连即将完成的 [RMS 专题](https://gitee.com/yuandj/siger/issues/I3DLD8#note_5109877_link)，开源祖师 自由软件世界的开创者都有 [以小行星命名](https://gitee.com/yuandj/siger/issues/I3DLD8#note_5110107_link) 的纪念。

“饮水思源” 我喜欢这个名字，她是正在为即将组织的 “[石榴派漂流计划](https://gitee.com/shiliupi/shiliupi99/blob/master/MagSi/Issue1:%20The%20Personal%20Programmable%20Computer%20-%20Shiliu%20Pi%2099.md)” 线下路演奔忙的志愿者。与 [SIGer](https://gitee.com/flame-ai/siger) 同源，因为石榴派的缘起到发展的每一次行动都怀着这份感恩之心。回报社区，就是每一位开源人的责任，这是分享之心的起点。《[欧拉指北](https://gitee.com/yuandj/siger/blob/master/%E7%AC%AC10%E6%9C%9F%20%E5%8D%97%E5%BE%81%E5%8C%97%E6%88%98%EF%BC%88%E4%B8%8A12%EF%BC%89.md)》就是对《[欧来指北](https://gitee.com/flame-ai/siger/blob/master/%E7%AC%AC0%E6%9C%9F%20Hello,%20openEuler!%20%E6%8B%A5%E6%8A%B1%E6%9C%AA%E6%9D%A5.md)》的感谢，一则《[南征北战](https://gitee.com/yuandj/siger/blob/master/%E7%AC%AC10%E6%9C%9F%20%E5%8D%97%E5%BE%81%E5%8C%97%E6%88%98%EF%BC%88%E4%B8%8A12%EF%BC%89.md)》的历史大片，与社区如火如荼地发展之势完全一致，这就是我学习成果的一次表现，也希望年轻人，在 [SIGer](https://gitee.com/flame-ai/siger) 科普的活动中，能更全面地了解即将面对的社区是什么样子。这时的石榴派 RiscV 已经萌芽了，开篇的两位主人公就是这期邂逅的。

接下来的故事就是 [石榴派](https://gitee.com/shiliupi/shiliupi99) 的高光时刻啦，[1](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/RV4Kids%201st%20%E3%80%8A%E6%98%9F%E8%BE%B0%EF%BC%8C%E6%9C%BA%E9%81%87%EF%BC%8C%E4%BD%BF%E5%91%BD%E3%80%8B1.1.md)/[2](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/Shiliu%20PI,%20Shiliu%20Silicon,%20Shiliu%20Si,%20%E7%9F%B3%E6%A6%B4%E6%A0%B8.md)/[3](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/CNRV%202021.6.%2021.%20-%2022.md)/[4](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/%E4%B8%80%E7%94%9F%E4%B8%80%E8%8A%AF3.md)/[5](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/RV4kids%205th%20FPGA%20study.md)/[6](https://gitee.com/shiliupi/shiliupi99/blob/master/MagSi/Issue1:%20The%20Personal%20Programmable%20Computer%20-%20Shiliu%20Pi%2099.md)/[7](https://gitee.com/RV4Kids/RVWeekly/issues/I40Z0P) 以及两期海报 凡事图个长长久久的好口彩。

如果说耕耘和回报，就应该是 SIGer 头三个月的耕耘，在 3.14 π日感谢树莓派基金会后，近三个月拥抱 RISC-V 后，锁定 “[可编程全栈个人计算机](https://gitee.com/shiliupi/shiliupi99/blob/master/MagSi/Issue1:%20The%20Personal%20Programmable%20Computer%20-%20Shiliu%20Pi%2099.md)” 就是我的回报啦。也是袁老师最满意的答卷。而这要感谢 [RMS](https://gitee.com/yuandj/siger/issues/I3DLD8#note_5109877_link) 精神领袖给我的指引，将自由软件和教育的使命相结合，为 [石榴派](https://gitee.com/shiliupi/shiliupi99) 指明了方向 —— 全栈赋能学习者，[石榴派](https://gitee.com/shiliupi/shiliupi99) 为科普而生！虽然将子品牌 [RV4Kids](https://gitee.com/RV4Kids/RVWeekly/tree/master/RV4Kids) 替换，但不能磨灭我心中对 [RVWeekly](https://gitee.com/RV4Kids/RVWeekly/) [小孔老师](https://gitee.com/lj-k)的感谢，是他为我打开了一扇窗，从他字里行间对 [PLCT 实验室](https://gitee.com/RV4Kids/RVWeekly/issues/I3ZTJH)的肯定，到我代表青少年科普力量站在 [RISCV 中国峰会](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/CNRV%202021.6.%2021.%20-%2022.md)讲台上演讲，是整个社区擎着 [石榴派](https://gitee.com/shiliupi/shiliupi99) 交到我手中的，而我身后是你们，掌握未来的新青年们！无论是 [石榴核](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/Shiliu%20PI,%20Shiliu%20Silicon,%20Shiliu%20Si,%20%E7%9F%B3%E6%A6%B4%E6%A0%B8.md) 还是 [FPGA](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/RV4kids%205th%20FPGA%20study.md) 的星辰大海，无论是 [一生一芯](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/%E4%B8%80%E7%94%9F%E4%B8%80%E8%8A%AF3.md) 的国之担当，还是 [IC wiki](https://gitee.com/RV4Kids/RVWeekly/issues/I40Z0P) 的浩瀚知识地图等待你们畅游。我们都永远在你们身后，你们就是时代的新青年，后浪们，加油！

最后，我用昨天刚刚收到的 [RISCV 中国峰会](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/CNRV%202021.6.%2021.%20-%2022.md)志愿者整理的我的演讲稿，作为对《新青年》创刊的贺词吧！

[![输入图片说明](https://images.gitee.com/uploads/images/2021/0720/194100_b0a2cf8b_5631341.png "屏幕截图.png")](https://www.bilibili.com/video/BV1qV411H7j9)

POSTER - 石榴派 - 袁德俊 - 第一届 [RISC-V 中国峰会](https://space.bilibili.com/1121469705)  
https://www.bilibili.com/video/BV1qV411H7j9

> 大家好，我有幸成为第一个演讲嘉宾。  
非常感谢大家来听我的演讲。  
今天我给大家带来两个作品。  
第一个叫做：开源的一个RISC-V期刊, 叫做 RV4kids。kids 是指青少年的意思。  
另外一个就是，我手里拿的一个很小的开发板（石榴派）：  
它有三部分组成：
> 1. 核呢是用 FPGA 跑的 RocketChip 大家都知道。
> 2. 它的中间层 （操作系统）来自 Linux 社区 叫做 OpenEuler, 从某种意义上我是代表 openEuler 社区来出席的
> 3. 就是它的上层应用（GNUchess）。基于人工智能的一个专业分支，叫做，计算机博弈（AlphaGo 大家都知道）。我本人还有一个身份，就是人工智能学会机器博弈专委会委员，负责科普工作。

> 所以，这两个作品的目标，是面对青少年。  
我讲一下青少年的范围啊：高中生和大学一、二年级。

> 这张图（海报 POST）呢，在 F4 展厅有我一个战区，有我的板子可以介绍，欢迎大家来看看。

  ![输入图片说明](https://images.gitee.com/uploads/images/2021/0720/194744_417b2ec2_5631341.jpeg "RV4KIDS（石榴派）RV2021中国峰会 海报 999.jpg")

> 大家一听到青少年，高中生，都会说：“RISC-V太难了”   
那我举个例子。我们计算机博弈实际有一个十五年的竞赛。  
10年前（我们）那时的环境不是特别好。  
我是三年前，带学生参加的。  
去年高中的学生已经拿到了大学生竞赛的前三甲。  
所以说是没有问题的，只要提供环境。  
那我们让青少年参加，一定要给他一个东西，就是我手里的（开发板）

> 请大家记住这两个名字：
> - 1个是：RV4kids
> - 第2个名字：我手里的（开发板）叫做 “石榴派”

> 我希望我们 RISC-V 社区能够像 石榴籽 一样，仅仅团结。  
祝 RISC-V 长盛，长长久久。谢谢大家。

> 我的名字叫 袁德俊，  
火焰棋实验室，青少年科普公益组织。  
谢谢大家！

---

## [小编示范](https://gitee.com/yuandj/siger/issues/I3MXUP#note_5889931_link) 如下：

![输入图片说明](https://images.gitee.com/uploads/images/2021/0719/141854_4e83fa50_5631341.png "屏幕截图.png")

@high-temperature-ingot

### 热门项目

- 欧来指北
Forked from [FlameAI](https://gitee.com/flame-ai) / [欧来指北](https://gitee.com/flame-ai/hello-openEuler)
  > 《开源指北》是Gitee社区集体创作的，为开源新手创作的保姆级手册。但第一次登陆开源大陆的少年，依然有大量细致到具体操作的问题，挡在他们面前去拥抱这属于未来的世界。openEuler社区是开源社区的担当，敞开温暖的环保，拥抱少年人的热情，sim-minzuchess 就是明证。为了回报openEular，更是回报开源社区，我们发心共同撰写一册 少年版 开源指北，并纪念 openEular 的初心，以 hello openEuler 为名，开展工作。将开源世界建造成属于未来的样子。在此发源，天地为证！- 本仓接受【拥抱】欢迎 SIGer 来访 :family:

- SIGer
Forked from [FlameAI](https://gitee.com/flame-ai) / [SIGer](https://gitee.com/flame-ai/siger)
  > 青少年开源活动科普方案 - 定位科普期刊，《SIGer》是创刊刊名。

  - #I3JCUG [心得](https://gitee.com/high-temperature-ingot/siger/issues/I3JCUG) 创建于 2021-04-17 16:48

    > 开源社区，又称开放源代码社区，一般由拥有共同兴趣爱好的人所组成，同时也为网络成员提供一个自由学习交流的空间。由于开放源码软件主要被散布在全世界的编程者所开发，开源社区就成了他们沟通交流的必要途径，因此开源社区在推动开源软件发展的过程中起着巨大的作用。
今天在体验开源文化的时候就了解到了很多，开始可能没有办法找到自己的兴趣。没有关系，你可以尝试从未接触过的事情并用以上方法仔细观察找到自己的兴趣所在。

    > 找到兴趣之后，如何锁定可变现兴趣呢？如果都是感官兴趣则要进一步培养成自觉兴趣。举例：以爱玩桌游为例，可以先和一帮喜欢玩桌游的朋友玩儿，进一步研究各类桌游的技巧及策略，然后成为组织者，聚集更多的人一起参与。

    > 最后，当你找到处在自觉及潜在兴趣后，别忘记为兴趣制定具体计划及实施步骤哦。当你完成每一小步骤时，你就会有一种成就感，而这种成就感会正向激励你继续坚持并更好的完成下一个步骤，一步一步脚踏实地最终走向成功
今天在gitee社区体验到了很多功能，从开始不知道都是什么，到现在可以清楚一些基本功能，可以去fork一些项目，去点赞一些项目。

    > 这样在相互的过程中，互相才会进步，这个圈子才能成长。。。

- 心得
gitee心得 
  - #I3JCR9 [心得](https://gitee.com/high-temperature-ingot/experience/issues/I3JCR9) 创建于 2021-04-17 16:45

- 翻译
  - #I3MV7G [翻译ludii](https://gitee.com/high-temperature-ingot/translate/issues/I3MV7G) 创建于 2021-04-19 22:07

    > Games are described as structured sets of ludemes (units of game-related information). This allows the full range of traditional strategy games from around the world to be modelled in a single playable database for the first time. Ludii is being developed as part of the ERC-funded 
    > 游戏被描述为一组结构化的ludemes(游戏相关信息的单位)。这使得来自世界各地的各种传统战略游戏首次在一个可玩的数据库中建模。是作为ERC资助的一部分而开发的。

    > Additional information about the development API can be found here 
    > 一些有用的提示和例子可以在教程页面上找到。

    > Our game library includes historical information and playable versions of a large number of games. This will eventually include the thousand most important traditional strategy games throughout history. 
    > 我们的游戏库包含大量游戏的历史信息和可玩版本。这最终将包括上千种历史上最重要的传统战略游戏。

    > The Digital Ludeme Project aims to bridge the gap between historical and computational studies of games, by applying modern AI techniques to the available historical and archaeological evidence. 
    > 数字路德姆项目旨在通过将现代人工智能技术应用于现有的历史和考古证据，来弥合游戏的历史研究和计算研究之间的差距。

    > The system will not only model and play games, but will evaluate reconstructions for quality and historical authenticity, automatically improving them where possible, and provide a number of other services.
    > 该系统不仅将建模和玩游戏，而且将评估重建的质量和历史真实性，在可能的情况下自动改进它们，并提供一些其他服务

### 贡献度

![输入图片说明](https://images.gitee.com/uploads/images/2021/0719/143156_8bca7323_5631341.png "屏幕截图.png")

最近一年贡献：8 次 最长连续贡献：1 日 最近连续贡献：1 日

### 动态

 **04.19** 

- 推送了 2 次提交

  - [blesschess/LuckyLudii](https://gitee.com/blesschess/LuckyLudii) [1 次提交 (位于分支 refs/pull/1/head)](https://gitee.com/blesschess/LuckyLudii/commits/refs/pull/1/head?end_date=2021-04-19&start_date=2021-04-19&user=high-temperature-ingot)
  - [blesschess/LuckyLudii](https://gitee.com/blesschess/LuckyLudii) [1 次提交 (位于分支 master)](https://gitee.com/blesschess/LuckyLudii/commits/master?end_date=2021-04-19&start_date=2021-04-19&user=high-temperature-ingot)

- 创建了 1 个任务

  - [翻译ludii](https://gitee.com/high-temperature-ingot/translate/issues/I3MV7G) [Noone/翻译](https://gitee.com/high-temperature-ingot/translate)

- 提交了 1 pull request

  - [【轻量级 PR】：update ludii.games/Home.md](https://gitee.com/blesschess/LuckyLudii/pulls/1). [blesschess/LuckyLudii](https://gitee.com/blesschess/LuckyLudii)

---

## 选题制度

下面的选题是袁老师给自己出的，围绕科普的角度展开，也会成为石榴派赋能的目标。  
作为一个 “制度” 沉淀为 SIGer 编辑的核心素养，予以推荐，以下选题会成为后续专题的实施计划。  
但选题并不能作为最终执行方案，需因时因地推进。(动态选题表移到 [ISSUES](https://gitee.com/yuandj/siger/issues/I3MXUP#note_5918092_link) 中)

#### 选题

| 摘要 | 编号 | 编委 | 主题 | issues |
|---|---|---|---|---|
| 航空航天 | #22 | 空天创客 | CANSAT | [06-29](https://gitee.com/yuandj/siger/issues/I3YEOI) |
| VSLAM | #21 | “庞博” | 机器视觉 | [06-28](https://gitee.com/yuandj/siger/issues/I3Y7Y0) |
| Biochip | #20 | 位育中学 | 《生物芯片》 | [06-25](https://gitee.com/yuandj/siger/issues/I3XQJ5) |
| 生命 | #19 | “梁冬” 尹烨 | 《生命•觉者》 | [04-20](https://gitee.com/yuandj/siger/issues/I3MXZZ) |
| 青春 | #18 | “SIGer” 执行编委 | 《新青年》 | [04-20](https://gitee.com/yuandj/siger/issues/I3MXUP) |
| 数学之美 | #17 | “和乐数学” | 《丘成桐传》 | [04-08](https://gitee.com/yuandj/siger/issues/I3HTQ7) |
| 石榴派 | #16 | “吴烜” | 《中文化 PYTHON》 | [04-05](https://gitee.com/yuandj/siger/issues/I3GFMC) |
| 量子计划 | #15 | “稚辉君” | 《华为天才少年素养》 | [04-02](https://gitee.com/yuandj/siger/issues/I3ETSD) |
| LICENSE | #14 | “卫_sir” | 《开源治理的根基是什么？》 | [03-26](https://gitee.com/yuandj/siger/issues/I3DLD8) |
| DOJO | #12 #13 | “秦风岭” | 《开源道场 - DOJO CHINA的构建》 | [03-24](https://gitee.com/yuandj/siger/issues/I3CST6) |
| ICPC | #11 | “ACM” | 《 ACM-ICPC 未来版 》 | [03-22](https://gitee.com/yuandj/siger/issues/I3CHVL) |
| MARs | #9 | “理想” | 《STEM UK & MARsDAY 3.18》 | [03-19](https://gitee.com/yuandj/siger/issues/I3C4HI) |
| 青创赛 | #8 | 创青春 | 《青少年科技创新奖》 | [03-17](https://gitee.com/yuandj/siger/issues/I3BS3L) |
| ICGA | #7 | “SIGer编委” | 《计算机博弈的国际范儿 LUDII.GAMES》 | [03-14](https://gitee.com/yuandj/siger/issues/I3BAD9) |
| 趣小组 | #6 | "王工" | 《真创客，树莓派实验室》 | [03-14](https://gitee.com/yuandj/siger/issues/I3BAD8) |
| NGO 赋能 | #5 | “三一小王子” | 《公益+ 开源+》 | [03-12](https://gitee.com/yuandj/siger/issues/I3B6P7) |
| 开源社2020 | #3 | “王伟” | 《开源故事会“台湾”“武汉”》 | [03-12](https://gitee.com/yuandj/siger/issues/I3B6MA) |
| STEM | #2 | “谢博” | 《上海的STEM故事》 | [03-12](https://gitee.com/yuandj/siger/issues/I3B6M1) |

#### 其他

从 SIGer 封面移到此，是 SIGer 新里程的需要，被 吾愿 取代。