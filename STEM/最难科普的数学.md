本期提要：

- [最难科普的 **数学** ](https://gitee.com/yuandj/siger/issues/I4SLV9)
- [直观の？傅里叶变换](https://gitee.com/yuandj/siger/issues/I4SLV9#note_8655298_link)
- [李健辉：知识分享，终生相随](https://gitee.com/yuandj/siger/blob/master/%E4%B8%93%E8%AE%BF/%E3%80%90%E6%9D%8E%E5%81%A5%E8%BE%89%E3%80%91%E7%9F%A5%E8%AF%86%E5%88%86%E4%BA%AB%EF%BC%8C%E7%BB%88%E7%94%9F%E7%9B%B8%E9%9A%8F.md)
- [倪光南：中国制造，2025](https://www.bilibili.com/video/BV1vi4y1R79t)
- [SIGer 学习笔记](https://gitee.com/yuandj/siger/issues/I4SLV9#note_8656380_link)

<p><img width="706px" src="https://images.gitee.com/uploads/images/2022/0213/203104_27953da2_5631341.jpeg" "傅里叶变换 Fourier Transform"></p>

> 选择这厚厚的书页做封面，是想穿越到 1822 重温《热的解析理论》，希望看到现在泛黄的书页，赭石色的墨迹记录的数学思想的脉络，同为工科生的我，直到今日才关心起作者的故事，不为谈资，只为那份紧迫感，缺少的一定是要补回来的。和以往的科普文章，试图说明其所以然不同，《[直观の数学](https://www.zhihu.com/column/c_1041689010053341184)》的作者 [李健辉博士](http://www.lijianhui.net) 更会花相当的篇幅介绍这些公式产生的由来和故事，像是在述说数学史，饱满立体。他还使用了最现代的开源工具，以动画的方式展示出来，花费的时间不是码字所能比拟。以下内容，以笔记为主，同学们一定要按图索骥阅读原文，除了领略 python 创造的更多惊奇，还能有 彩蛋 可听哟 :pray:

# [自然科学学科中最难进行科普的有哪些？](https://www.zhihu.com/question/325240281/answer/693123889) @[leekunhwee](https://gitee.com/leekunhwee)

谢邀。

首先想说，所有 **自然科学** 的科普都不容易，而题主提到的数学，构建了几乎所有其他自然科学领域、经济金融领域、 **工程技术** 领域等的理论基础，也是大家最普遍关注的。

本人是工科生，深感工程技术对数学的依赖性。

当学习傅里叶变换时，为了去直观理解它，我整理并总结了傅里叶变换的思路：

- [李狗嗨：如何给文科生解释傅里叶变换？](https://zhuanlan.zhihu.com/p/48305950) <img height="32px" src="https://images.gitee.com/uploads/images/2022/0204/170504_170ff41b_5631341.png">
1560 赞同 · 123 评论

在此科普文撰写的过程中，我发现要理解傅里叶变换中复指数的特性，而前提是得先理解复数，复数相对于实数而言，又引申出了虚数i的概念。所以总结了虚数的概念：

- [李狗嗨：虚数i真的很“虚”吗？](https://zhuanlan.zhihu.com/p/48392273)  <img height="32px" src="https://images.gitee.com/uploads/images/2022/0204/170513_d21288ea_5631341.png">
1145 赞同 · 119 评论

而前面提到的复指数是以e为底指数，我又在纠结为啥是e？所以又总结了e的由来、自然指数的特性，以及与之相对应的 **自然对数** ：

- [李狗嗨：自然底数e怎么就“自然”了？](https://zhuanlan.zhihu.com/p/48391055)  <img height="32px" src="https://images.gitee.com/uploads/images/2022/0204/170522_f920eef9_5631341.png">
2655 赞同 · 193 评论

- [李狗嗨：为什么e^x 的导数是还是其自身？](https://zhuanlan.zhihu.com/p/48678377)  <img height="32px" src="https://images.gitee.com/uploads/images/2022/0204/170539_71501a42_5631341.png">
581 赞同 · 63 评论文章

- [李狗嗨：为什么说"对数"可以延长天文学家寿命？](https://zhuanlan.zhihu.com/p/48393525) <img height="32px" src="https://images.gitee.com/uploads/images/2022/0204/170636_32378b79_5631341.png">
294 赞同 · 34 评论

后来我发现还不够，我想将傅里叶变换中的三角函数与复指数联系起来，必将涉及到欧拉公式，所以我总结了欧拉公式的直观推导：

- [李狗嗨："上帝公式"(欧拉公式)真的神到无法触碰？](https://zhuanlan.zhihu.com/p/48392958) <img height="32px" src="https://images.gitee.com/uploads/images/2022/0204/170642_28f8d2b6_5631341.png">
8629 赞同 · 529 评论

然后，由于欧拉公式中还有一个符号π没解释，所以补充了一下π的由来，及弧度和角度制之间的区别：

- [李狗嗨：古人是如何寻找到π的？](https://zhuanlan.zhihu.com/p/48307582)  <img height="32px" src="https://images.gitee.com/uploads/images/2022/0204/170655_8e9cdd61_5631341.png">
1013 赞同 · 107 评论

- [李狗嗨：一圈为何是360°？](https://zhuanlan.zhihu.com/p/48767522)  <img height="32px" src="https://images.gitee.com/uploads/images/2022/0204/170702_81479eb8_5631341.png">
559 赞同 · 51 评论文章

到此为止，我才仅仅是把一些非常基础概念解释清楚，而这一切结果的原始动机，只是想理解一下什么是傅里叶变换而已。

如想了解更多内容，可以关注我的专栏：

- [直观の数学](​zhuanlan.zhihu.com/c_1041689010053341184)  <img height="32px" src="https://images.gitee.com/uploads/images/2022/0204/170712_5171afdf_5631341.png">

编辑于 2019-05-30 02:40

来源：[知乎](https://www.zhihu.com/question/325240281/answer/693123889)：李狗嗨 

<p><img width="633px" height="396px" src="https://images.gitee.com/uploads/images/2022/0212/214349_a20ae99e_5631341.png"></p>

 **Fourier Transform**  _In mathematics, a Fourier transform (FT) is a mathematical transform that decomposes functions depending on space or time into functions depending on spatial or temporal frequency, such as the expression of a musical chord in terms of the volumes and frequencies of its constituent notes. The term Fourier transform refers to both the frequency domain representation and the mathematical operation that associates the frequency domain representation to a function of space or time._  ([Wikipedia](http://en.wikipedia.org/wiki/Fourier_transform))

> 借用 健辉 老师的开篇，我插播一段儿学习心得，藉由此汇报下对 Fourier Transform 的学习成果。全文 99% 都是引用，只稍微调整了下目标，是给 小小科技迷们，足够  **直观の**  内容，适合所有读者。并且科技时代文理不分家。 :pray:

# [如何给文科生解释傅里叶变换？](https://zhuanlan.zhihu.com/p/48305950) 【[笔记](https://gitee.com/yuandj/siger/issues/I4SLV9#note_8643616_link)】

<p><img width="633px" src="https://images.gitee.com/uploads/images/2022/0211/232058_335b4668_5631341.gif"></p>

正弦波就是一个圆周运动在一条直线上的投影。下面是公式 _formula_ ：

<p><img width="633px" src="https://images.gitee.com/uploads/images/2022/0211/232542_841e8d17_5631341.png"></p>

通过三种信号分别是 方波、锯齿波和三角波，来介绍它们的共同点就是：

 **它们都是由无数个无相位差（假设初试相位角均为零）、且成谐波关系的正弦波构成的周期性信号。** 

### 01、方波 (图片来源: 1ucasvb) 和 02、锯齿波
先看方波的叠加，之后是它们 _formula_ 和比对图。
<p><img width="369px" src="https://images.gitee.com/uploads/images/2022/0211/232916_2566d894_5631341.gif"> <b>时域&频域</b> (<i>频域讨论并未展开，同学们可参考<a href="https://zhuanlan.zhihu.com/p/48305950" target="_blank">原文</a></i> )</p>
<p><img width="369px" src="https://images.gitee.com/uploads/images/2022/0212/221741_3605ed02_5631341.png"> <img width="369px" src="https://images.gitee.com/uploads/images/2022/0211/233345_f78834f5_5631341.png"></p>
<p><img width="369px" src="https://images.gitee.com/uploads/images/2022/0211/232700_8f112ae8_5631341.gif"> <img width="369px" src="https://images.gitee.com/uploads/images/2022/0211/233444_b6ea2088_5631341.gif"></p>
<p><img width="369px" src="https://images.gitee.com/uploads/images/2022/0211/233017_f1e9c09c_5631341.gif"> <img width="369px" src="https://images.gitee.com/uploads/images/2022/0211/233456_a3221a67_5631341.gif"></p>
<p><img width="369px" src="https://images.gitee.com/uploads/images/2022/0211/233128_9ea09e2f_5631341.gif"> <img width="369px" src="https://images.gitee.com/uploads/images/2022/0211/233507_f14b69db_5631341.gif"></p>

### 03、三角波

<p><img width="369px" src="https://images.gitee.com/uploads/images/2022/0212/213851_7f7a4672_5631341.gif"> <img width="369px" src="https://images.gitee.com/uploads/images/2022/0211/234018_0742bd72_5631341.gif"></p>

### 怎样让他们  **直观の**  ？

上面那些是简单信号的傅里叶级数近似。最近，我在学习 3Blue1Brown 的动画引擎 manim ，利用里面的素材制作了用傅里叶级数生成任意图形的动画演示，例如 Github 的吉祥物——章鱼猫（Octocat）：

<p><img src="https://pic2.zhimg.com/v2-c04bf85b1809dbc7bca64c18fd6f0a21_b.webp"></p>

如果你是初学者，可能会以为这个很难，但是实际上这个动画做起来并没有你们想象的那么难，如果有兴趣的话可以去我的知乎专栏看看，有 3B1B 动画制作的详细教程。

- [直观の数学](https://zhuanlan.zhihu.com/p/108839666) 专栏目录388 赞同 · 12 评论文章

### [总结](https://gitee.com/yuandj/siger/issues/I4SLV9#note_8643674_link)

通过上面的例子可以看到，对于满足狄里赫利(Dirichlet)条件的周期信号，可以分解为一组成谐波关系的正弦信号，或者说该周期信号做傅里叶变换可以得到一组傅里叶级数。

对于周期信号，既然知道了其中的各个成分是成谐波关系的，那么频率成分就确定了。所以在不考虑相位差的情况下，问题关键是如何得到这些成谐波关系的正弦信号前的系数（或者说，谐波的幅值，也即是各个成分的大小）。而傅里叶变换的公式恰恰就给了我们解决该问题途径。也就是本文最开始那个公式了。由待分析的周期信号 [公式] ，可以积分得到其中所包含的谐波成分的幅值 [公式] ，而将这些频率成分全部相加则可以重构出原周期信号。

<p><img width="633px" src="https://images.gitee.com/uploads/images/2022/0211/234431_43c55ca9_5631341.png"></p>

有人为了方便理解，将傅里叶级数的求解用下式表达：

![](https://images.gitee.com/uploads/images/2022/0211/234439_b3aec2c5_5631341.png "屏幕截图.png")

(图片来源: betterexplained)

### [傅里叶简介](https://gitee.com/yuandj/siger/issues/I4SLV9#note_8654058_link)

好，明白了“变换”的意义之后，现在看看正经的傅里叶变换。首先，介绍一下大名鼎鼎的傅里叶[[2]](https://zhuanlan.zhihu.com/p/48305950#ref_2)。

Jean-Baptiste Joseph Fourier 21 March 1768 - 16 May 1830 (图片来源: [Wikipedia](https://images.gitee.com/uploads/images/2022/0212/185739_9be97914_5631341.png))

傅里叶的一生很传奇，幼年时父母相继离世，二十多岁毕业后当了数学老师，后又被聘任为巴黎综合理工学院的教授。但他并不是一个安分的人，20岁的血气方刚恰逢当时的法国大革命，他的一些政治行动曾两次险些将其送上断头台，但他也因此获得了器重。

三十岁时傅里叶跟随东征，被任命为下埃及总督，并负责为法军的远征部队提供军火。在此期间，这个教过书、造过反、还背过枪的人竟然还向开罗埃及学院递交了几篇有关数学的论文。内容主要是关于他在三角级数方面的贡献。

<p><img width="633px" src="https://images.gitee.com/uploads/images/2022/0212/185801_c8e8b47c_5631341.png"></p>

远征军失败后，他回国并于1801年被任命为伊泽尔省格伦诺布尔地方长官。到了1807年，傅里叶在研究中发现了一系列成谐波关系的正弦曲线可以用来表示物体内的温度分布。他还声称，“任何”周期信号都可以用一系列成谐波关系的正弦曲线来表示。

他随后向巴黎科学院呈交了这篇名为《热的传播》的论文，主审这篇文章的四个人中。拉克尔华(F. Lacroix) 、蒙日(G. Monge)和拉普拉斯(P. S. de Laplace)都赞成发表这篇论文，但是拉格朗日(J. L. Lagrange)坚持拒绝傅里叶提出的这一套三角级数理论，因为在他看来，三角级数的适用范围及其有限，不可能把具有例如导数不连续的信号表现出来（当然，拉格朗日也并没有说错，因为在用三角级数近似导数不连续信号时，在不可导点附近会出现“吉布斯现象”，但是可以通过近似使两者的能量差为零）。至于为什么要用正弦(余弦)函数近似，是因为三角级数具有很多特殊的性质，例如，它是线性时不变系统的特征函数，也就是说向该系统输入一个正弦(余弦)函数，输出依然是同频率的正弦(函数)，只是幅值和相位改变了(关于幅值、频率、相位后面会说到)，这是其他简单信号（方波、三角波等等）不具备的性质。还有就是三角级数自身构成了完备的正交基空间，该性质使其能够在重构信号的过程中既无泄漏也无冗余。在小波变换出现之前，对人们来说，该完备正交基空间亦是可遇不可求的。


由于拉格朗日的强烈反对，导致傅里叶的这篇论文从未发表。在几次尝试让法国学院接受和出版他的论文后，傅里叶着手撰写他作品的另一个版本。1822年，傅里叶将这套理论写在了他的著作:《热的解析理论》之中。这距离他首次提出该理论已经过去了整整15年。

<p><img width="633px" src="https://images.gitee.com/uploads/images/2022/0212/185821_439552d8_5631341.png"></p>

Théorie analytique de la chaleur

虽然他关于三角级数的论述很有意义，但隐藏在这一问题后面的很多基本概念已经被其他科学家们所发现；同时，傅里叶的数学证明也不是很完善。后来于1829年，狄里赫利(Dirichlet)给出了若干精确的条件，在这些条件下，一个周期信号才可以用一个傅里叶级数表示。

因此，傅里叶实际上并没对傅里叶的数学理论做出什么贡献。然而，他确实洞察出级数表示法的潜在威力，并且由于其断言，大大激励和推动了傅里叶级数问题的深入研究。另外，傅里叶在这一问题上的研究成果比他的任何先驱者都大大前进了一步，这指的是他还得出了关于非周期信号的表示——并非成谐波关系的正弦信号的加权和，而是不全成谐波关系的正弦信号的加权积分。

他的发现对19世纪及之后的数学、物理、化学及各个工程领域都产生了深远影响，他是名字被刻在埃菲尔铁塔的七十二位法国科学家与工程师之中。

<p><img width="633px" src="https://images.gitee.com/uploads/images/2022/0211/231757_ee25e605_5631341.jpeg"></p>

# 《知识分享，终生相随》
—— SIGer 是如何炼成的？

 **[李健辉](http://www.lijianhui.net/)** 
- 机械工程博士 
- 知乎 数学话题 优秀答主

感谢袁老师的盛情邀请，其实，我原本只是想单纯地做知识分享，不涉及任何个人的信息，但既然袁老师多次发出邀请，那我就在这里简单地做个自我介绍吧。我是2021年博士毕业，专业是机械工程，现在是家乡的一名公务员，平时比较忙，但又喜静，所以闲暇的时候爱好是读书。



由于工科的背景，在读研、读硕博期间接触到了大量的数学概念。例如，在《控制工程》中会用到拉氏变换、在《工程测试技术》中会用到傅里叶变换、在《振动力学》中会用到模态分析方法，等等。虽然这些都是工科课程，但里面用到的这些“变换”或“方法”实质上都是纯数学工具。例如，“拉氏变换”和“傅里叶变换”是《复变函数》中的内容，“模态分析方法”说白了就是《高等数学》中的微分方程和《线性代数》中的特征值计算方法的集成应用。

 

硕博期间，我确实遇到了很多疑问，但是要解决疑问，只有搞懂涉及到的一个个基本的数学概念。在这个过程中，我查阅了很多资料，后来在 Better Explained 网站上，我发现了很多数学概念的通俗解释，受益匪浅。但是，当年国内在这方面的科普做得很少，很多工科生对数学基本概念的理解只是“知其然，而不知其所以然”。为了避免遗忘，我在理解了一个数学概念后，就用笔记的形式记录到自己的知乎专栏中。用知乎记录是因为其方便修改，并且其公式编辑器也很好用，不过一开始我并没有意识到这种做法的“正外部性”。

 

直到后来，知乎粉丝越来越多，很多人都评论说在该专栏中学到了很多。而我本人也享受到了分享知识的乐趣，所以就一直坚持了下来，并开始在多个平台分享自己的学习笔记和经验总结，涉及的领域也越来越多，有我自己专业的知识，也有我感兴趣的非专业知识。

 

由于现在已经离开了科研环境，所以我未来的关注点可能将转向社会、经济、文化领域。但不论是在哪个领域，我都会坚持知识的分享，因为知识分享是一种后天获得的精神，这精神一旦拥有，便终生相随。

 

 **什么时候开始喜欢数学的？** 

应该是在硕博期间，在知识分享的过程中渐渐喜欢数学。

 

 **作为一名理科生的爱好都有那些？** 

正是因为我是理工科学生，所以在人文方面较为欠缺，需要补充的知识很多。所以在闲暇的时候，我喜欢看一些历史、人文、经济方面的书调剂一下。当然，还对各种电子产品感兴趣，个人的非必须品开销，80%以上都是花在了电子产品上。

 

 **高考志愿选择？研博研究方向？和兴趣爱好有那些关系？** 

高中时对各种专业真的没有什么概念，当时挑了一个名字最长的，机械设计制造及其自动化，原因就是听起来挺厉害。硕博也是机械专业，硕士是机械制造及其自动化，博士是机械工程。这个可能和兴趣爱好没有什么直接的关系，当时真的是乱选的。

 

 **ALG 奥利给 不等式 的名字是您取的吗？** 

这个不是我取的名字，是在知乎上看到有人提出的。觉得这个名字很有意思，所以就回答了一下知乎上的相关问题。

 

 **倪光南：希望大家群策群力，2025** 
 

> 我国制造业全球市场占比近 30%，但工业软件市场占有率尚不足 6%，深表无奈，深感掣肘，深受其苦，深以为然。

 

 **面对未来的学弟学妹有什么鼓励的话吗？未来的坦途还是艰难险阻报以什么样的决心才能不虚度？** 

尼古拉·奥斯特洛夫斯基在《钢铁是怎样炼成的》一书中说：“人最宝贵的是生命，生命对人来说只有一次。人的一生应当这样度过：当他回首往事时，不会因为碌碌无为而羞愧，也不因虚度年华而悔恨。” 所谓的“不虚度”其实就是“活得有意义”。

从个人层面讲，每个人都会经历各种各样的抉择，除去消极避世的那部分，大多数人眼中为人处世的最高标准无非就是“立德”、“立功”与“立言”，能达到的人少之又少。虽不能至，心向往之。

从社会层面讲，现在的信息渠道比十年前要广泛得多，作为年轻人，要主动去寻求和发现国家、社会当前及未来几十年的需求，并朝着提供这种需求的方向努力，就必不会虚度这一生。

 

 **五轴数控机床为什么是五轴联动而非六轴联动呢？** 

关于欧拉角的补充(无处不在的欧拉，可以出一期专题啦。

 **刀头升降轴和三轴的Z轴不是重复了吗？** 

实际机床并没有“刀头升降轴”这种说法。当然，对于实用的机床，必然会避免冗余轴的出现。

 **5轴机床就到头了吗？可以满足所有场景了吗？有更多轴的吗？六轴机器人，有更多轴的机器人吗？** 

显然是由更多轴的机床、机器人，但是在加工领域，常见的多轴（指三轴以上）机床为五轴机床，因为五轴机床可以实现任意方位的加工，轴数越少越利于机床刚性的保持。而更多的轴的加入往往是为了实现特定的加工工艺、某种特定情形下的避障要求，或更加灵活的运动路径。

 

 **如果自己上手一台桌面的加工设备，有推荐吗？委托加工漫长地等，能自己DIY是创客的乐趣无穷啊** 

如果预算不高，可以入手一个3D打印机，便宜点的一两千就能买一台，对于一些对强度要求不高的小零件，这可能是周期最短的制造设备了。

 

 **能介绍下您的实验室装备吗？垂涎中。。。** 

在加拿大课题组的实验室里有一台高精度的Quaser五轴加工中心和一台车铣复合机床，另外还有两个三轴铣床。我的研究内容多是在那台五轴加工中心上完成的。在国内课题组里，有几套比较先进的测试设备，例如，Kistler的测力仪、Siemens是振动测试仪、Renishaw的激光干涉仪等等。

 

 **《Lemon》是首日文歌, 健辉日语多少级？** 

日语没考级，只是自学了一段时间。

 **是需要看技术资料吗？据传日本的数控机床很利害？国外的技术水平是个什么样的存在？再比如德国。** 

确实是有看日本论文的需要，日本80、90年代的一些好论文是用日文出版的，而且没有对应的英文版做参照，但是往往会在查找参考文献的时候找到这些日文论文，这时就需要一些日语知识才能理解。这也侧面反映了日本、德国等国家在机床加工领域的研究水平。不过从各种文献中也可以看出，日本在机床研究方面更加偏向于应用，而欧美更加偏向于理论计算，所以在工厂中常常能见到一些造型奇特的日本机床，这些机床往往是专用机床，所谓“专用机床”指的是只能造某一种、或某一类产品。并不是说这样不好，因为专用机床一定是针对某一加工工艺做了优化，这样便能够极大地提高加工效率和精度。总体来说，我们的高端机床制造水平还不如日本和欧美的一些国家，相应的工业软件，更是差了一大截，追赶尚需时日。

 

 **《直观的数学》为啥要用日语 の 很多专题都是，还有小语种 外语 の** 

这个只是为了让人看了更能留下印象，另外也能和大多数专栏的名字区分开来。

 

 **嘿，文青！你真的“文艺”吗？** 

健辉除了幽默，文笔也很好，如果不选科技专业，文学会是你的方向吗？

 **平时也一定博览群书，有那些爱好呢？** 

文笔真的不能说是好，博览群书更是谈不上，但是个人对于中华传统文化及历史非常感兴趣，对儒家的文化更是痴迷，所以平时会看这方面的书。理工学科可以培养人的思维方式，人文学科可以塑造人的道德秉性。像四书五经、资治通鉴、唐诗宋词、古文观止等等都是值得反复研读的经典。

 

 **SIGer 编委都会再入坑之初《我的兴趣爱好》？** 

 **很多同学同样也会好奇 健辉 学长的爱好兴趣？** 

 **还有，生活学习日常？能多分享些** 

我这个人比较宅，不怎么喜欢运动（当然得改改，不要学我），除非是有人喊我一起骑行。个人对各种小语种感兴趣，本科的时候自学了韩语，还考了个初级。硕博时期，闲暇时间多是在做各种知识的整理与分享，这便成了我最大的爱好。

### [倪光南：希望大家群策群力，中国制造 2025](https://www.bilibili.com/video/BV1vi4y1R79t) 【[笔记](https://gitee.com/yuandj/siger/issues/I4SLV9#note_8643986_link)】

1. 先生们，大家上午好，很荣幸受邀参加第六届中国制造日活动，由于疫情原因不能前来现场参与，在这里我向此次活动的顺利召开表示热烈的祝贺。总书记强调，制造业是实体经济的基础，必须始终高度重视发展壮大实体经济，抓实体经济一定要抓好制造业，要加强自主创新，发展高端制造，智能制造，推动我国实体经济，有量大转向质强。随着中国工业和制造业的快速发展，在世界上逐渐形成了三大工业区，西欧、北美和东亚。2021年3月，工信部肖亚庆部长在工业举办的新闻发布会上说，中国工业增长值由23.5万亿元，增加到31.3万亿元。

2. 连续11年成为世界最大的制造业大国。制造业占比对世界制造业的贡献比重接近30%。中国庞大的制造业支撑了中国国民经济的发展，同时也对今天我们进入新一轮科技革命、实施数字化转型提出了新的挑战。其中如何迅速的补齐中国在工业软件领域的短板，就是一个亟待解决的问题。众所周知，工业软件是近几十年来伴随着工业发展的过程，逐渐形成的，他即是软件技术在工业领域应用。又是工业技术软件化的产物。基于历史原因，我国在工业软件领域还相当落后。中国作为世界第一大工业国，制造业约占全球的30%，但在工业软件方面市场占比不足6%，该领域长期处于被国外企业垄断的状况。今后中国要制造2025，实现产业技术高级化，达到高质量发展，都需要大力发展中国自己的工业软件。

3. 2021年5月28日，两院院士大会上发表重要讲话指出，科技攻关要坚持问题导向。奔着最紧急、最紧迫的问题去。要从国家急迫需要和长远需求出发，在石油天然气、基础原材料、高端芯片和工业软件等等关键核心技术上全力攻坚。显然，工业软件已被列为国家最紧急、最紧迫的问题，同时关乎国家急迫需求和长远需求。与基础原材料和高端芯片等并重。充分体现了当前发展工业软件的重大战略意义。造成我国工业软件短板的主要原因是中国在发展历史过程中工业化进程的缺失。所以，未来15到30年是中国重塑制造业的技术体系、生存模式、产业形态，推进制造2025战略、实现适合中国国情的 “工业4.0” 的关键时期。

4. 在当前中国的发展中，不过早的强调去工业化并不合适，可能会导致实体经济衰退，不利于制造2025战略发展。从历史上看，科技和产业发展的一个重要的表现形式是工业革命，也就是产业变革。由于科技进步带来的产业变革，实际是向前发展的动力。从第一次工业革命蒸汽时代，到第二次工业革命电气时代，再到第三次工业革命的自动化时代，现在正在进入的新一轮工业革命，在德国称为工业4.0。 是以智能制造为主导，通过充分利用信息物理系统，也就是CPS，实现制造业向智能化转型。因此，智能化和工业化的融合发展是这一轮工业革命的显著特征。这新一轮工业革命，对中国的工业化进程而言，是一次重大的历史性机遇。相应地，我国目前急需的能支撑这一新工业革命的工业软件，应当充分体现智能化和工业化融合的趋势。

5. 我国工业软件虽然起步迟，总体水平低，但是我国具有世界上门类最齐全的工业体系，在广阔的工业领域积累了丰富的经验和应用场景，形成了比较全面的知识体系和人才队伍，这为克服工业软件算法等薄弱环节创造了有利条件。我相信，只要做好发展中国工业软件的顶层设计，发挥工业界的协同努力、开拓创新，一定能够构建起中国自己的工业软件体系，并通过应用实践迭代推广。补齐中国工业软件的短板。为此，我们既要学习发达国家既有的工业软件，但又不能走简单的跟随、模仿路线，而应当积极的引入新兴的人工智能、物联网、大数据等等新一代信息技术，并与中国超大规模应用场景密切结合，以此提升传统的工业软件，使其更适合智能制造的需求。

6. 希望大家为发展中国的工业软件，为推进中国制造，为推进制造业的智能升级，集思广益，群策群力，使我国早日制造2025，谢谢大家。

---

### [自然科学学科中最难进行科普的有哪些？](https://www.zhihu.com/question/325240281/answer/693123889) @[leekunhwee](https://gitee.com/leekunhwee) 【[笔记](https://gitee.com/yuandj/siger/issues/I4SLV9)】

> 首先想说，所有 **自然科学** 的科普都不容易，而题主提到的数学，构建了几乎所有其他自然科学领域、经济金融领域、 **工程技术** 领域等的理论基础，也是大家最普遍关注的。本人是工科生，深感工程技术对数学的依赖性。

- [#I4SL0T 和乐数学II：趣味数学之编程示范](https://gitee.com/yuandj/siger/issues/I4SL0T#note_8553163_link) 【[笔记](https://gitee.com/yuandj/siger/issues/I4SLV9#note_8643480_link)】

  > 初识健辉老师，缘于 欧拉。
  
    - ["上帝公式"(欧拉公式)真的神到](https://zhuanlan.zhihu.com/p/48392958) ... - 知乎专栏 「[笔记](https://gitee.com/yuandj/siger/issues/I4SL0T#note_8551073_link)」

      - [公众号：科研狗](https://images.gitee.com/uploads/images/2022/0203/104533_a62527d9_5631341.png) 

        - [我的博士毕业致辞](http://mp.weixin.qq.com/s?__biz=MzI5MzA4MjA1MA==&mid=2651484526&idx=1&sn=845619180d8ec84d1399a73c323b6536)
        - [《Lemon》这首歌到底在讲什么？](http://mp.weixin.qq.com/s?__biz=MzI5MzA4MjA1MA==&mid=2651483873&idx=1&sn=9402f12a7c6132c667d2ea446a181a11)
        - [五轴数控机床为什么是五轴联动而非六轴联动呢？](http://mp.weixin.qq.com/s?__biz=MzI5MzA4MjA1MA==&mid=2651483289&idx=1&sn=668716c655237e365fb7c3c613ba1170)
        - [硕博学位论文 LaTeX 模板及其使用教程](http://mp.weixin.qq.com/s?__biz=MzI5MzA4MjA1MA==&mid=2651484265&idx=1&sn=27907fa2675c36ef8f698d97d44a7b4c)
        - [什么是“奥利给不等式”？](http://mp.weixin.qq.com/s?__biz=MzI5MzA4MjA1MA==&mid=2651483427&idx=1&sn=2ef92fbe951d7d5d219dc931330d368c)
        - [嘿，文青！你真的“文艺”吗？](http://mp.weixin.qq.com/s?__biz=MzI5MzA4MjA1MA==&mid=2651483913&idx=1&sn=3a6d809efbf589d9156b8955f1594c22)

      - https://github.com/leekunhwee/

        - Jianhui Li （leekunhwee）
        - 【公众号】: 科研狗 【知乎ID】: 李狗嗨
        - 24 followers · 17 following
        - Xi'an Jiaotong University
        - Xi'an, Shaanxi, China 
        - leekunhwee@gmail.com 

      - http://www.lijianhui.net <img src="https://images.gitee.com/uploads/images/2022/0203/110255_181d181f_5631341.png" height="19px">

        Welcome! I’m Alex Lee. My Chinese name is Jianhui Li. Welcome to my blog!


- [如何给文科生解释傅里叶变换？](https://zhuanlan.zhihu.com/p/48305950) 【[笔记](https://gitee.com/yuandj/siger/issues/I4SLV9#note_8643616_link)】
  
  - 02、[锯齿波](https://gitee.com/yuandj/siger/issues/I4SLV9#note_8643627_link) 
  - 03、[三角波](https://gitee.com/yuandj/siger/issues/I4SLV9#note_8643639_link)

  <p><img title="骚年，我有故事，你有酒吗？" src="https://images.gitee.com/uploads/images/2022/0212/214349_a20ae99e_5631341.png" height="99px"> <img src="https://images.gitee.com/uploads/images/2022/0211/234746_e12d010c_5631341.png" height="99px" title="Pierre-Simon Laplace (1749–1827)图片来源：（Wikipedia）"></p>
    
  - [拉普拉斯变换中的 $S$ 是个什么鬼？](https://zhuanlan.zhihu.com/p/48314585) 【[笔记](https://gitee.com/yuandj/siger/issues/I4SLV9#note_8643688_link)】

    > A good way of thinking of where the Laplace transform comes from, and a way which dispels some of its mystery is by thinking of power series.(一个比较好的关于Laplace变换的解释方法是从幂级数(Power Series)入手。)  — —Arthur Mattuck (原MIT数学系主任)

  - [THEORIE DE LA CHALEUR FOURIER](https://gitee.com/yuandj/siger/issues/I4SLV9#note_8644019_link) 封面素材
    
    <p><img height="99px" src="https://images.gitee.com/uploads/images/2022/0211/231506_9560258a_5631341.jpeg"> <img height="99px" src="https://images.gitee.com/uploads/images/2022/0212/021254_f8750dfb_5631341.png"> <img height="99px" src="https://images.gitee.com/uploads/images/2022/0212/021330_fad30c0b_5631341.png"> <img height="99px" src="https://images.gitee.com/uploads/images/2022/0212/022041_b1d6310f_5631341.jpeg"></p>
    
  - [笔记 正文 （动图）](https://gitee.com/yuandj/siger/issues/I4SLV9#note_8655298_link)
  
    - 怎样让他们 直观の ？
    - [总结](https://gitee.com/yuandj/siger/issues/I4SLV9#note_8643674_link)
    - [傅里叶简介](https://gitee.com/yuandj/siger/issues/I4SLV9#note_8654058_link)
      
- [虚数 $i$ 真的很“虚”吗？](https://zhuanlan.zhihu.com/p/48392273) 【[笔记](https://gitee.com/yuandj/siger/issues/I4SLV9#note_8643755_link)】

  <p><img height="99px" src="https://images.gitee.com/uploads/images/2022/0212/225136_5e1e7101_5631341.png"></p>
  
- [自然底数 $e$ 怎么就“自然”了？](https://zhuanlan.zhihu.com/p/48391055) 【[笔记](https://gitee.com/yuandj/siger/issues/I4SLV9#note_8643790_link)】

  > 首先，我们需要知道 $e$ 这个表示自然底数的符号是由瑞士数学和物理学家 Leonhard Euler (莱昂纳德·欧拉)[ $^{[2]}$ ](https://zhuanlan.zhihu.com/p/48391055#ref_2)命名的，取的正是 Euler 的首字母“ $e$ ”。

  如果，我们进行一系列的迭代运算，我们将看到以下结果：其中， $n$ 指的是一年中结算利息的次数。只要在年利率保持 100% 不变的情况下，不断地提高利息的结算次数，余额就将会逼近 $e = 2.71828182845...$ 然后，终于可以祭出这个高等数学微积分里计算 $e$ 的一个重要极限了：$ r=a \cdot e ^{b \cdot \theta}$
  
  <p><img height="99px" src="https://images.gitee.com/uploads/images/2022/0212/001751_d3743ce2_5631341.png"> <img height="99px" src="https://images.gitee.com/uploads/images/2022/0212/001714_0bf0655a_5631341.png"> <img height="99px" src="https://images.gitee.com/uploads/images/2022/0212/002003_c97aa971_5631341.png"> <img height="99px" src="https://images.gitee.com/uploads/images/2022/0212/002034_4a58ff74_5631341.png"> <img height="99px" src="https://images.gitee.com/uploads/images/2022/0212/002043_8c76c55c_5631341.png"> <img height="99px" src="https://images.gitee.com/uploads/images/2022/0212/002049_6a776613_5631341.png"></p>

  $$e = \lim_{n \to \infty} \left( 1 + \frac{1}{n} \right)  ^ n $$
  
- [为什么 $e^x$ 的导数是还是其自身？](https://zhuanlan.zhihu.com/p/48678377) 【[笔记](https://gitee.com/yuandj/siger/issues/I4SLV9#note_8643812_link)】

  > 前年看了一部家喻户晓的国产科幻小说——《三体》，刷新了自己的世界观，我不仅对“黑暗森林”理论感到惊异，还对作者刘慈欣充满了敬佩。能写出这样的科幻作品势必需要丰厚的数学、物理、天文、地理等知识。书中留给我最深刻印象、让我觉得最不可思议的当属“歌者”使用的“二向箔”武器，其厉害之处在于可以将“三维”中的一个维度坍缩，最后变成“二维”，
  
  > 数学中也有一种东西可以看做是“降维操作”，那就是“求导”，而且数学中有一个特例， **能够在这种“降维操作”中，保持自身不变，那就是以 [公式] 为底的指数，或者说自然指数。** 
  
  相关文章有：

  - 《[自然底数 $e$ 怎么就“自然”了](https://zhuanlan.zhihu.com/p/48391055)？》
  - 《[0的零次方 $0^0$ 为何等于1](https://zhuanlan.zhihu.com/p/48319167)？》
  - 《[对数可以延长人类寿命](https://zhuanlan.zhihu.com/p/48393525)？》
  - 《[拉普拉斯变换中的S是个什么鬼](https://zhuanlan.zhihu.com/p/48314585)？》等。

  1、对数求导
  2、反函数的导数
  3、指数求导

  这就是为什么以自然底数 $e$ 为底的指数求导之后还是其本身。

  > 说好的降维打击呢？这分明就是升维打击。又开始推倒公式啦。:(
  
- [为什么说"对数"可以延长天文学家寿命？](https://zhuanlan.zhihu.com/p/48393525) 【[笔记](https://gitee.com/yuandj/siger/issues/I4SLV9#note_8643899_link)】 

  <p><img height="99px" src="https://images.gitee.com/uploads/images/2022/0212/005106_464ae721_5631341.png"></p>

  > "看起来在数学实践中，最麻烦的莫过于大数字的乘法、除法、开平方和开立方，计算起来特别费事又伤脑筋，于是我开始构思有什么巧妙好用的方法可以解决这些问题。"

    - 约翰·纳皮尔，《奇妙的对数定律说明书》[ $^{[5]}$ ](https://zhuanlan.zhihu.com/p/48393525#ref_5)

  作为数学家、物理学家兼天文学家，他在计算各种行星轨道数据时，也被浩瀚的计算量所折磨，因此很痛恨这些乏味的重复性工作。为了解决这一问题，他用了20年的时间，进行了数百万次的计算，发明了对数和对数表，听起来很矛盾，一个不想做重复工作的人结果做了20年重复性工作。但是，他的努力确实为后人减少了大量的重复性工作，大大减少了数学家、天文学家的计算量，由此可见，这在天文学界算得上是一项伟大的发明了，看看名人们对其的评价就能看出其重要性[6]。

  - “对数的发明、解析几何的创始和微积分的建立是17世纪数学的三大成就。” ——恩格斯
  - “对数的发现，因其节省劳力而延长了天文学家的寿命。” ——拉普拉斯
  - “给我空间、时间及对数，我就可以创造一个宇宙。” ——伽利略
  
- [一圈为何是360°？](https://zhuanlan.zhihu.com/p/48767522)  【[笔记](https://gitee.com/yuandj/siger/issues/I4SLV9#note_8643915_link)】

  因为 $360 ^ \circ$ 容易被整除， $360$ 的真因数除了 $1$ 和自身以外，一共有 $22$ 个（2、3、4、5、6、8、9、10、12、15、18、20、24、30、36、40、45、60、72、90、120、180），所以很多特殊角的角度都是整数[ $^{[1]}$ ](https://zhuanlan.zhihu.com/p/48767522#ref_1)。另外，这也更加符合以 $60$ 为进制的 “ **巴比伦数字** ” 系统[ $^{[2]}$ ](https://zhuanlan.zhihu.com/p/48767522#ref_2)。

  - 数学史 主题，占星，

    <p><img height="99px" src="https://images.gitee.com/uploads/images/2022/0212/005422_e13711bb_5631341.png"></p>
  
- [为什么(分享)？好奇心，求知欲。](https://gitee.com/yuandj/siger/issues/I4SLV9#note_8643928_link) 问题提纲
  
  - [《知识分享，终生相随》—— SIGer 是如何炼成的？](https://gitee.com/yuandj/siger/issues/I4SLV9#note_8653431_link) 【[正文](https://gitee.com/yuandj/siger/blob/master/%E4%B8%93%E8%AE%BF/%E3%80%90%E6%9D%8E%E5%81%A5%E8%BE%89%E3%80%91%E7%9F%A5%E8%AF%86%E5%88%86%E4%BA%AB%EF%BC%8C%E7%BB%88%E7%94%9F%E7%9B%B8%E9%9A%8F.md)】
  - [倪光南：希望大家群策群力，中国制造 2025](https://www.bilibili.com/video/BV1vi4y1R79t) 【[笔记](https://gitee.com/yuandj/siger/issues/I4SLV9#note_8643986_link)】
    
- [加拿大人喜欢佩戴的花？ ](https://mp.weixin.qq.com/s?__biz=MzI5MzA4MjA1MA==&mid=2651483124&idx=1&sn=74d966cab212b4406f1ad253589780fd) 【[笔记](https://gitee.com/yuandj/siger/issues/I4SLV9#note_8644024_link)】

  <p><img height="99px" src="https://images.gitee.com/uploads/images/2022/0212/022727_8f3e3220_5631341.png"></p>

  这个11.11 是一份纪念！纪念一战停战100周年纪念币 (图片来源: Canada Post)
  
- [如何成为公式编辑高手](https://mp.weixin.qq.com/s?__biz=MzI5MzA4MjA1MA==&mid=2651483080&idx=1&sn=f2dcbaf8b2b3525691e58744ff92e242) 【[笔记](https://gitee.com/yuandj/siger/issues/I4SLV9#note_8644027_link)】

  <p><img height="99px" src="https://images.gitee.com/uploads/images/2022/0212/022928_4c345705_5631341.png"></p>
