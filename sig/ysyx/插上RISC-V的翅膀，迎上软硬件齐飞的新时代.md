# 插上RISC-V的翅膀，迎上软硬件起飞新时代

 **果壳与香山** 都是基于 RISC-V 的开源高性能处理器，这里有一个新名词 “RISC-V”，RISC-V是一个基于精简指令集（RISC）原则的开源指令集架构（ISA），V 是第五代的意思。与大多数指令集相比，RISC-V 指令集可以自由地用于任何目的，允许任何人设计、制造和销售 RISC-V 芯片和软件。虽然这不是第一个开源指令集，但它具有重要意义，因为其设计使其适用于现代计算设备（如仓库规模云计算机、高端移动电话和微小嵌入式系统）。设计者考虑到了这些用途中的性能与功率效率。该指令集还具有众多支持的软件，这解决了新指令集通常的弱点。其有着完全开源、架构简单、易于移植、模块化设计、完整的工具链等特点。相对于 CISC 复杂指令集计算机而言，有着结构简单速度快、成本低可靠性高、支持高级语言等优点。我认为，RISC-V 必是未来的趋势，就在前不久 Intel 也投入了大量资金去研发 RISC-V 芯片，而中国对于 RISC-V 芯片的研发明显早于 Intel，这或许是中国硬件发展的一个转折点。

 **硬件与软件是相辅相成的** 。有着良好的硬件，才能通过软件实现更多功能；有着良好的软件，硬件才能发挥它的作用。在以前硬件不发达的时代，所有的程序和软件都必须考虑优化这一问题，如果自己的程序或软件过于繁琐，那么它就不能在硬件上发挥作用；而现在硬件飞速发展，许多软件和程序都没有做太好的优化处理，那么这种问题如何得到解决呢？利用 RISC-V 的指令系统：把主要精力放在那些经常使用的指令上，尽量使它们具有简单高效的特色。对不常用的功能，常通过组合指令来完成。相当于把优化嵌入到芯片之中，这样不论对开发者还是使用者都有巨大的好处。

 **关于硬件的发展** ，或许大家都听到过 Intel 和 AMD 的处理器，却从来没有听说过一款国产的处理器，主要因为我国在 CISC 处理器上发展的较晚，但对于果壳与香山这样的 RISC-V 处理器，大家都从 0 开始研究，而我国的发展又相对较早，在目前这个软硬件都飞速发展的时代，我相信我们很快就能看到一款知名的国产 RISC-V 处理器。