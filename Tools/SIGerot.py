# 效仿 SCrot 这一最经典应用的名字，SIGerot 是为 SIGer 期刊提供打印排版服务的，通过控制浏览器生成可以用于打印输出的分页版式，并沿用 Gitee 的UI风格。
# 本需求源自 第2期 拥抱开源，改变世界 的打印需求 :pray: 而且该期推荐的两期期刊中 出了《宇宙》，《中文化 PYTHON》中的网页爬虫能力，也是 SIGerot 的核心技能。

# 经过民族棋中国史 LuckyLudii 项目的验证，SIGerot01~15 顺利完成了下载数据库的任务，之后又使用 soup 完成了几期的内容的转载
# 本次上传的 SIGerot22.1.py 和 SIGerot22.2.py 分别完成网页文本截取，图片下载的任务，同时上传了 例子 in/out (html/md) 同名文件
# SIGerot 将以 PY 例子的方式，为编辑们示范如何辅助自己的转载工作，经过相对多的实践后，再提取需求，形成通用的辅助工具。

# - 提取全文文本 SIGerot22.1.py ： https://new.qq.com/rain/a/20220402A04KU400 
# - 下载全部图片 SIGerot22.2.py ： http://faxing.jkb.com.cn/home/index/lists.html?goods=5&y=2022&name=dzjk