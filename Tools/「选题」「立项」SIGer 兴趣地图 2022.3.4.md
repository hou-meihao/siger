### [SIGer 兴趣地图 2022.3.4](https://gitee.com/flame-ai/siger/issues/I4ZSOV#note_9532884_link)

| # | 2022.1 | 2022.2 | 2022.3 | 2022.3.4 | # |
|---|---|---|---|---|---|
| 1 | [琴瑟之美，<br>三月不识肉味](https://gitee.com/ggrrkkjjddzz/siger/issues/I4RJ8U) | [民服志愿者讲解员培训](https://gitee.com/flame-ai/caihongqiao/issues/I4UEVP) | [Her Future, Her Way 最好的科技女性主题就有啦！](https://gitee.com/RV4Kids/RVWeekly/issues/I4WXOS#note_9094745_link) | [清华附中机器博弈社团](https://gitee.com/yuandj/siger/issues/I4UFQY#note_9467348_link)/[泓燚软件](http://thcgc.gitee.io/hongyisoft/) 招新帖 | 1 |
| 2 | [有趣的数独](https://gitee.com/zhao-yuxiang-1/siger/issues/I4QL6P#note_8273224_link) | [民服老照片](https://gitee.com/flame-ai/caihongqiao/issues/I4J4EH) | [Ada Lovelace 是谁？](https://gitee.com/RV4Kids/RVWeekly/issues/I4WXOS#note_9094744_link) | [吕朝说公益 - 火种计划](https://gitee.com/yuandj/siger/issues/I4ZSP9) | 2 |
| 3 | [谈芯—什么是<br>FPGA工程师<br>核心竞争力](https://gitee.com/shiliupi/easy_raspi_python_one/issues/I4ZREG) | [鄂温克族时装画惊艳亮相！](https://gitee.com/flame-ai/caihongqiao/issues/I4LJUN) | [Your Pi Day Impact](https://gitee.com/blesschess/luckystar/issues/I4XT54) | [健康听力技术论坛](https://gitee.com/yuandj/siger/issues/I4ZSXG) | 3 |
| 4 | [侠之江湖 Metaverse's<br>Door - MUD](https://gitee.com/yuandj/siger/issues/I4M57V) | [水族新年 - 民族服饰](https://gitee.com/flame-ai/caihongqiao/issues/I4UEVC) | [拥抱网络，拥抱科技，改善学习 ——不让任何人掉队的SIGer兴趣小组](https://gitee.com/huang-zixin001/siger/issues/I4YFWY) | [向未来的管理者推荐DA课程](https://gitee.com/yuandj/siger/issues/I4ZVHN) | 4 |
| 5 | [它星云<br>（宠物篇）](https://gitee.com/fan-jingyi/siger/issues/I4ROK5) | [三月桃花水](https://gitee.com/yuandj/siger/issues/I4VF3J) | [David Parlett https://parlettgames.uk/](https://gitee.com/blesschess/LuckyLudii/issues/I4XTW8#note_9271988_link) | [北京顺义 环境检测 大气颗粒物检测 激光散射 & 花粉浓度，花粉颗粒物](https://gitee.com/yuandj/siger/issues/I4ZYF1) | 5 |
| 6 | [《意林》](https://gitee.com/xiaoxu-tongxue/siger/issues/I4R6ZI) | [钱学森](https://gitee.com/yuandj/siger/issues/I4VF35) | [中国剩余定理](https://gitee.com/cchen55/siger/issues/I4X88K) | [无源外骨骼——新时代的纯机械之美](https://gitee.com/yuandj/siger/issues/I4ZZ3B#note_9486992_link) | 6 |
| 7 | [《棋魂》](https://gitee.com/huang-shutz/siger/issues/I4QYV4) | [折纸](https://gitee.com/yuandj/siger/issues/I4UTYE) | [分形几何分镜拼接 ★](https://gitee.com/cchen55/siger/issues/I4XBIK) | [iBOT轮椅发明者 “发明怪杰” 迪安·卡门](https://gitee.com/yuandj/siger/issues/I4ZZ3B) | 7 |
| 8 | [分形几何2](https://gitee.com/cchen55/siger/issues/I4SLNS) | [ROS allin (无人车,dual-ur...)](https://gitee.com/yuandj/siger/issues/I4UFAJ) | [植下一棵树，收获万点绿，3.12 植树节](https://gitee.com/g5pik/siger/issues/I4X1FD#note_9181156_link) | [CCTV-9 生活的减法 (断舍离)](https://gitee.com/yuandj/siger/issues/I500FW#note_9493630_link) | 8 |
| 9 | [基于OpenGL<br>三维动画编程<br>—太阳系行星<br>转动交互设计](https://gitee.com/cchen55/siger/issues/I4QUJQ) | [生活里的科学，央视视频](https://gitee.com/yuandj/siger/issues/I4UFAC) | [Git 抱抱](https://gitee.com/g5pik/siger/issues/I4X1FD#note_9181148_link) | [毛绒玩具修复师 - 玩能修爷爷](https://gitee.com/yuandj/siger/issues/I500FW) | 9 |
| 10 |  | [数学III](https://gitee.com/yuandj/siger/issues/I4UF4O) | [“一生一芯” SIGer 科普小组 ★](https://gitee.com/flame-ai/siger/tree/master/sig/ysyx) | [贝尔灯塔，人类工业文明进程的标志](https://gitee.com/yuandj/siger/issues/I508GE) | 10 |
| 11 |  | [EPICGAMES - 元宇宙史诗 SIGGRAPH](https://gitee.com/yuandj/siger/issues/I4UEVV) | [FLAME? to FlameOI](https://gitee.com/yuandj/siger/issues/I4UFQY) | [知心姐姐：爱好和学习会冲突吗？我该怎么选择？](https://gitee.com/yuandj/siger/issues/I50D2T) | 11 |
| 12 |  | [冬奥顶流“谷爱凌” —— SIGER眼中的爱凌](https://gitee.com/yuandj/siger/issues/I4UEPF) | [SIGer.gitee.io ★ 站长](https://gitee.com/siger99/siger99/issues/I4YMG9) | [丝绸之路，千寻读书](https://gitee.com/yuandj/siger/issues/I50GAG) | 12 |
| 13 |  | [手抄报](https://gitee.com/yuandj/siger/issues/I4SL9Y) | [莫把丹青等闲看，无声诗里颂千秋](https://gitee.com/lixue060304/siger/issues/I4XHT0) | [Ele实验室 UP主的自我修养](https://gitee.com/g5pik/siger/issues/I4ZZOG#note_9506155_link) | 13 |
| 14 |  | [103机](https://gitee.com/yuandj/siger/issues/I4UC12) | [Walter Moers 1999年出版长篇小说《蓝熊船长的奇幻大冒险》](https://gitee.com/wei-tong11/siger/issues/I4XKQC#note_9187506_link) | [恩尼格码密码机如何工作？](https://gitee.com/g5pik/siger/issues/I4ZZOG#note_9498236_link) | 14 |
| 15 |  | [机械](https://gitee.com/rain-swz/siger/issues/I4SWC8) | [《梦书之城》THE CITY OF DREAMING BOOKS](https://gitee.com/wei-tong11/siger/issues/I4XKQC#note_9187504) | [极客工作流：向专业人士一样学习与工作](https://gitee.com/g5pik/siger/issues/I503C1) | 15 |
| 16 |  | [sig-三江源](https://gitee.com/hexi2007/siger/issues/I4UEK9) | [DUMMY 青春 + ElectronBot 电脑 等你复刻](https://gitee.com/duan-haotian/siger/issues/I4XM18) @[稚辉君](https://gitee.com/peng_zhihui) 4th | [怎么就走进了星辰大海](https://gitee.com/huang-zixin001/siger/issues/I4ZZHM) | 16 |
| 17 |  | [藏棋非遗传人](https://gitee.com/hexi2007/siger/issues/I4UEK7) | [小昕大瑞的奇幻之旅](https://gitee.com/huang-zixin001/siger/issues/I4Y343) | [开往春天的列车](https://gitee.com/huang-zixin001/siger/issues/I504ID) | 17 |
| 18 |  | [明清散文](https://gitee.com/zhong-tongyu/siger/issues/I4SLVO) | [我的兴趣爱好：科学幻想](https://gitee.com/huang-zixin001/siger/issues/I4XL1O) | 问天：[“天眼”是用来干什的？中国的天眼先进吗？](https://gitee.com/huang-zixin001/siger/issues/I505G4) [LAMOST](https://gitee.com/huang-zixin001/siger/issues/I50FGK) | 18 |
| 19 |  | [击剑](https://gitee.com/song-yi123ai/siger/issues/I4S95L) | [书展、诗词大会、书法展](https://gitee.com/huang-zixin001/siger/issues/I4Y03T#note_9238190_link) | [斑衣蜡蝉，“花大姐”](https://gitee.com/huang-zixin001/siger/issues/I504TD) | 19 |
| 20 |  | [神经网络与深度学习](https://gitee.com/duan-haotian/siger/issues/I4TFZD) | [《定风波.莫听穿林打叶声》读后感](https://gitee.com/huang-zixin001/siger/issues/I4Y03T) | [波谱艺术家，ANDYWARHOL 算法和装置](https://gitee.com/LHZ_CUC/siger/issues/I4NY6D) | 20 |
| 21 |  | [魔方小志](https://gitee.com/duan-haotian/siger/issues/I4RVAI) | [热血男儿辛弃疾](https://gitee.com/huang-zixin001/siger/issues/I4Y5FI) | [AI 图像智能修复 为 红色经典 上色开4K高清](https://gitee.com/LHZ_CUC/siger/issues/I4ONLZ) | 21 |
| 22 |  | [Singularity·K·Chen 《回归学习》](https://gitee.com/RV4Kids/ysyxsoc/issues/I4VJ0X) | [中国储能行业进入快车道](https://gitee.com/yuandj/siger/issues/I4YMFE) | [SIGer少年II, 把很多小照片拼成一张大照片 ★](https://gitee.com/LHZ_CUC/siger/issues/I4XBII) | 22 |
| 23 |  | [用钢丝做一台RISCV架构的计算机并跑个linux](https://gitee.com/sumsigma/siger/issues/I4UFU3) | [每一个喜欢火车的孩子，都很酷](https://gitee.com/huang-zixin001/siger/issues/I4YTWA#note_9350800_link) | [麒麟9000——制裁之下的华为海思绝唱](https://gitee.com/LHZ_CUC/siger/issues/I5074N) | 23 |
| 24 |  | [BACK to YSYX3rd](https://gitee.com/RV4Kids/ysyx3rd/issues/I4UTYI) | [时代顶流：飞天不是梦 :pray:](https://gitee.com/huang-zixin001/siger/issues/I4ZF86) | [大李淘手机: 索尼α7MarkⅣ——疫情之下的“缺货神机”](https://gitee.com/LHZ_CUC/siger/issues/I5074T) | 24 |
| 25 |  | [量子纠错，数独，欧拉！](https://gitee.com/yuandj/siger/issues/I4SL0N) | [我的兴趣爱好：篮球](https://gitee.com/zyz666hhh/siger/issues/I4XL3U) | [大李淘相机: 如何选择您的第一台数码相机？](https://gitee.com/LHZ_CUC/siger/issues/I50754) | 25 |
| 26 |  |  |  | [大创项目好题材 ★](https://gitee.com/huang-shutz/siger/issues/I507H5) | 26 |
| 27 |  |  |  | [“东数西算”工程 & 绿电](https://gitee.com/huang-shutz/siger/issues/I507H5#note_9513256_link) | 27 |
| 28 |  |  |  | [稚晖君的“电子”小机器人已上传至DigiPCBA](https://gitee.com/duan-haotian/siger/issues/I4XM18#note_9262636_link) | 28 |
| 29 |  |  |  | [爱体育爱生活](https://gitee.com/rone220315/siger/issues/I4Y0E6) | 29 |
| 30 |  |  |  | [我的偶像是 约翰卡马克](https://gitee.com/du_hengyi_admin/siger/issues/I507KC) | 30 |
| 31 |  |  |  | [《对话》打造科技创新驱动力](https://gitee.com/chen-houzhi/siger/issues/I4ZSH3) | 31 |
| 32 |  |  |  | [《个人信息保护法时代的隐私保卫战》系列报道·澎湃新闻·澎湃号·媒体](https://gitee.com/g5pik/siger/issues/I507W6) | 32 |
| 33 |  |  |  | [春日的暖阳中，感受诗词的无限魅力](https://gitee.com/huang-zixin001/siger/issues/I500UF) | 33 |

### [按照频道分类](https://gitee.com/flame-ai/siger/issues/I4ZSOV#note_9548870_link)

|  | 生活 | 少年 | 专访 | chess | sudoku | ysyx | STEM | RISC-V | MARs |
|---|---|---|---|---|---|---|---|---|---|
| 立项数 | 39 | 2 | 0 | 3 | 1 | 5 | 29 | 2 | 5 |

- 按照现有频道初步花粉，专访栏目暂时空置！还有部分内容属于 Tools 未加入！

### [SIGer 频道](https://gitee.com/flame-ai/siger/issues/I4ZSOV#note_9579761_link) [详解](https://gitee.com/flame-ai/siger/issues/I4ZSOV#note_9580082_link)

|  | 生活 | 少年 | Tools | chess | sudoku | ysyx | STEM | RISC-V | MARs |
|---|---|---|---|---|---|---|---|---|---|
| 立项数 | 40 | 2 | 5 | 3 | 1 | 5 | 29 | 2 | 5 |

- 专栏 改成 Tools
- 收录了 建站还有PY工具类
- 漏了一个《手抄报》（学习类，划分到生活）
- 生活是个大频道 40 个项目，包含：读书，体育，环保，民族服饰等专栏，这是一个北向频道，开源社区不能没有方向
- chess, sudoku 是体育类的细分，智力运动，是火焰棋实验室的缘起，SIGER 提升了 火焰棋实验室 从 AI 到 OI 开放智能的意思。站名解析中已经体现。民族棋中国史相关，被收录到 chess 中，尽管 board game 桌游才是正确的解释，但这是传统。
- 少年是专访的细分，只收录 SIGER 榜样和 SIGER 优秀编委
- 其他 都是 STEM 的细分，
- STEM 是 科普的重要内容，是第2大类。

### [STEM MAP](https://gitee.com/flame-ai/siger/issues/I4ZSOV#note_9582083_link):

1. [数学III](https://gitee.com/yuandj/siger/issues/I4UF4O)
   - [分形几何2](https://gitee.com/cchen55/siger/issues/I4SLNS)
   - [基于 OpenGL 三维动画编程 —— 太阳系行星转动交互设计](https://gitee.com/cchen55/siger/issues/I4QUJQ)
   - [中国剩余定理](https://gitee.com/cchen55/siger/issues/I4X88K)

1. 元宇宙：
   - [侠之江湖 Metaverse's Door - MUD](https://gitee.com/yuandj/siger/issues/I4M57V)
   - [EPICGAMES - 元宇宙史诗 SIGGRAPH](https://gitee.com/yuandj/siger/issues/I4UEVV)
   - [我的偶像是 约翰卡马克](https://gitee.com/du_hengyi_admin/siger/issues/I507KC)

1. [钱学森](https://gitee.com/yuandj/siger/issues/I4VF35)
1. [103机](https://gitee.com/yuandj/siger/issues/I4UC12)
1. [机械](https://gitee.com/rain-swz/siger/issues/I4SWC8)
1. [ROS allin (无人车,dual-ur...)](https://gitee.com/yuandj/siger/issues/I4UFAJ)
1. [神经网络与深度学习](https://gitee.com/duan-haotian/siger/issues/I4TFZD)

1. [Her Future, Her Way 最好的科技女性主题就有啦！](https://gitee.com/RV4Kids/RVWeekly/issues/I4WXOS#note_9094745_link)
   - [Ada Lovelace 是谁？](https://gitee.com/RV4Kids/RVWeekly/issues/I4WXOS#note_9094744_link)

1. [Your Pi Day Impact](https://gitee.com/blesschess/luckystar/issues/I4XT54)
1. [中国储能行业进入快车道](https://gitee.com/yuandj/siger/issues/I4YMFE)

1. [iBOT轮椅发明者 “发明怪杰” 迪安·卡门](https://gitee.com/yuandj/siger/issues/I4ZZ3B)
   - [无源外骨骼——新时代的纯机械之美](https://gitee.com/yuandj/siger/issues/I4ZZ3B#note_9486992_link)

1. [贝尔灯塔，人类工业文明进程的标志](https://gitee.com/yuandj/siger/issues/I508GE)
1. [Ele实验室 UP主的自我修养](https://gitee.com/g5pik/siger/issues/I4ZZOG#note_9506155_link)
1. [恩尼格码密码机如何工作？](https://gitee.com/g5pik/siger/issues/I4ZZOG#note_9498236_link)
1. [极客工作流：向专业人士一样学习与工作](https://gitee.com/g5pik/siger/issues/I503C1)
1. [波谱艺术家，ANDYWARHOL 算法和装置](https://gitee.com/LHZ_CUC/siger/issues/I4NY6D)
1. [AI 图像智能修复 为 红色经典 上色开4K高清](https://gitee.com/LHZ_CUC/siger/issues/I4ONLZ)
1. [SIGer少年II, 把很多小照片拼成一张大照片 ★](https://gitee.com/LHZ_CUC/siger/issues/I4XBII)
1. [大创项目好题材 ★](https://gitee.com/huang-shutz/siger/issues/I507H5)
1. [“东数西算”工程 & 绿电](https://gitee.com/huang-shutz/siger/issues/I507H5#note_9513256_link)
1. [稚晖君的“电子”小机器人已上传至DigiPCBA](https://gitee.com/duan-haotian/siger/issues/I4XM18#note_9262636_link)
1. [《对话》打造科技创新驱动力](https://gitee.com/chen-houzhi/siger/issues/I4ZSH3) 

### [生活 频道 MAP](https://gitee.com/flame-ai/siger/issues/I4ZSOV#note_9582218_link)：

1. [琴瑟之美，三月不识肉味](https://gitee.com/ggrrkkjjddzz/siger/issues/I4RJ8U)

1. [民服志愿者讲解员培训](https://gitee.com/flame-ai/caihongqiao/issues/I4UEVP)
   - [民服老照片](https://gitee.com/flame-ai/caihongqiao/issues/I4J4EH)
   - [鄂温克族时装画惊艳亮相！](https://gitee.com/flame-ai/caihongqiao/issues/I4LJUN)
   - [水族新年 - 民族服饰](https://gitee.com/flame-ai/caihongqiao/issues/I4UEVC)
1. [生活里的科学，央视视频](https://gitee.com/yuandj/siger/issues/I4UFAC)
   - [折纸](https://gitee.com/yuandj/siger/issues/I4UTYE)
1. [爱体育爱生活](https://gitee.com/rone220315/siger/issues/I4Y0E6)
   - [冬奥顶流“谷爱凌” —— SIGER眼中的爱凌](https://gitee.com/yuandj/siger/issues/I4UEPF)
   - [击剑](https://gitee.com/song-yi123ai/siger/issues/I4S95L)
   - [我的兴趣爱好：篮球](https://gitee.com/zyz666hhh/siger/issues/I4XL3U)
   - [魔方小志](https://gitee.com/duan-haotian/siger/issues/I4RVAI)
1. [书展、诗词大会、书法展](https://gitee.com/huang-zixin001/siger/issues/I4Y03T#note_9238190_link)
   - [明清散文](https://gitee.com/zhong-tongyu/siger/issues/I4SLVO)
   - [莫把丹青等闲看，无声诗里颂千秋](https://gitee.com/lixue060304/siger/issues/I4XHT0)
   - [《意林》](https://gitee.com/xiaoxu-tongxue/siger/issues/I4R6ZI)
   - [三月桃花水](https://gitee.com/yuandj/siger/issues/I4VF3J)
   - [Walter Moers 1999年出版长篇小说《蓝熊船长的奇幻大冒险》](https://gitee.com/wei-tong11/siger/issues/I4XKQC#note_9187506_link)
   - [《梦书之城》THE CITY OF DREAMING BOOKS](https://gitee.com/wei-tong11/siger/issues/I4XKQC#note_9187504)
   - [《定风波.莫听穿林打叶声》读后感](https://gitee.com/huang-zixin001/siger/issues/I4Y03T)
   - [热血男儿辛弃疾](https://gitee.com/huang-zixin001/siger/issues/I4Y5FI)
   - [向未来的管理者推荐DA课程](https://gitee.com/yuandj/siger/issues/I4ZVHN)
   - [丝绸之路，千寻读书](https://gitee.com/yuandj/siger/issues/I50GAG)
1. [每一个喜欢火车的孩子，都很酷](https://gitee.com/huang-zixin001/siger/issues/I4YTWA#note_9350800_link)
1. [吕朝说公益 - 火种计划](https://gitee.com/yuandj/siger/issues/I4ZSP9)
   - [它星云（宠物篇）](https://gitee.com/fan-jingyi/siger/issues/I4ROK5)
   - [sig-三江源](https://gitee.com/hexi2007/siger/issues/I4UEK9)
   - [植下一棵树，收获万点绿，3.12 植树节](https://gitee.com/g5pik/siger/issues/I4X1FD#note_9181156_link)
1. [CCTV-9 生活的减法 (断舍离)](https://gitee.com/yuandj/siger/issues/I500FW#note_9493630_link)
   - [毛绒玩具修复师 - 玩能修爷爷](https://gitee.com/yuandj/siger/issues/I500FW)
1. [知心姐姐：爱好和学习会冲突吗？我该怎么选择？](https://gitee.com/yuandj/siger/issues/I50D2T)
1. 迎春
   - [北京顺义 环境检测 大气颗粒物检测 激光散射 & 花粉浓度，花粉颗粒物](https://gitee.com/yuandj/siger/issues/I4ZYF1)
   - [开往春天的列车](https://gitee.com/huang-zixin001/siger/issues/I504ID)
   - [斑衣蜡蝉，“花大姐”](https://gitee.com/huang-zixin001/siger/issues/I504TD)
   - [春日的暖阳中，感受诗词的无限魅力](https://gitee.com/huang-zixin001/siger/issues/I500UF)
1. 大李淘机 | 摄影 
   - [麒麟9000——制裁之下的华为海思绝唱](https://gitee.com/LHZ_CUC/siger/issues/I5074N)
   - [大李淘机: 索尼α7MarkⅣ——疫情之下的“缺货神机”](https://gitee.com/LHZ_CUC/siger/issues/I5074T)
   - [大李淘机: 如何选择您的第一台数码相机？](https://gitee.com/LHZ_CUC/siger/issues/I50754)
1. [《个人信息保护法时代的隐私保卫战》系列报道·澎湃新闻·澎湃号·媒体](https://gitee.com/g5pik/siger/issues/I507W6)
1. [手抄报](https://gitee.com/yuandj/siger/issues/I4SL9Y)

### [他山之石 Reference](https://gitee.com/flame-ai/siger/issues/I4ZSOV#note_9582699_link)

1. [Ele实验室 UP主的自我修养](https://gitee.com/g5pik/siger/issues/I4ZZOG#note_9506155_link)
1. [知心姐姐：爱好和学习会冲突吗？我该怎么选择？](https://gitee.com/yuandj/siger/issues/I50D2T)
1. [吕朝说公益 - 火种计划](https://gitee.com/yuandj/siger/issues/I4ZSP9)

1. CCTV
   - [CCTV-9 生活的减法 (断舍离)](https://gitee.com/yuandj/siger/issues/I500FW#note_9493630_link)
   - [生活里的科学，央视视频](https://gitee.com/yuandj/siger/issues/I4UFAC) CCTV-10
   - [《对话》打造科技创新驱动力](https://gitee.com/chen-houzhi/siger/issues/I4ZSH3) CCTV-2

1. 书单：
   - [丝绸之路，千寻读书](https://gitee.com/yuandj/siger/issues/I50GAG)
   - [向未来的管理者推荐DA课程](https://gitee.com/yuandj/siger/issues/I4ZVHN)

![输入图片说明](https://images.gitee.com/uploads/images/2022/0402/093157_1ce35f78_5631341.png "在这里输入图片标题")

[Ele实验室](https://space.bilibili.com/481434238) 是 @g5pik 推荐的《计算机博物志》的作者，UP主团队，在这个时点阅读《[是怎么做视频的](https://www.bilibili.com/video/BV1AF411i79H)？》格外有意义，也是作为他山之石第一个推荐的内容的原有，看这张 思维导图 一目了然地展示了 一个科普自媒体的工作流程，咱们 SIGer 编委完全可以类比学习。SIGer 是平面媒体，所以文案后就可以直接发布了，这是 Gitee 开源平台的益处，也是博客时代的历史遗存，社区自媒体的特性。

> 因为我总觉得即使是早已知道的东西，也有可能一直带有过程误解，所以仍然需要进一步的确认，甚至重新学习这些。

相对自媒体的传播属性，SIGer 更加轻装上阵，我们的目标直接就是 编委自身的学习，就好想老师们给同学们的 “手抄报” 作业一样。SIGer 2021 是 @yuandj 老师自己的示范，以 石榴派 作为最大收获，再就是 SIGer 本身。2022 前三个月 提前完成 99 个选题的任务，可以为全年小目标提供足够的内容保证，这都是在社区中学习，向社区学习的直接表现。他山之石，就是我们的镜子，学习的榜样，我相信每个同学，包括 SIGer 都会在未来的 某一天 从镜子中看到的是真正的自己，大家未来的样子的。:pray:

《知心姐姐》可以说是 成就 @yuandj 老师火焰棋社区传播的媒体推手，官媒，没有火焰棋就不会有火焰棋实验室以及现在的开源实践，也就遇见不了同学们。《吕朝》老师亲自说公益，是一位老兵奉献给新时代的优质媒体平台，是为了我的发愿：石榴派 - 公益芯 请求的见证。开源社区，就是公益社区，它不是工具，而是文化，是生活的一部分。

CCTV-9 纪录频道，CCTV-10 科教频道 是我的最爱，《宇宙》为2022开篇，就是源自 9 纪录。大量选题都来自这里，央视是一个敬意的称呼，CCTV-2 的选择是一个偶然，只是他的一个专题和上周的 纳米科技 专题有交集而已。

而书单则是一个同类书籍的概览，我认为 SIGer 只有亲自读过的分享，才是 SIGer 应该秉承的学习方法，转载只能说明一种向往，而不实践，不亲自阅读，不是学习。仅仅是做起封面来好看而已。学习的过程都在 ISSUES 的跟贴中，这也是每篇 成刊的 .md 都有一个【笔记】栏目的原因。

### [我们的兴趣爱好？](https://gitee.com/flame-ai/siger/issues/I4ZSOV#note_9592487_link)

- 我的兴趣爱好？
- 我的偶像？
- 我的母校？

从分享开始学习，是所有 SIGer 编委的起点，我们分享过去，分享我们的当下，分享我们的向往的学习生活，都是 SIGer 选题的范畴，当 聚合 更多的分享，一张清晰的 SIGer 兴趣地图就能展现出来，我相信，那一定是一张正能量满满，朝气蓬勃的兴趣地图，能展现出我们 SIGer 的精神风貌来。

（本节是对 [新人三步曲：「上岗」「选题」「立项」](https://gitee.com/flame-ai/siger/issues/I4RG2K#note_8388077_link) 的补充说明！）

### 我的项目？

SIGer 不会只动嘴皮子和笔杆子，同样也是行动派，公益芯石榴派，民族棋中国史就是范例，我们叫项目。也将会是最真实的分享，其过程，就会是很好的学习资料，必须分享。

### 我们的学习环境？

极客工作流：向专业人士一样学习与工作，同学们已经身在其中啦。本片文章无处不在，展示着 SIGer 编委的工作流程，开源就是源自 极客 的社区，发展至今已经成为了一种文化。让同学们感同身受这样的先进文化，就是 SIGer 的使命，不是说教，而是实践，变学习，变分享，共同实践的大家庭。

### 我们的同学？

既然是开源社区，就不会像教室里一样，同一个时空随时交流，ISSUES 是我们的交流空间，对仓的 PR 是我们学习成果交换的成果，尽管平时有群，但习惯自我探索学习是 最重要的。这并不意味 你是孤独的，看这些 立项创意，还有过往的成品和海报，都是同学们的思想轨迹，一定可以给你灵感和启发。你的分享也同样会是给他们启发。

除了启发你进一步探索丰富内容外，直接引用，组合成新的期刊，也是一种学习成果的表现，我们可以换不同的角度去重新审视既有的知识，重新发现他们的价值。

SIGer 的每一个同学都是平等的，包括老师（SIGer 大使），也只是一个角色，频道是一个个项目用以组织内容，主编是对该频道和内容更熟悉的同学，能够对其进行再组织，再编辑，以更好地呈现知识。站长，是 SIGer 的另一个表现形式 pages 的志愿者，对 HTML 更熟悉，https://siger99.gitee.io/ 就是现在的呈现，会逐步完善。

SIGer 和其他开源项目一样，是一个会聚分享的地方，唯一的区别就是它是以内容为主导的，区别于代码仓，并不需要你掌握计算机语言和编程。它可以更好地让你聚焦 开源社区 的组织形式，交流方式，这就是它的起点了，然后通过大家的努力，形成一本兴趣宝殿，SIGER 兴趣地图，作为更多青少年同学的指南，就是我们的使命啦。

