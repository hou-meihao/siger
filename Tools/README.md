# SIGer

#### 介绍
为GITEE少年版开辟阵地

这个仓会汇集各种兴趣小组的文件，想法。采用SIG管理架构，通过PR分享各自社团的学习成果。寄希望成为一个学生社团管理的宝典和百科。

同时，所有的SIGer兴趣小组，都应该具备数字化工具，专属工具应该被推荐，必定每个小组有自己的特点。通用工具GITEE少年版，肯定是必备的。协同创作本身，就符合开源社区的标配。而SIGer使用的起点工具GITEE，只是一个脚手架的作用，未来一定会被全新的工具所替代。就如LINUX之于minux.


> The name "git" was given by Linus Torvalds when he wrote the veryfirst version. He described the tool as "the stupid content tracker"and the name as (depending on your way):
>
> - random three-letter combination that is pronounceable, and not   actually used by any common UNIX command.  The fact that it is a   mispronunciation of "get" may or may not be relevant. 
> - stupid. contemptible and despicable. simple. Take your pick from the   dictionary of slang. 
> - "global information tracker": you're in a good mood, and it actually   works for you. Angels sing, and a light suddenly fills the room. 
> - "g*dd*mn idiotic truckload of sh*t": when it breaks

GitSTAR 恒星 这就是 SIGer 的工具名啦，STAR 恒星 是宇宙的核心，宇宙黑暗背景中的星光都源自恒星，象征着未来之星，符合最初 Gitee 未来版 的设定。未来之星的协作空间，诞生未来之星的地方。第一个工具 [SIGerot.py](SIGerot.py) 是 SIGer 打印输出工具，SIGer 分享的新渠道，有关 GitSTAR 的设想请移步 [GitSTAR.md](GitSTAR.md) 

#### 软件架构
B/S网站，使用JS前端技术，后端PHP MYSQL。


#### 安装教程

1.  选择任何一个“石榴派”的镜像服务器，或者在自己的树莓派安装“石榴派”。
2.  GMing 整个目录 upload 到石榴派的服务器上.
3.  配置号WEB服务器的启动目录，就可以使用了。

#### 使用说明

1.  首先，这是一个B/S服务，在个人终端上输入配置好的服务器地址，即可正常访问。
2.  通用的协作功能，不需要额述。
3.  选择自己的SIGer专用的数字化工具，参考其说明即可。 

#### 参与贡献

1.  Fork 本仓库
2.  新建 siger xxx 分支 （xxx为您选择的兴趣小组的名字sig，任何分支，请务必指修改一个SIG目录）
3.  提交代码 （这里是您贡献的新资料，或者代码。）
4.  新建 Pull Request 
   （请在PR前，一定要保存好自己的本地更新，并在此之前，对master的版本变更，做到心中有数）


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
