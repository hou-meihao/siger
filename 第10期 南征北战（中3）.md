《南征北战》[目录](https://gitee.com/yuandj/siger/blob/master/%E7%AC%AC10%E6%9C%9F%20%E5%8D%97%E5%BE%81%E5%8C%97%E6%88%98%EF%BC%88%E4%B8%8A12%EF%BC%89.md#%E5%8D%97%E5%BE%81%E5%8C%97%E6%88%98)：
- 一、封面故事：[阳光海涛](https://gitee.com/yuandj/siger/blob/master/%E7%AC%AC10%E6%9C%9F%20%E5%8D%97%E5%BE%81%E5%8C%97%E6%88%98%EF%BC%88%E4%B8%8A12%EF%BC%89.md#%E4%B8%80%E5%B0%81%E9%9D%A2%E6%95%85%E4%BA%8B%E9%98%B3%E5%85%89%E6%B5%B7%E6%B6%9B)
  - [能在社区传播的路上有个我](https://gitee.com/yuandj/siger/blob/master/%E7%AC%AC10%E6%9C%9F%20%E5%8D%97%E5%BE%81%E5%8C%97%E6%88%98%EF%BC%88%E4%B8%8A12%EF%BC%89.md#-%E8%83%BD%E5%9C%A8%E7%A4%BE%E5%8C%BA%E4%BC%A0%E6%92%AD%E7%9A%84%E8%B7%AF%E4%B8%8A%E6%9C%89%E4%B8%AA%E6%88%91)
  - [从零开始在Windows下启动openEuler（WSL）](https://gitee.com/yuandj/siger/blob/master/%E7%AC%AC10%E6%9C%9F%20%E5%8D%97%E5%BE%81%E5%8C%97%E6%88%98%EF%BC%88%E4%B8%8A12%EF%BC%89.md#-%E4%BB%8E%E9%9B%B6%E5%BC%80%E5%A7%8B%E5%9C%A8windows%E4%B8%8B%E5%90%AF%E5%8A%A8openeulerwsl)
- 二、[从 0 开始构建 RISC-V 私有云](https://gitee.com/yuandj/siger/blob/master/%E7%AC%AC10%E6%9C%9F%20%E5%8D%97%E5%BE%81%E5%8C%97%E6%88%98%EF%BC%88%E4%B8%8A12%EF%BC%89.md#%E4%BA%8C%E4%BB%8E-0-%E5%BC%80%E5%A7%8B%E6%9E%84%E5%BB%BA-risc-v-%E7%A7%81%E6%9C%89%E4%BA%91)
  - 见证社区成长：[优矽科技、芯来科技 加入 openEuler](https://gitee.com/yuandj/siger/blob/master/%E7%AC%AC10%E6%9C%9F%20%E5%8D%97%E5%BE%81%E5%8C%97%E6%88%98%EF%BC%88%E4%B8%8A12%EF%BC%89.md#%E8%A7%81%E8%AF%81%E7%A4%BE%E5%8C%BA%E6%88%90%E9%95%BF%E4%BC%98%E7%9F%BD%E7%A7%91%E6%8A%80%E8%8A%AF%E6%9D%A5%E7%A7%91%E6%8A%80-%E5%8A%A0%E5%85%A5-openeuler)
  - [SUMMER 2021 & NutShell](https://gitee.com/yuandj/siger/blob/master/%E7%AC%AC10%E6%9C%9F%20%E5%8D%97%E5%BE%81%E5%8C%97%E6%88%98%EF%BC%88%E4%B8%8A12%EF%BC%89.md#-summer-2021--nutshell)
- 三、[一生一芯 NutShell 国科大](https://gitee.com/yuandj/siger/blob/master/%E7%AC%AC10%E6%9C%9F%20%E5%8D%97%E5%BE%81%E5%8C%97%E6%88%98%EF%BC%88%E4%B8%AD3%EF%BC%89.md)
- 四、[欧拉指北 从 0 到 10](https://gitee.com/yuandj/siger/blob/master/%E7%AC%AC10%E6%9C%9F%20%E5%8D%97%E5%BE%81%E5%8C%97%E6%88%98%EF%BC%88%E4%B8%8B456%EF%BC%89.md#%E5%9B%9B%E6%AC%A7%E6%8B%89%E6%8C%87%E5%8C%97-%E4%BB%8E-0-%E5%88%B0-10)
- 五、[SIG组生态树 73-81 SIG 治理地图 生机盎然](https://gitee.com/yuandj/siger/blob/master/%E7%AC%AC10%E6%9C%9F%20%E5%8D%97%E5%BE%81%E5%8C%97%E6%88%98%EF%BC%88%E4%B8%8B456%EF%BC%89.md#%E4%BA%94sig%E7%BB%84%E7%94%9F%E6%80%81%E6%A0%91-73---81-sig-%E6%B2%BB%E7%90%86%E5%9C%B0%E5%9B%BE-%E7%94%9F%E6%9C%BA%E7%9B%8E%E7%84%B6)
  - [社区成立73个特别兴趣小组（SIG）](https://gitee.com/yuandj/siger/blob/master/%E7%AC%AC10%E6%9C%9F%20%E5%8D%97%E5%BE%81%E5%8C%97%E6%88%98%EF%BC%88%E4%B8%8B456%EF%BC%89.md#%E7%A4%BE%E5%8C%BA%E6%88%90%E7%AB%8B73%E4%B8%AA%E7%89%B9%E5%88%AB%E5%85%B4%E8%B6%A3%E5%B0%8F%E7%BB%84sig)
  - [17 sigs not in table:](https://gitee.com/yuandj/siger/blob/master/%E7%AC%AC10%E6%9C%9F%20%E5%8D%97%E5%BE%81%E5%8C%97%E6%88%98%EF%BC%88%E4%B8%8B456%EF%BC%89.md#17-sigs-not-in-table)
  - 大作业：[北向 sig-minzuchess 南向 sig-riscv](https://gitee.com/yuandj/siger/blob/master/%E7%AC%AC10%E6%9C%9F%20%E5%8D%97%E5%BE%81%E5%8C%97%E6%88%98%EF%BC%88%E4%B8%8B456%EF%BC%89.md#%E5%A4%A7%E4%BD%9C%E4%B8%9A%E5%8C%97%E5%90%91-sig-minzuchess-%E5%8D%97%E5%90%91-sig-riscv)
- 六、[用 RISC-V 赋予的全栈能力，打破条条框框，轻装前进！](https://gitee.com/yuandj/siger/blob/master/%E7%AC%AC10%E6%9C%9F%20%E5%8D%97%E5%BE%81%E5%8C%97%E6%88%98%EF%BC%88%E4%B8%8B456%EF%BC%89.md#%E5%85%AD%E7%94%A8-risc-v-%E8%B5%8B%E4%BA%88%E7%9A%84%E5%85%A8%E6%A0%88%E8%83%BD%E5%8A%9B%E6%89%93%E7%A0%B4%E6%9D%A1%E6%9D%A1%E6%A1%86%E6%A1%86%E8%BD%BB%E8%A3%85%E5%89%8D%E8%BF%9B)

---

## 三、[一生一芯](https://gitee.com/flame-ai/hello-openEuler/issues/I3CHPZ#note_5046553_link) NutShell 国科大

> 赵大娘：这孩子啊！是刘永贵的，打出世以来啊，还没见过他爹什么样呢。唉！JIANG介石啊，不知糟蹋了多少人的好日子啊？  
> 高营长：是啊！大娘，为了以后孩子们永远过好日子，眼前就得忍着点儿。

[![输入图片说明](https://images.gitee.com/uploads/images/2021/0408/225637_734c4ed7_5631341.png "在这里输入图片标题")](https://gitee.com/flame-ai/hello-openEuler/issues/I2DRB3#note_4740353_link)

- （[上集](http://blog.sina.com.cn/s/blog_e781b75e01030767.html)）: 这是大踏步撤退退出根据地的情景，老百姓不理解，但相信部队一定能打回来。部队的指战员有情绪，但思想上是有信心的，因为他们有着共同的愿景，为了以后孩子们永远过上好日子。这和大娘的话 “J介石啊，不知糟蹋了多少人的好日子” 形成了鲜明的对比。（刘永贵是高营长的机枪手，都到家门口了，为了转移都不能进家门儿。）

> 李、刘：呵呵……  
> 师政委（不解地）：你们笑什么？  
> 李 进：我们笑，北撤的时候，有些同志还闹情绪呢。呵呵……  
> 师政委：是啊！离开了老根据地，离开了自己的家乡，总会有点不痛快。可是为了将来……。同志们！我们将来是怎么回事儿？  
> 战 士1：解放咱们全中国。  
> 师政委：对！就是为了这个，可是J介石是不会甘心的。我们还要和各个解放区的兄弟部队一样，按照领袖的办法，还要多跑路，多打像凤凰山这样的歼灭战，而且要越打越漂亮，才能解决问题。现在敌人又在陕北发动进攻了，胡宗南调了11个旅向延安进犯。  
> 陈德海：这还行？JIANG介石这小子想占咱们延安？  
> 李 进（站了起来）：政委，给上级发电报，我们用打胜仗来保卫延安。  
> 王 春（站了起来）：对！打电报。  
> 剧情：战士们都义愤填膺地站了起来。  
> 师政委：对！（站了起来）同志们，我们再加把劲，坚决地把面前这部分敌人给消灭掉，用胜利来保卫延安。

[![输入图片说明](https://images.gitee.com/uploads/images/2021/0509/001109_bd5201e8_5631341.png "屏幕截图.png")](https://gitee.com/flame-ai/hello-openEuler/issues/I2DRB3#note_4740353_link)

国科大 “一生一芯” [NutShell](https://gitee.com/RV4Kids/NutShell) [docs](https://gitee.com/RV4Kids/NutShell-doc) 的两个仓留待进一步学习，这个 sig-RISC-V 的南向起点的意义，则是本期重点的学习内容。一篇知乎的旧闻  #[I3POU4](https://gitee.com/RV4Kids/NutShell/issues/I3POU4) [如何评价中国科学院大学「一生一芯」计划？对国产芯片的发展意味着什么？](https://www.zhihu.com/question/409298856/answer/1366307211)，[全部 237 个评论问答摘要](https://gitee.com/RV4Kids/NutShell/issues/I3POU4#note_5022446_link) 研读过后，这样一个全栈学习机会 被全国同学的垂涎 一目了然。而 赞成 : 反对 的比例，让我有了一丝担忧：[141:89:24 = 1.5842696629213483146067415730337](https://gitee.com/flame-ai/hello-openEuler/issues/I3CHPZ#note_5046553_link) 。相当于 8:5 这其中羡慕的同学也包含其中，客观的社区大约是 1:1 ，这说明社区的观望情绪。这和《南征北战》的思想工作的局面无法比拟，这正是我们要向前辈学习的地方。更是应该深思的地方。我把 营长和大娘的对话，比拟做 “北极星” 这是深入骨髓的 上下一心，而师政委对行动的精确解读，是对战术配合战略的分析，是 “北向”。这样分析下来，重新全面审视 RISC-V 的战略战术，就更加能有的放矢啦。这里，留待大家交流，仅此抛砖引玉。
