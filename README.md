# SIGer

#### 介绍
青少年开源文化的阵地 - SIGer 兴趣小组（科普期刊）

这个仓会汇集各种兴趣小组的文件，想法。采用 SIG 管理架构，通过 PR 分享各自社团的学习成果。寄希望成为一个学生社团管理的宝典和百科。

同时，所有的 SIGer 兴趣小组，都应该具备数字化工具，专属工具应该被推荐，必定每个小组有自己的特点。通用工具肯定是必备的。协同创作本身，就符合开源社区的标配。而 SIGer 使用的起点工具 GITEE，只是一个脚手架的作用，未来一定会被全新的工具所替代。就如 LINUX 之于 minux.

#### 开源

《[ **开源治理的根基** 是教育和赋能！](第14期%20开源治理的根基是教育和赋能！.md)》 #14th

- _**[教育](./%E4%B8%93%E8%AE%BF/%E3%80%90RMS%E3%80%91Free%20Software,%20Free%20Society.md#%E6%95%99%E8%82%B2)**_ 即指明方向，指北就是文化选择的问题，要什么样的生活的问题。  
-  **[赋能](https://gitee.com/shiliupi/shiliupi99/blob/master/MagSi/Issue1%20-%20The%20Personal%20Programmable%20Computer%20-%20Shiliu%20Pi%2099.md)**  是赋能未来，赋能青少年，去肩负创造未来美好生活的使命，即理想和信念。

#### 最新 [STEM](STEM/) [LIFE](生活/) [2022](2022%20海报集.md)

《[STEM 在上海](第2期%20STEM%20in%20ShangHai.md)》(1.8) 2nd 《[中文化 PYTHON](第16期%20中文化%20PYTHON.md)》(1.6) 16th @ 2022

[![8th](https://images.gitee.com/uploads/images/2022/0105/221324_7c8379c1_5631341.png "屏幕截图.png")](第8期%20星辰大海II%20-%20宇宙.md) [![17](https://images.gitee.com/uploads/images/2022/0105/221033_7e7d72c0_5631341.png "屏幕截图.png")](第17期%20数学之美《丘成桐传》.md)

《[计算机博弈的国际范儿 LudiiGames](第7期%20计算机博弈的国际范儿%20LudiiGames.md)》（[民族棋中国史](https://gitee.com/blesschess/LuckyLudii/) 专题）7th  
《[RISC-V指令扩展加速特型数独生成数独](RISC-V/Shinning%20Star.md)》（闪闪红星 专题）26th

[![输入图片说明](https://images.gitee.com/uploads/images/2021/1230/155427_723146af_5631341.png)](第7期%20计算机博弈的国际范儿%20LudiiGames.md) [![输入图片说明](https://images.gitee.com/uploads/images/2022/0108/020409_433ff9e3_5631341.png "屏幕截图.png")](http://shiliupi.cn/2021/12/24/gnu99/)

《[SIGer 周年庆](https://images.gitee.com/uploads/images/2021/1230/154825_ea5b1875_5631341.jpeg)，[吉牛象棋火种队再出发！](http://shiliupi.cn/2021/12/24/gnu99/)》海报赏析 [I](https://images.gitee.com/uploads/images/2021/1230/154811_d647eb1e_5631341.jpeg)/[II](https://images.gitee.com/uploads/images/2021/1230/154825_ea5b1875_5631341.jpeg)

[![输入图片说明](https://images.gitee.com/uploads/images/2021/1207/132155_5af09a7e_5631341.png)](STEM/The%20Computers%20That%20Made%20Britain%20%E5%B0%81%E9%9D%A2%EF%BC%8C%E7%9B%AE%E5%BD%95%EF%BC%8C%E4%BB%8B%E7%BB%8D.md) [![输入图片说明](https://images.gitee.com/uploads/images/2021/1207/131847_3fe5c8ec_5631341.png)](STEM/无为堂记：计算机历史博物馆.md)

- [The Computers That Made Britain](STEM/The%20Computers%20That%20Made%20Britain%20%E5%B0%81%E9%9D%A2%EF%BC%8C%E7%9B%AE%E5%BD%95%EF%BC%8C%E4%BB%8B%E7%BB%8D.md)
- [计算机历史博物馆](STEM/无为堂记：计算机历史博物馆.md)

[![输入图片说明](https://images.gitee.com/uploads/images/2021/1207/131521_9ebe95e8_5631341.png)](第15期%20量子计划%20“稚辉君”%20不负青丝，向善而行.md) [![输入图片说明](https://images.gitee.com/uploads/images/2021/1207/131650_b4928408_5631341.png)](少年/26th%20夸克石榴派实现少年梦.md)

《[“ **稚辉君** ” 不负青丝，向善而行！](第15期%20量子计划%20“稚辉君”%20不负青丝，向善而行.md)》 #15th [#26th](少年/26th%20夸克石榴派实现少年梦.md)  

#### 优秀 [MagSi](https://gitee.com/shiliupi/shiliupi99/tree/master/MagSi)

《[全栈学习平台 —— 可编程个人计算机](https://gitee.com/shiliupi/shiliupi99/blob/master/MagSi/Issue1%20-%20The%20Personal%20Programmable%20Computer%20-%20Shiliu%20Pi%2099.md)》 ——  **[石榴派](http://ShiliuPi.cn)**   
1st [The Personal Programmable Computer - Shiliu Pi 99](https://gitee.com/shiliupi/shiliupi99/blob/master/MagSi/Issue1%20-%20The%20Personal%20Programmable%20Computer%20-%20Shiliu%20Pi%2099.md)  
2nd [Opreating System Shiliu Pi 99 was Released](https://gitee.com/shiliupi/shiliupi99/blob/master/MagSi/Issue2%20-%20Opreating%20System%20Shiliu%20Pi%2099%20was%20Released.md)

- 石榴派 漂流计划 Shiliu Pi (Pomegranate Pi) Drift Plan. 安全抵英 ...
- 祺福“石榴派”早日装备到火种志愿者手中。为万千少年带去欢乐！
- [ShiliuPi.cn](http://ShiliuPi.cn) 官网发布，汇集更多 "世界范围" 的优秀民族棋 样本。

 **《[Better RISC-V For Better Life](第4期%20Better%20RISC-V%20For%20Better%20Life.md)》#4th** 

[![输入图片说明](https://images.gitee.com/uploads/images/2021/0325/124434_b68e4799_5631341.png "屏幕截图.png")](第4期%20Better%20RISC-V%20For%20Better%20Life.md) [![输入图片说明](https://images.gitee.com/uploads/images/2021/0720/204922_166bcbeb_5631341.png "屏幕截图.png")](https://images.gitee.com/uploads/images/2021/0715/075956_be1fb9ad_5631341.jpeg)

> @[孔令军](专访/【孔令军】RVWeekly%20SIGer+%20青少年增刊.md) @[PicoRio](RISC-V/PicoRio.md) @[浪潮RISC-V小组](RISC-V/riscv.md) @[RV与芯片评论](RISC-V/rvnews.md) @[CRVA](RISC-V/crvs2020.md) @[RV4Kids](RISC-V/RV4Kids.md) 依照本期 SIGer 专刊的 《[开源芯片 RISC-V](https://gitee.com/yuandj/siger/issues/I3B6OG) 》的主题，一篇一篇 md 入库。[JOHN L. HENNESSY DAVID A. PATTERSON](RISC-V/JOHN-L-HENNESSY-DAVID-A-PATTERSON.md) [COMPUTER ARCHITECTURE](RISC-V/a-new-golden-age-for-computer-architecture.md) 这些关键字映入眼帘，没有比这个时刻更加激动人心的啦，一个崭新的[黄金时代](RISC-V/a-new-golden-age-for-computer-architecture.md)到来啦。

 **[RV4Kids](RISC-V/RV4Kids.md)** ：  _中文名（[ **RV少年** ](https://gitee.com/RV4Kids)）_ 

我极力地推荐 [RV4Kids](RISC-V/RV4Kids.md) 这个崭新的教育品牌，是我和 [RVWeekly](https://gitee.com/inspur-risc-v/RVWeekly) 主编[孔令军](专访/【孔令军】RVWeekly%20SIGer+%20青少年增刊.md)先生共同推荐的一个科普项目，我们邀请 RISC-V 的全体同仁和社区都能展开双臂拥抱青少年，您只需在您的实践项目（开源项目）中 @[RV4Kids](https://gitee.com/RV4Kids) 字样，尽您所能，在您开展的 RISC-V 相关的活动，课程，工作坊，等等可以吮吸 RISC-V 知识的场合，为青少年提供便利，允许他们参与，安静地来，悄悄地走，积极提问，求知若渴。谁能知道，不远的将来，他们中的某人会成为您的同事，甚至战友。“ **身负强国使命，与一众小伙伴各种厮杀** ” 呢？@小孔语录

[![输入图片说明](https://images.gitee.com/uploads/images/2021/0628/193300_6ee28677_5631341.png "屏幕截图.png")](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/Shiliu%20PI,%20Shiliu%20Silicon,%20Shiliu%20Si,%20%E7%9F%B3%E6%A6%B4%E6%A0%B8.md) [![输入图片说明](https://images.gitee.com/uploads/images/2021/0628/193244_7327bba1_5631341.png "屏幕截图.png")](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/CNRV%202021.6.%2021.%20-%2022.md)

> 学习小组 是 SIGer 的延伸，以 RISCV 为学习目标，分享实验课心得，成为 [RV4kids](https://gitee.com/RV4Kids/RVWeekly/tree/master/RV4Kids) 的主要内容，编辑本身就是学习。这符合 SIGer 编委的工作和学习内容。唯有分享，才是最好的学习方法。创刊后，以青少年科普新势力受邀 CNRV 中国峰会，产出两期：2《[Shiliu Si 石榴核](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/Shiliu%20PI,%20Shiliu%20Silicon,%20Shiliu%20Si,%20%E7%9F%B3%E6%A6%B4%E6%A0%B8.md)》，3《[中国峰会 日记](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/CNRV%202021.6.%2021.%20-%2022.md)》。并伴随暑期 石榴派实验课的启动，4《[一生一芯](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/%E4%B8%80%E7%94%9F%E4%B8%80%E8%8A%AF3.md)》5《[FPGA 学习笔记](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/RV4kids%205th%20FPGA%20study.md)》等专题陆续登场。锁定 6《[全栈学习平台——可编程个人计算机](https://gitee.com/shiliupi/shiliupi99/blob/master/MagSi/Issue1%20-%20The%20Personal%20Programmable%20Computer%20-%20Shiliu%20Pi%2099.md)》后，将学习小组的学习方法沉淀为 7《[IC WIKI](https://gitee.com/RV4Kids/RVWeekly/issues/I40Z0P)》的学习笔记的知识分享。将 8《[SIGer 编辑手册](专访/【石榴派】SIGer%20编辑手册.md)》纳入学习小组序列，一共八期，统一风格，见 [增刊](#%E5%A2%9E%E5%88%8A)。

#### 往期 [SIGer 兴趣地图](Tools/「选题」「立项」SIGer%20兴趣地图%202022.3.4.md) 2022.3.4

- [第29期 香山处理器 “南湖” 国重十周年:包云岗](RISC-V/RV4kids%2020th%20-%20Nan%20Hu.md) (2021.12.7) 20th 
- [第28期 计算机历史博物馆](STEM/无为堂记：计算机历史博物馆.md) (2021.11.23) 19th
- [第27期 1980's 英国家用电脑革命](STEM/The%20Computers%20That%20Made%20Britain%20封面，目录，介绍.md) (2021.11.19) 18th
- [第26期 RISC-V指令扩展加速特型数独生成数独](RISC-V/Shinning%20Star.md) (2021.12.27)（闪闪红星）40th
- [第25期 世上无难事只要肯登攀](https://images.gitee.com/uploads/images/2021/1230/154811_d647eb1e_5631341.jpeg) (2021.12.25) [SIGer 1周年](https://images.gitee.com/uploads/images/2021/1230/154825_ea5b1875_5631341.jpeg) 贺 [吉牛火种队](http://shiliupi.cn/2021/12/24/gnu99/)！39th
- [第24期 书法少年书写未来中国](生活/书法少年书写未来中国.md) (2021.12.17) 33th
- [第23期 用镜头记录生活](生活/用镜头记录生活.md) (2021.12.22) 32th
- [第22期 SIGer 少年，样貌几何？](第22期%20SIGer%20少年，样貌几何？.md) (2021.12.17) 31th
- [第21期 小觅新视界](第21期%20小觅新视界.md) (2022.1.17) 30th [一起建造火焰棋塔](第21期%20小觅新视界.md#大作业-building-a-flamechess-tower-by-heart)
- [第20期 新青年II "兴趣爱好" 随手小画](第20期%20新青年II%20-%20兴趣爱好%20随手小画.md) (2021.12.7) 29th
- [第19期 OpenEyeTap - AR glasses On RPi Zero2w](STEM/OpenEyeTap%20-%20AR%20glasses%20based%20Raspberry%20Pi%20Zero%202w.md) (2021.11.26)
- [第18期 新青年 "校园智遇" 特刊](第18期%20新青年.md) (2021.8.25)
- [第17期 数学之美《丘成桐传》](第17期%20数学之美《丘成桐传》.md) (2022.1.3)
- [第16期 中文化 PYTHON.md](第16期%20中文化%20PYTHON.md) (2022.1.6)
- [第15期 量子计划 “稚辉君” 不负青丝，向善而行](第15期%20量子计划%20“稚辉君”%20不负青丝，向善而行.md) (2021.10.25) [少年](少年/)/[缘 26th](少年/26th%20夸克石榴派实现少年梦.md)
- [第14期 开源治理的根基是教育和赋能！](第14期%20开源治理的根基是教育和赋能！.md) (2021.7.31)
- [第13期 小冰我们的朋友你好吗？](第13期%20小冰我们的朋友你好吗？.md) (2022.1.15)
- [第12期 开源道场 DOJO CHINA 的构建](第12期%20开源道场%20DOJO%20CHINA%20的构建.md) (2022.1.13)
- [第11期 ACM-ICPC 未来版](第11期%20ACM-ICPC%20未来版.md) (2021.12.30)
- [第10期 南征北战](%E7%AC%AC10%E6%9C%9F%20%E5%8D%97%E5%BE%81%E5%8C%97%E6%88%98%EF%BC%88%E4%B8%8A12%EF%BC%89.md) [上](%E7%AC%AC10%E6%9C%9F%20%E5%8D%97%E5%BE%81%E5%8C%97%E6%88%98%EF%BC%88%E4%B8%8A12%EF%BC%89.md)/[中](%E7%AC%AC10%E6%9C%9F%20%E5%8D%97%E5%BE%81%E5%8C%97%E6%88%98%EF%BC%88%E4%B8%AD3%EF%BC%89.md)/[下](%E7%AC%AC10%E6%9C%9F%20%E5%8D%97%E5%BE%81%E5%8C%97%E6%88%98%EF%BC%88%E4%B8%8B456%EF%BC%89.md) (2021.5.11)
- [第9期 我们的未来是星辰大海！](第9期：我们的未来是星辰大海！.md) (2021.9.1)
- [第8期 星辰大海II - 宇宙](第8期%20星辰大海II%20-%20宇宙.md) (2022.1.5)
- [第7期 计算机博弈的国际范儿 LudiiGames](第7期%20计算机博弈的国际范儿%20LudiiGames.md) (2021.12.29)
- 第6期 [趣派专题](https://images.gitee.com/uploads/images/2022/0108/204558_52a4c3ae_5631341.png)：[Joy@PIday, Joy@everyday](https://talk.quwj.com/topic/2119). (2021.3.14) [Zero2w 19th](STEM/OpenEyeTap%20-%20AR%20glasses%20based%20Raspberry%20Pi%20Zero%202w.md)
- [第5期 CANSAT 我的航天梦](第5期%20CANSAT%20我的航天梦.md) (2022.1.13)
- [第4期 Better RISC-V For Better Life](第4期%20Better%20RISC-V%20For%20Better%20Life.md) (2021.3.21)
- [第3期 为中国而教 2021 教育创客集](第3期%20为中国而教%202021%20教育创客集.md) (2021.12.30)
- [第2期 STEM in ShangHai](第2期%20STEM%20in%20ShangHai.md) (2022.1.8)
- [第1期 迎接我们未来的生活方式](第1期%20迎接我们未来的生活方式.md) (2021.3.5)
- [第0期 Hello, openEuler! 拥抱未来](第0期%20Hello,%20openEuler!%20拥抱未来.md) (2021.2.3)

#### 增刊

 [![输入图片说明](https://images.gitee.com/uploads/images/2021/1223/214925_f8604a16_5631341.png "屏幕截图.png")](生活/书法少年书写未来中国.md)  [![输入图片说明](https://images.gitee.com/uploads/images/2021/1223/215128_f36556d4_5631341.png "屏幕截图.png")](RISC-V/RV4kids%2020th%20-%20Nan%20Hu.md)

- 青春主题，完全由 “SIGer” 执行编委 主笔，呼应 “[缘起](https://gitee.com/yuandj/siger/issues/I3MXUP)”。[18](%E7%AC%AC18%E6%9C%9F%20%E6%96%B0%E9%9D%92%E5%B9%B4.md)/[20](第20期%20新青年II%20-%20兴趣爱好%20随手小画.md)  
  [生活](生活/)专栏：《[用镜头记录生活](生活/用镜头记录生活.md)》23th《[书法少年书写未来中国](生活/书法少年书写未来中国.md)》24th
- [RV4Kids](https://gitee.com/RV4Kids/) 1st 《[星辰，机遇，使命](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/RV4Kids%201st%20%E3%80%8A%E6%98%9F%E8%BE%B0%EF%BC%8C%E6%9C%BA%E9%81%87%EF%BC%8C%E4%BD%BF%E5%91%BD%E3%80%8B1.1.md)》 - [RV 一起学](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/RV4Kids%201st%20%E3%80%8A%E6%98%9F%E8%BE%B0%EF%BC%8C%E6%9C%BA%E9%81%87%EF%BC%8C%E4%BD%BF%E5%91%BD%E3%80%8B1.2.md#rv%E4%B8%80%E8%B5%B7%E5%AD%A6)  
  （[1](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/RV4Kids%201st%20%E3%80%8A%E6%98%9F%E8%BE%B0%EF%BC%8C%E6%9C%BA%E9%81%87%EF%BC%8C%E4%BD%BF%E5%91%BD%E3%80%8B1.1.md)/[2](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/Shiliu%20PI,%20Shiliu%20Silicon,%20Shiliu%20Si,%20%E7%9F%B3%E6%A6%B4%E6%A0%B8.md)/[3](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/CNRV%202021.6.%2021.%20-%2022.md)/[4](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/%E4%B8%80%E7%94%9F%E4%B8%80%E8%8A%AF3.md)/[5](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/RV4kids%205th%20FPGA%20study.md)/[6](https://gitee.com/shiliupi/shiliupi99/blob/master/MagSi/Issue1%20-%20The%20Personal%20Programmable%20Computer%20-%20Shiliu%20Pi%2099.md)/[7](https://gitee.com/RV4Kids/RVWeekly/issues/I40Z0P)/[8](https://gitee.com/flame-ai/siger/blob/master/%E4%B8%93%E8%AE%BF/%E3%80%90%E7%9F%B3%E6%A6%B4%E6%B4%BE%E3%80%91SIGer%20%E7%BC%96%E8%BE%91%E6%89%8B%E5%86%8C.md)/[9](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/ISSUE%209%20-%20RV%20%E5%B0%91%E5%B9%B4.md)/[10](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/issue%2010%20-%20inspur.md)/[11](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/Issue%2011%20-%20GNUgames%2099.md)/[12](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/Issue%2012%20-%20TurboLinux.md)/[13](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/%20Issue%2013%20-%20Codasip.md)/[14](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/Issue%2014%20-%20Difftest%20Learning.md)/[15](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/Issue%2015%20-%20Ubuntu%20heart.md)/[16](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/Issue%2016%20-%20W%20R%20GNUgamer.md)） - [RV 少年](https://gitee.com/RV4Kids)  
  《[香山处理器 “南湖” 国重十周年:包云岗](RISC-V/RV4kids%2020th%20-%20Nan%20Hu.md)》20th  

#### 番外

[![输入图片说明](https://images.gitee.com/uploads/images/2021/0330/095806_af12450a_5631341.png "屏幕截图.png")](./%E7%AC%AC9%E6%9C%9F%EF%BC%9A%E6%88%91%E4%BB%AC%E7%9A%84%E6%9C%AA%E6%9D%A5%E6%98%AF%E6%98%9F%E8%BE%B0%E5%A4%A7%E6%B5%B7%EF%BC%81.md) [![输入图片说明](https://images.gitee.com/uploads/images/2021/1213/063013_dd20962a_5631341.png "屏幕截图.png")](https://images.gitee.com/uploads/images/2021/1213/054023_8aaef31e_5631341.jpeg)

最近喜欢 B站 的学习氛围，也来个二次元，SIGer 本不分内外，本番意在连接，上荐语：

- [趣派社区](https://talk.quwj.com/topic/2119)是我的称呼，我从那里知道了 “考古”，体验了图床、B站，现有了 **[Zero2W](STEM/OpenEyeTap%20-%20AR%20glasses%20based%20Raspberry%20Pi%20Zero%202w.md)** 
- [MARS](第9期：我们的未来是星辰大海！.md) ：“社区的同频共振，认同并实践「分享创造连接 协作产生价值」” [星辰大海 II](第8期%20星辰大海II%20-%20宇宙.md)

#### 贡献

1.  Fork 本仓库
2.  新建 siger xxx 分支 （xxx为您选择的兴趣小组的名字sig，任何分支，请务必指修改一个SIG目录）
3.  提交代码 （这里是您贡献的新资料，或者代码。）
4.  新建 Pull Request 
   （请在PR前，一定要保存好自己的本地更新，并在此之前，对master的版本变更，做到心中有数）

详见：[how to become a SIGer? 编委是如何工作的？](%E7%AC%AC1%E6%9C%9F%20%E8%BF%8E%E6%8E%A5%E6%88%91%E4%BB%AC%E6%9C%AA%E6%9D%A5%E7%9A%84%E7%94%9F%E6%B4%BB%E6%96%B9%E5%BC%8F.md#4-%E4%BB%BB%E5%8A%A1how-to-become-a-siger-%E7%BC%96%E5%A7%94%E6%98%AF%E5%A6%82%E4%BD%95%E5%B7%A5%E4%BD%9C%E7%9A%84%E5%BC%80%E6%BA%90%E7%A7%91%E6%99%AE-siger-1%E6%9C%9F-%E6%8E%A8%E5%B9%BF%E6%96%B9%E6%A1%88-%E9%A1%BA%E5%88%A9%E5%AE%9E%E6%96%BD)

![输入图片说明](https://images.gitee.com/uploads/images/2021/0307/094640_bdeeac29_5631341.png "屏幕截图.png")

附：《[SIGer 编辑手册](专访/【石榴派】SIGer%20编辑手册.md)》2021-7-20 [缘起、志愿者岗位、五大项目](第20期%20新青年II%20-%20兴趣爱好%20随手小画.md) 12-7

[![输入图片说明](https://images.gitee.com/uploads/images/2021/1230/154454_14932eeb_5631341.png "屏幕截图.png")](http://shiliupi.cn/2021/12/24/gnu99/)

#### 伴礼

出游返乡，拜访故友，总不可空手去空手回，礼尚往来自古有之。  
访友皆是客，回礼相送，均为自制，请笑纳。  
附教程，供您使用，只愿将开源文化分享的更远更广。

[![输入图片说明](https://images.gitee.com/uploads/images/2021/0721/085600_511ffead_5631341.png "213030_ac45c7da_5631341副本.png")](https://gitee.com/shiliupi/shiliupi99)

[![输入图片说明](https://images.gitee.com/uploads/images/2021/0513/230721_16a40023_5631341.png "屏幕截图.png")](https://gitee.com/mixshare/occupymars) [![输入图片说明](https://images.gitee.com/uploads/images/2021/0513/230844_0b125c03_5631341.png "屏幕截图.png")](https://gitee.com/flame-ai/caihongqiao)

<a href="Tools/stemINshanghai2.pdf" target="_blank"><img src="https://images.gitee.com/uploads/images/2022/0226/073735_65454439_5631341.jpeg" title="SIGer 印刷品样刊 2022年1月出品，倍塔狗助印"></a>

#### 吾愿

- 祺福天下孩童，健康快乐！ :pray:  :pray:  :pray:  :pray:  :pray:  :pray:  :pray:  :pray:  :pray: 

  > 请您支持，共同祈愿，分享寄语：  
为自己，为家人，为朋友，为他人，为世界和平，均可。  
伴以的捐赠，请包含数字 3/6/9 ，请相信愿的力量，求天下与您共同见证。 :pray: 

[![best wishes 1](https://images.gitee.com/uploads/images/2022/0226/075543_c5cf5498_5631341.png "1partOF5wishes99-02.png")](Tools/1partOF5wishes99-02.png)
[![best wishes 2](https://images.gitee.com/uploads/images/2022/0226/075648_2eadb0ce_5631341.png "1partOF5wishes99-03.png")](Tools/1partOF5wishes99-03.png)
[![best wishes 3](https://images.gitee.com/uploads/images/2022/0226/075735_3f650253_5631341.png "1partOF5wishes99-04.png")](Tools/1partOF5wishes99-04.png)
[![best wishes 4](https://images.gitee.com/uploads/images/2022/0226/075810_6c4f85ac_5631341.png "1partOF5wishes99-05.png")](Tools/1partOF5wishes99-05.png)
[![best wishes 5](https://images.gitee.com/uploads/images/2022/0226/075926_2bd9bb38_5631341.png "1partOF5wishes99-06.png")](Tools/1partOF5wishes99-06.png)