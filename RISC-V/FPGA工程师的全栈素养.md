- [85后 FPGA 工程师 的 全栈素养！](https://gitee.com/shiliupi/easy_raspi_python_one/issues/I4RCXS#note_8408557_link)
- [这碗鸡汤我干啦！大飞随意！:pray:](https://gitee.com/shiliupi/easy_raspi_python_one/issues/I4RCXS)
- @[史然飞](https://gitee.com/shiranfei) : [我都可以，您一定比我强多了](https://gitee.com/shiliupi/easy_raspi_python_one/tree/master/%E8%B5%A0%E8%A8%80%E2%80%94%E2%80%94%E6%88%91%E9%83%BD%E5%8F%AF%E4%BB%A5%EF%BC%8C%E6%82%A8%E4%B8%80%E5%AE%9A%E6%AF%94%E6%88%91%E5%BC%BA%E5%A4%9A%E4%BA%86)

<p><img width="706px" src="https://images.gitee.com/uploads/images/2022/0327/105710_d9eac01c_5631341.jpeg" title="dafei.jpg"></p>

> 我是嵌入式工程师出身，发现我的当前阶段的职业规划与老石《[FPGA工程师最核心的就是全栈能力](http://www.sohu.com/a/295982108_453160)》基本一致，不论嵌入式工程师还是FPGA工程师，最后都要晋级为软硬件系统皆精通的全栈工程师（天下武功，殊途同归）。

> 全栈工程师就到头了吗？不，学海无涯啊。 **全栈工程师** 向上，还有 **算法全栈工程师** （自己发明的名词，就是理解用户需求，将算力合理的分配套PS端和PL端，在全栈工程师基础上，具有算法硬件化的能力）， **数学工程师** （在算法全栈工程师基础上，有了大量的工程实践，理解当前计算机体系架构背后的数学意义）。

用大飞老师的原文做引言最适合本期主题，全栈 -> 栈溢出 是工程师的自我修养的过程，也是正确的学习观，值得 SIGer 全体编委效仿。我更要身先士卒，在全栈之路上努力前行，这是 2022 之初的立项，如果只做转载，内容早就翔实了，只有附上学习笔记，才能真正是名副其实的 “学习小组” 笔记。

- 沿着大飞老师的学习脉络，同步上 FPGA 实验课
- 补上了 NutShell 的 构建实验
- 从头构建 fpga 的 vivado 工程 （未完成任务）
- 谈芯 —— 什么是FPGA工程师的核心竞争力 （新立项）
- [Verilog入門教學](https://gitee.com/RV4Kids/RVWeekly/issues/I3IA6B) 与背景知识 (RV4Kids 遗留, [天璇](https://space.bilibili.com/1563198865))

# 品鉴师 @[史然飞](https://gitee.com/shiranfei) 简介

标准的一枚85后，山东大汉。

寒窗苦读12年，考入鲁东大学。大学四年有两大收货：深深的知道自己将来只适合走技术的道路，考上了大连理工大学研究生。推荐下母校鲁东大学，如果您的学历不想止步于本科，请到鲁东大学这里，这里有浓厚的学习氛围，优良的考研传统，到了鲁东大学，你已经半个身子进入跨入硕士研究生的大门了。

在大连理工大学的两年半的求学生涯中，完成了2大类工程项目，当由于不是计算机科班出生，而且在工作之前对读书兴趣不是那么高，浅浅的看了《C和指针》、《嵌入式实时操作系统uCOS-II》，所以项目之后，感觉自己发虚，只是知其然，但不知其所以然。但硕士研究生的求学生涯，让我看到了我有能力继续从事嵌入式开发工作，同时，项目为我积累了大量工程实践经验，也为我留下了太多的为什么等待我去解答。

毕业后，有幸从事航天事业工作，我的工作仍然是嵌入式系统开发，作为一名新员工，领导为我们提供自我提高的时间，我就开始的嵌入式世界之旅了。

1. 先从接手的ARM处理器项目开始，购买《ARM体系结构与编程》进行学习；

1. C语言学习三件套《C和指针》、《C专家编程》、《C陷阱与缺陷》让我重新认识了我的好伙伴C语；

1. 《深入理解计算机系统(第二版)》(当时出版的是第二版，现在已出第三版了)让我看到了计算机完整的体系架构，我按图索骥，一块块打开操作系统、计算机网络、编译原理、数据库的地图（编译原理和数据库至今还处于初级理解）；

1. 有了整体系统的架构，我开始进入具体的系统Linux系统，从Linux应用编程三件套《UNIX环境高级编程》、《UNIX网络编程.卷1》、《UNIX网络编程.卷2》，理解了linux为我们提用的服务;

1. 这么多服务是怎么实现的?带着这个问题，潜入linux内核，带着三大管氧气瓶——《LINUX设备驱动开发》、《深入理解LINUX网络技术内幕》、《深入理解LINUX内核》来了一场旷日持久的linux深潜，还好氧气带足了，安全上岸；

1. 最后踏上了网路世界旅程，带着三卷《TCP/IP详解》、两卷《TCP/IP路由技术》安全到目的地；

1. 这期间也浏览过linux逸闻轶事类图书、大数据类科普图书还有一些与管理相关的图书。

1. 时间来到了2017年，2016年阿尔法围棋让全世界看到了新发展方向，我也在2017年开始了解、理解深度学习，借助黄校长的“炼数成金”课程，完成了我对深度学习启蒙学习；

1. 在开始深度学习探索之前，我在2018年下了决定，先补齐我计算机世界最重要的短板——算法，借助“算法时空”的“算法导论”系列课程和“炼数成金”的“算法导论课程”，用了1年时间，完成了课程的学习和《算法导论》书籍的粗读和第一遍精读，虽然现在还不能灵活应用，但我已经开始用算法的思维重新理解我的计算机世界了。

1. 离开了学习、工作6年的航天事业，进入了全新的工作方式，在与公司领导和哈工大学校老师们的学习交流中，xilinx提供的包含PS端和PL端的zynq系列SOC进入了我的世界，zynq系列帮我解决了如何将深度学习引入嵌入式领域的难题，其配套到的Vivado 高层次综合（HLS）让我可以在嵌入式端实现自己的网络模型，进行边缘端计算。

1. 正在进行的事业，zynq的引入，我把自己已经建立的属于我的计算机世界打碎，我要重新建立新的属于我的计算机世界，新的世界为我提出了新的挑战，要深入理解深度网络模型的构造和计算方式，如何甄选出或自己改造出硬件友好型深度网络模型，如何要将深度网络模型在PL端实现，如何合理分配计算量到PS端和PL端、让系统整体算力最优等

1. 更远的、更高的山峰。在探索计算机世界中，越来越感觉到数学知识的匮乏，隐约看到，在错综复杂的事物表面下，隐藏简单、和谐而美丽的数学规律。我自己深知，这才是最终要攀登、要到达的高峰。

### 来吧，干了它吧！
从大学的不知所错，到而立之年的目标明确，一切都来自与读书，读书使人明智。

在中国几千万大学生中，我只是最普通的一员，但读书带我重新认识了自己，坚定了我要走的路。

在读书过程中，不仅是知识的增长，更是耐性的磨炼，磨练出一股韧劲。在自己奋斗的道路上，在每次坚持与放弃抉择时，能继续坐冷板凳，耐得住寂寞，让根扎的更深。

经过多年修炼，本人神功已小成——“不论车船、飞机，拿起书来，我就与世隔绝了”。

个人对于读书总结了一套规律，我看书先通读全书、掌握书中知识点的框架，然后精读图书、能大致理解书中主要知识点，然后在工作实践中将新理解的知识点应用进去，边应用、边查书本，经过几个项目后，再连贯性的精读图书，最后过了1年或者再长点(这段时间，会不自觉地在脑海中细细品味图书)，在精读图书，定会有别样的发现。同时，将新的知识，用于工作中，可以让重复性的工作不重复。

再次让我们拿起图书，放下手机，翱翔知识的海洋。

### 漏下“富”
这是工作7年读过的书，读书其实挺耗钱的，这是真金白银呢。为了尽量少花钱，平日攒够一批要买的图书，等到京东、当当打折季一起购买，还是能省不少钱。

您可以参考我的书柜，开始属于你的书籍之旅。

注：

1. 计算机类图书都是工作后购买的，都是正版图书，眼感好。

1. 图片右上角的数学类和自动控制原理，是我的主要专业课。我的数学需要好好补补了。


![输入图片说明](https://images.gitee.com/uploads/images/2022/0119/003155_9ac50bd5_5631341.jpeg "在这里输入图片标题")

# [中学生都能玩的人工智能](https://gitee.com/shiranfei/open_course_for_AI/)

### 介绍

1. 当前中学生、大学生人工智能教学实验大都采用选择模型、选择数据集、使用数据集训练模型，然后进行部署。

2. 模型的选择、训练等工作，对于大多数中学生、大学生还是有一定难度的，而且对计算机配置要求高。

3. 随着5G的大规模普及、人工智能数据分析的成本逐步降低（百度、华为、腾讯都能提供强大的数据分析能力云），越来越多的智能设备、智能应用将出现在我们身边。

4. 本课程采用性价比高的树莓派4B微型计算机+百度AI人工智能云服务，打造一个超低门槛、超低成本的人工智能实验课。

5. 通过本课程，可以学会如何使用像百度AI这样的人工智能云服务，并自己打造我们智能机器人。

6. 本课程选用树莓派4B，除了价格便宜、资料多外，还有一个重要原因：体积小、功耗低，在树莓派制作完毕自己的智能应用后，将树莓派可以直接安装在家里、学校里、
工业现场（现在已有工业级的树莓派）、甚至可以当做您的毕业设计。


### 运行平台

1. 树莓派4B+课程定制的RaspberryOS操作系统。

2. 需要自行购买树莓派专用摄像头、麦克风等。

3. 本课程将在树莓派4B上进行开发，给树莓派外接一个显示屏、键盘、鼠标，将树莓派打造为超强微型计算机。

4. 一定要使用课程定制的RaspberryOS操作系统，因为您需要在树莓派官方的系统上，安装很多软件才能开始本课程学习，这个安装工作相当痛苦。

### 课程资源

1. 学习视频（持续更新中）：B站，搜索“大飞品树莓”，视频名称——《中学生都能玩的人工智能》
    
   链接：https://space.bilibili.com/403216235/channel/detail?cid=134742
     
2. 课程定制系统镜像下载：

   链接：https://pan.baidu.com/s/14KYZl21Hyf3sjjcWxa5_bg 

   提取码：dbe5
    
3. 课程代码：

   国内gitee托管地址：https://gitee.com/shiranfei/open_course_for_AI.git

   国外github托管地址：https://github.com/srf1986/open_course_for_AI.git

4. 课程交流群：

   QQ群：902672207

### 课程目录

- 00.[课程介绍](https://gitee.com/shiranfei/open_course_for_AI/tree/master/00.%E8%AF%BE%E7%A8%8B%E4%BB%8B%E7%BB%8D)
- 01.[巧妇难为无米之炊——课程开发平台介绍与购买清单](https://gitee.com/shiranfei/open_course_for_AI/tree/master/01.%E5%B7%A7%E5%A6%87%E9%9A%BE%E4%B8%BA%E6%97%A0%E7%B1%B3%E4%B9%8B%E7%82%8A%E2%80%94%E2%80%94%E8%AF%BE%E7%A8%8B%E5%BC%80%E5%8F%91%E5%B9%B3%E5%8F%B0%E4%BB%8B%E7%BB%8D%E4%B8%8E%E8%B4%AD%E4%B9%B0%E6%B8%85%E5%8D%95)
- 02.[小派也能背唐诗了——百度语音合成](https://gitee.com/shiranfei/open_course_for_AI/tree/master/02.%E5%B0%8F%E6%B4%BE%E4%B9%9F%E8%83%BD%E8%83%8C%E5%94%90%E8%AF%97%E4%BA%86%E2%80%94%E2%80%94%E7%99%BE%E5%BA%A6%E8%AF%AD%E9%9F%B3%E5%90%88%E6%88%90)
- 03.[小派学我说话，学的还挺像——百度语音识别](https://gitee.com/shiranfei/open_course_for_AI/tree/master/03.%E5%B0%8F%E6%B4%BE%E5%AD%A6%E6%88%91%E8%AF%B4%E8%AF%9D%EF%BC%8C%E5%AD%A6%E7%9A%84%E8%BF%98%E6%8C%BA%E5%83%8F%E2%80%94%E2%80%94%E7%99%BE%E5%BA%A6%E8%AF%AD%E9%9F%B3%E8%AF%86%E5%88%AB%E3%80%80)　
- 04.[小派好聪明啊，都会春联了——百度智能春联创作](https://gitee.com/shiranfei/open_course_for_AI/tree/master/04.%E5%B0%8F%E6%B4%BE%E5%A5%BD%E8%81%AA%E6%98%8E%E5%95%8A%EF%BC%8C%E9%83%BD%E4%BC%9A%E6%98%A5%E8%81%94%E4%BA%86%E2%80%94%E2%80%94%E7%99%BE%E5%BA%A6%E6%99%BA%E8%83%BD%E6%98%A5%E8%81%94%E5%88%9B%E4%BD%9C)
- 05.[小派小派，我做的人工智能机器人](https://gitee.com/shiranfei/open_course_for_AI/tree/master/05.%E5%B0%8F%E6%B4%BE%E5%B0%8F%E6%B4%BE%EF%BC%8C%E6%88%91%E5%81%9A%E7%9A%84%E4%BA%BA%E5%B7%A5%E6%99%BA%E8%83%BD%E6%9C%BA%E5%99%A8%E4%BA%BA)
- 06.[我跟小派聊聊天——小派升级了，他真正理解我了](https://gitee.com/shiranfei/open_course_for_AI/tree/master/06.%E6%88%91%E8%B7%9F%E5%B0%8F%E6%B4%BE%E8%81%8A%E8%81%8A%E5%A4%A9%E2%80%94%E2%80%94%E5%B0%8F%E6%B4%BE%E5%8D%87%E7%BA%A7%E4%BA%86%EF%BC%8C%E4%BB%96%E7%9C%9F%E6%AD%A3%E7%90%86%E8%A7%A3%E6%88%91%E4%BA%86)
- 07.[摇头晃脑，小派也能定位你——百度人脸识别](https://gitee.com/shiranfei/open_course_for_AI/tree/master/07.%E6%91%87%E5%A4%B4%E6%99%83%E8%84%91%EF%BC%8C%E5%B0%8F%E6%B4%BE%E4%B9%9F%E8%83%BD%E5%AE%9A%E4%BD%8D%E4%BD%A0%E2%80%94%E2%80%94%E7%99%BE%E5%BA%A6%E4%BA%BA%E8%84%B8%E8%AF%86%E5%88%AB)
- 08.[我为抗疫做贡献，带好口罩才能进——百度人脸识别2](https://gitee.com/shiranfei/open_course_for_AI/tree/master/08.%E6%88%91%E4%B8%BA%E6%8A%97%E7%96%AB%E5%81%9A%E8%B4%A1%E7%8C%AE%EF%BC%8C%E5%B8%A6%E5%A5%BD%E5%8F%A3%E7%BD%A9%E6%89%8D%E8%83%BD%E8%BF%9B%E2%80%94%E2%80%94%E7%99%BE%E5%BA%A6%E4%BA%BA%E8%84%B8%E8%AF%86%E5%88%AB2)
- 09.[太神奇了，小派有过目不忘的本领——百度文字识别](https://gitee.com/shiranfei/open_course_for_AI/tree/master/09.%E5%A4%AA%E7%A5%9E%E5%A5%87%E4%BA%86%EF%BC%8C%E5%B0%8F%E6%B4%BE%E6%9C%89%E8%BF%87%E7%9B%AE%E4%B8%8D%E5%BF%98%E7%9A%84%E6%9C%AC%E9%A2%86%E2%80%94%E2%80%94%E7%99%BE%E5%BA%A6%E6%96%87%E5%AD%97%E8%AF%86%E5%88%AB)
- 10.[我也能做的智能停车场——百度车牌识别](https://gitee.com/shiranfei/open_course_for_AI/tree/master/10.%E6%88%91%E4%B9%9F%E8%83%BD%E5%81%9A%E7%9A%84%E6%99%BA%E8%83%BD%E5%81%9C%E8%BD%A6%E5%9C%BA%E2%80%94%E2%80%94%E7%99%BE%E5%BA%A6%E8%BD%A6%E7%89%8C%E8%AF%86%E5%88%AB)
- 11.[小派还能认识动物——百度动物识别](https://gitee.com/shiranfei/open_course_for_AI/tree/master/11.%E5%B0%8F%E6%B4%BE%E8%BF%98%E8%83%BD%E8%AE%A4%E8%AF%86%E5%8A%A8%E7%89%A9%E2%80%94%E2%80%94%E7%99%BE%E5%BA%A6%E5%8A%A8%E7%89%A9%E8%AF%86%E5%88%AB)
- 12.[安全驾驶，小派也来帮帮忙——百度驾驶行为分析](https://gitee.com/shiranfei/open_course_for_AI/tree/master/12.%E5%AE%89%E5%85%A8%E9%A9%BE%E9%A9%B6%EF%BC%8C%E5%B0%8F%E6%B4%BE%E4%B9%9F%E6%9D%A5%E5%B8%AE%E5%B8%AE%E5%BF%99%E2%80%94%E2%80%94%E7%99%BE%E5%BA%A6%E9%A9%BE%E9%A9%B6%E8%A1%8C%E4%B8%BA%E5%88%86%E6%9E%90)


# [使用xilinx最新工具vitis2019.2，定制zc702开发板linux系统](https://gitee.com/link?target=https%3A%2F%2Fblog.csdn.net%2Fsrf1986%2Farticle%2Fdetails%2F103263272)

大飞品树莓  于 2019-11-27 19:22:46 发布  8847  收藏 28
分类专栏： 品鉴zynq系列SOC 文章标签： ZYNQ FPGA vitis vivado Linux
版权

品鉴zynq系列SOC

专栏收录该内容
1 篇文章2 订阅
订阅专栏

### 目录

- 需要的工具
- 定制自己的zc702包
  - vivado定制znyq7020板级描述
  - petalinux定制linux发行版
- zc702启动自定制的linux系统
- 小结和后续工作

### 需要的工具
2019年10月9日，xilinx推出新一代FPGA集成开发工具——vitis，喜欢尝鲜的我，安装了vitis来进行FPGA开发。

我是嵌入式工程师出身，发现我的当前阶段的职业规划与老石《[FPGA工程师最核心的就是全栈能力](http://www.sohu.com/a/295982108_453160)》基本一致，不论嵌入式工程师还是FPGA工程师，最后都要晋级为软硬件系统皆精通的全栈工程师（天下武功，殊途同归）。

全栈工程师就到头了吗？不，学海无涯啊。全栈工程师向上，还有算法全栈工程师（自己发明的名词，就是理解用户需求，将算力合理的分配套PS端和PL端，在全栈工程师基础上，具有算法硬件化的能力），数学工程师（在算法全栈工程师基础上，有了大量的工程实践，理解当前计算机体系架构背后的数学意义）。

闲话扯多了，回到正轨吧。在zynq系列（我使用zc702开发板，zynq7020的SOC）开发有操作系统的SOC系统，需要vitis工具（包含vivado2019.2、vivado_hls2019.2、vitis2019.2）和petalinux2019.2。

我的工具运行平台vitis安装在windows10上（vitis也可以安装在ubuntu上，我安装成功了，有两个问题，让我放弃了，第一，我的笔记本中ubuntu剩余空间太小了，不足安装；第二，vivado在ubuntu中运行正常，但vitis加载vivado生成的xsa文件失败，所以暂时放弃全linux环境开发zynq，后续争取全linux环境开发zynq）；petalinux安装在ubuntu16.04上（明年2020年准备升级为ubuntu20.04）。

- vitis下载，[下载地址](https://www.xilinx.com/support/download/index.html/content/xilinx/en/downloadNav/vitis.html)：选择Xilinx Vitis 2019.2: All OS installer Single-File Download (TAR/GZIP - 30.76 GB) 文件，尽量不要使用web安装方式，方式网络通信失败导致多次安装。

- petalinux下载，[下载地址](https://www.xilinx.com/support/download/index.html/content/xilinx/en/downloadNav/embedded-design-tools.html)：选择PetaLinux 2019.2 Installer (TAR/GZIP - 7.92 GB) 文件。

vitis安装，基本就是下一步、下一步，里面有个选项，默认为vivado，更换为vitis即可，安装装包选择全部安装（也可根据自己需要进行更改）。安装完毕后，生成xilinx_vitis_ide2019.02、vivado2019.02、vivado_hls2019.02等。

petalinux安装，首先运行：sudo apt-get install -y python gawk gcc git make net-tools libncurses5-dev tftpd zlib1g:i386 libssl-dev flex bison libselinux1 gnupg wget diffstat chrpath socat xterm autoconf libtool tar unzip texinfo zlib1g-dev gcc-multilib build-essential libsdl1.2-dev libglib2.0-dev screen pax gzip chrpath socat texinfo gcc-multilib。安装必要的依赖包，然后运行./petalinux-v2019.2-final-installer.run，安装petalinux，按照提示安装即可，也很简单。

安装工具后，开始玩啥zc702吧。

## 定制自己的zc702包

### vivado定制znyq7020板级描述

Petalinux2019.2需要Vivado2019.2提供的板级表述文件变为xsa文件（之前需要hdf文件）。

打开vivado2019.2工具，选择“open Example Project”->“Next”。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0121/014524_e00268a5_5631341.png "屏幕截图.png")

选择“Base Zynq”->“Next”，建立zynq7020的工程。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0121/014540_c9f5acf1_5631341.png "屏幕截图.png")

自己填写工程名称、工程目录->“Next”，配置工程名称和要保存的目录位置。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0121/014557_ddeac641_5631341.png "屏幕截图.png")

选择zc702开发板->“Next”。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0121/014619_73e0361a_5631341.png "屏幕截图.png")

点击“Finish”完成工程建立。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0121/014632_59d58a48_5631341.png "屏幕截图.png")

新建工程状态，可以配置PS端连接状态。当前就不配置了，直接综合、布局布线生成xsa文件和bit文件。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0121/014653_4b8d06f3_5631341.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2022/0121/014706_6dc1fbb5_5631341.png "屏幕截图.png")

点击“Generate Bitstream”进行综合和布局布线。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0121/014713_817218a3_5631341.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2022/0121/014721_5f9ce9ff_5631341.png "屏幕截图.png")

综合和布局布线完毕后。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0121/014741_59357da1_5631341.png "屏幕截图.png")

导出*.xda板级描述文件和*.bit FPGA文件。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0121/014749_f1e0d447_5631341.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2022/0121/014800_dedda6ca_5631341.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2022/0121/014815_11923b46_5631341.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2022/0121/014821_b883e26c_5631341.png "屏幕截图.png")

生成我们需要的zc702_demo.xda和zc702_demo.bit文件。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0121/014833_0beec232_5631341.png "屏幕截图.png")

## petalinux定制linux发行版

需要下载官方提供的zc702的linux的包，加上用vivado定制的zc702_demo.xda和zc702_demo.bit两个文件即可。

- zc702的BSP包[下载地址](https://www.xilinx.com/support/download/index.html/content/xilinx/en/downloadNav/embedded-design-tools.html)：下载 ZC702 BSP (BSP - 99.45 MB) 文件。

打开ubuntu的终端，收入指令输入指令：source /home/shirf/peta_linux/2019.2/settings.sh，是当前终端使用petalinux的工具。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0121/014929_190e7d90_5631341.png "屏幕截图.png")

使用zc702的BSP包生成linux工程包。输入指令：petalinux-create -t project -s setup_packet/xilinx-zc702-v2019.2-final.bsp。生成xilinx-zc702-2019.2工程目录。
　　
![输入图片说明](https://images.gitee.com/uploads/images/2022/0121/014947_75885a53_5631341.png "屏幕截图.png")

使用zc702_demo.xsa文件配置linux工程。输入指令：petalinux-config --get-hw-description=…/my_zc702_vivado_export。其中…/my_zc702_vivado_export包含zc702_demo.xsa文件。
　　
![输入图片说明](https://images.gitee.com/uploads/images/2022/0121/015002_bbd0fdcb_5631341.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2022/0121/015012_1bd357f2_5631341.png "屏幕截图.png")

选择上图中的exit，不进行修改，使用默认配置。配置工程成功状态如下：
　
![输入图片说明](https://images.gitee.com/uploads/images/2022/0121/015030_0f6b26a4_5631341.png "屏幕截图.png")

进行工程编译。输入指令：petalinux-build。进行整个工程的编译。编译成功后，在"image/linux"目录下，有烧写文件。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0121/015044_4f440885_5631341.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2022/0121/015054_651a48f9_5631341.png "屏幕截图.png")
　　
生成BOOT.BIN文件，使用vivado生成的zc702_demo.bin文件，重命名为system.bit文件覆盖image/linux中system.bit文件。输入指令：etalinux-package --boot --format BIN --fsbl ./images/linux/zynq_fsbl.elf --fpga ./images/linux/system.bit --u-boot。将fsbl文件、bit文件、uboot文件，生成BOOT.BIN文件。至此，针对linux系统的烧写文件BOOT.BIN、image.ub、system.dtb文件都准备好了，存放在image/linux目录下。
　　
![输入图片说明](https://images.gitee.com/uploads/images/2022/0121/015108_1bcfe445_5631341.png "屏幕截图.png")

## zc702启动自定制的linux系统

准备一张sd卡，我的为2G，使用fdisk命令分区，建立两个区，第一个区为200Mbyte、第二个区为剩下1.8Gbyte，第一个区会用fat32文件系统格式化、第二个区使用ext4文件系统格式化。具体方式参考xilinx的[wiki](https://xilinx-wiki.atlassian.net/wiki/spaces/A/pages/18841655/Prepare+Boot+Medium#PrepareBootMedium-SDBoot)（中，SD BOOT部分）。将BOOT.BIN、image.ub、system.dtb放到第一个分区中（一定要为fat32格式，不然zynq内部的启动程序不认识其他分区）。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0121/015206_bb2e6eec_5631341.png "屏幕截图.png")

配置zc702从[开发板启动](https://xilinx-wiki.atlassian.net/wiki/spaces/A/pages/18841655/Prepare+Boot+Medium#PrepareBootMedium-SDBoot)（中，SD BOOT部分），将SD卡插入zc702开发板中，启动电源，此时zc702正常启动linux系统，通过板载的usb转串口，在计算机上用minicom观察系统启动信息为，用户名为root，密码为root即可登录系统。
　　

```
U-Boot 2019.01 (Nov 27 2019 - 06:35:40 +0000) Xilinx Zynq ZC702

CPU: Zynq 7z020
Silicon: v0.0
Model: Zynq ZC702 Development Board
DRAM: ECC disabled 1 GiB
MMC: mmc@e0100000: 0
Loading Environment from SPI Flash… SF: Detected n25q128a with page size 256 B
*** Warning - bad CRC, using default environment

In: serial@e0001000
Out: serial@e0001000
Err: serial@e0001000
Model: Zynq ZC702 Development Board
Net: ZYNQ GEM: e000b000, phyaddr 7, interface rgmii-id
eth0: ethernet@e000b000
U-BOOT for xilinx-zc702-2019_2

ethernet@e000b000 Waiting for PHY auto negotiation to complete… TIMEOUT!
Hit any key to stop autoboot: 0
Device: mmc@e0100000
Manufacturer ID: 3
OEM: 5344
Name: SU02G
Bus Speed: 50000000
Mode : SD High Speed (50MHz)
Rd Block Len: 512
SD version 3.0
High Capacity: No
Capacity: 1.8 GiB
Bus Width: 4-bit
Erase Group Size: 512 Bytes
10953960 bytes read in 633 ms (16.5 MiB/s)
,## Loading kernel from FIT Image at 10000000 …
Using ‘conf@system-top.dtb’ configuration
Verifying Hash Integrity … OK
Trying ‘kernel@1’ kernel subimage
Description: Linux kernel
Type: Kernel Image
Compression: uncompressed
Data Start: 0x10000104
Data Size: 4125936 Bytes = 3.9 MiB
Architecture: ARM
OS: Linux
Load Address: 0x00008000
Entry Point: 0x00008000
Hash algo: sha1
Hash value: 44ec07a1417655d9b39f1f90057d717e378a4fca
Verifying Hash Integrity … sha1+ OK
,## Loading ramdisk from FIT Image at 10000000 …
Using ‘conf@system-top.dtb’ configuration
Verifying Hash Integrity … OK
Trying ‘ramdisk@1’ ramdisk subimage
Description: petalinux-user-image
Type: RAMDisk Image
Compression: gzip compressed
Data Start: 0x103f48d0
Data Size: 6805130 Bytes = 6.5 MiB
Architecture: ARM
OS: Linux
Load Address: unavailable
Entry Point: unavailable
Hash algo: sha1
Hash value: 006df8e566b1077a431ff8ff8f6b23c4ac323dd2
Verifying Hash Integrity … sha1+ OK
,## Loading fdt from FIT Image at 10000000 …
Using ‘conf@system-top.dtb’ configuration
Verifying Hash Integrity … OK
Trying ‘fdt@system-top.dtb’ fdt subimage
Description: Flattened Device Tree blob
Type: Flat Device Tree
Compression: uncompressed
Data Start: 0x103ef6f4
Data Size: 20762 Bytes = 20.3 KiB
Architecture: ARM
Hash algo: sha1
Hash value: 63cdb6f817b5faed1dde292b02a16e0f20cc1634
Verifying Hash Integrity … sha1+ OK
Booting using the fdt blob at 0x103ef6f4
Loading Kernel Image … OK
Loading Ramdisk to 07982000, end 07fff68a … OK
Loading Device Tree to 07979000, end 07981119 … OK

Starting kernel …

Booting Linux on physical CPU 0x0
Linux version 4.19.0-xilinx-v2019.2 (oe-user@oe-host) (gcc version 8.2.0 (GCC))9
CPU: ARMv7 Processor [413fc090] revision 0 (ARMv7), cr=18c5387d
CPU: PIPT / VIPT nonaliasing data cache, VIPT aliasing instruction cache
OF: fdt: Machine model: Zynq ZC702 Development Board
earlycon: cdns0 at MMIO 0xe0001000 (options ‘115200n8’)
bootconsole [cdns0] enabled
Memory policy: Data cache writealloc
cma: Reserved 16 MiB at 0x3f000000
random: get_random_bytes called from start_kernel+0x80/0x3c4 with crng_init=0
percpu: Embedded 16 pages/cpu @(ptrval) s35916 r8192 d21428 u65536
Built 1 zonelists, mobility grouping on. Total pages: 260608
Kernel command line: console=ttyPS0,115200 earlycon
Dentry cache hash table entries: 131072 (order: 7, 524288 bytes)
Inode-cache hash table entries: 65536 (order: 6, 262144 bytes)
Memory: 1006284K/1048576K available (6144K kernel code, 204K rwdata, 1604K roda)
Virtual kernel memory layout:
vector : 0xffff0000 - 0xffff1000 ( 4 kB)
fixmap : 0xffc00000 - 0xfff00000 (3072 kB)
vmalloc : 0xf0800000 - 0xff800000 ( 240 MB)
lowmem : 0xc0000000 - 0xf0000000 ( 768 MB)
pkmap : 0xbfe00000 - 0xc0000000 ( 2 MB)
modules : 0xbf000000 - 0xbfe00000 ( 14 MB)
.text : 0x(ptrval) - 0x(ptrval) (7136 kB)
.init : 0x(ptrval) - 0x(ptrval) (1024 kB)
.data : 0x(ptrval) - 0x(ptrval) ( 205 kB)
.bss : 0x(ptrval) - 0x(ptrval) ( 133 kB)
rcu: Preemptible hierarchical RCU implementation.
rcu: RCU restricting CPUs from NR_CPUS=4 to nr_cpu_ids=2.
Tasks RCU enabled.
rcu: Adjusting geometry for rcu_fanout_leaf=16, nr_cpu_ids=2
NR_IRQS: 16, nr_irqs: 16, preallocated irqs: 16
efuse mapped to (ptrval)
slcr mapped to (ptrval)
L2C: platform modifies aux control register: 0x72360000 -> 0x72760000
L2C: DT/platform modifies aux control register: 0x72360000 -> 0x72760000
L2C-310 erratum 769419 enabled
L2C-310 enabling early BRESP for Cortex-A9
L2C-310 full line of zeros enabled for Cortex-A9
L2C-310 ID prefetch enabled, offset 1 lines
L2C-310 dynamic clock gating enabled, standby mode enabled
L2C-310 cache controller enabled, 8 ways, 512 kB
L2C-310: CACHE_ID 0x410000c8, AUX_CTRL 0x76760001
zynq_clock_init: clkc starts at (ptrval)
Zynq clock init
sched_clock: 64 bits at 333MHz, resolution 3ns, wraps every 4398046511103ns
clocksource: arm_global_timer: mask: 0xffffffffffffffff max_cycles: 0x4ce07af02s
Switching to timer-based delay loop, resolution 3ns
clocksource: ttc_clocksource: mask: 0xffff max_cycles: 0xffff, max_idle_ns: 537s
timer #0 at (ptrval), irq=17
Console: colour dummy device 80x30
Calibrating delay loop (skipped), value calculated using timer frequency… 666.)
pid_max: default: 32768 minimum: 301
Mount-cache hash table entries: 2048 (order: 1, 8192 bytes)
Mountpoint-cache hash table entries: 2048 (order: 1, 8192 bytes)
CPU: Testing write buffer coherency: ok
CPU0: Spectre v2: using BPIALL workaround
CPU0: thread -1, cpu 0, socket 0, mpidr 80000000
Setting up static identity map for 0x100000 - 0x100060
rcu: Hierarchical SRCU implementation.
smp: Bringing up secondary CPUs …
CPU1: thread -1, cpu 1, socket 0, mpidr 80000001
CPU1: Spectre v2: using BPIALL workaround
smp: Brought up 1 node, 2 CPUs
SMP: Total of 2 processors activated (1333.33 BogoMIPS).
CPU: All CPU(s) started in SVC mode.
devtmpfs: initialized
VFP support v0.3: implementor 41 architecture 3 part 30 variant 9 rev 4
clocksource: jiffies: mask: 0xffffffff max_cycles: 0xffffffff, max_idle_ns: 191s
futex hash table entries: 512 (order: 3, 32768 bytes)
pinctrl core: initialized pinctrl subsystem
NET: Registered protocol family 16
DMA: preallocated 256 KiB pool for atomic coherent allocations
cpuidle: using governor menu
hw-breakpoint: found 5 (+1 reserved) breakpoint and 1 watchpoint registers.
hw-breakpoint: maximum watchpoint size is 4 bytes.
zynq-ocm f800c000.ocmc: ZYNQ OCM pool: 256 KiB @ 0x(ptrval)
zynq-pinctrl 700.pinctrl: zynq pinctrl initialized
e0001000.serial: ttyPS0 at MMIO 0xe0001000 (irq = 27, base_baud = 3125000) is as
console [ttyPS0] enabled
console [ttyPS0] enabled
bootconsole [cdns0] disabled
bootconsole [cdns0] disabled
GPIO IRQ not connected
XGpio: gpio@41200000: registered, base is 1020
vgaarb: loaded
SCSI subsystem initialized
usbcore: registered new interface driver usbfs
usbcore: registered new interface driver hub
usbcore: registered new device driver usb
media: Linux media interface: v0.10
videodev: Linux video capture interface: v2.00
pps_core: LinuxPPS API ver. 1 registered
pps_core: Software ver. 5.3.6 - Copyright 2005-2007 Rodolfo Giometti <giometti@>
PTP clock support registered
EDAC MC: Ver: 3.0.0
FPGA manager framework
Advanced Linux Sound Architecture Driver Initialized.
clocksource: Switched to clocksource arm_global_timer
NET: Registered protocol family 2
tcp_listen_portaddr_hash hash table entries: 512 (order: 0, 6144 bytes)
TCP established hash table entries: 8192 (order: 3, 32768 bytes)
TCP bind hash table entries: 8192 (order: 4, 65536 bytes)
TCP: Hash tables configured (established 8192 bind 8192)
UDP hash table entries: 512 (order: 2, 16384 bytes)
UDP-Lite hash table entries: 512 (order: 2, 16384 bytes)
NET: Registered protocol family 1
RPC: Registered named UNIX socket transport module.
RPC: Registered udp transport module.
RPC: Registered tcp transport module.
RPC: Registered tcp NFSv4.1 backchannel transport module.
Trying to unpack rootfs image as initramfs…
Freeing initrd memory: 6648K
hw perfevents: no interrupt-affinity property for /pmu@f8891000, guessing.
hw perfevents: enabled with armv7_cortex_a9 PMU driver, 7 counters available
workingset: timestamp_bits=30 max_order=18 bucket_order=0
jffs2: version 2.2. (NAND) (SUMMARY) © 2001-2006 Red Hat, Inc.
bounce: pool size: 64 pages
io scheduler noop registered
io scheduler deadline registered
io scheduler cfq registered (default)
io scheduler mq-deadline registered
io scheduler kyber registered
dma-pl330 f8003000.dmac: Loaded driver for PL330 DMAC-241330
dma-pl330 f8003000.dmac: DBUFF-128x8bytes Num_Chans-8 Num_Peri-4 Num_Eve6
brd: module loaded
loop: module loaded
m25p80 spi0.0: found n25q128a11, expected n25q512a
m25p80 spi0.0: n25q128a11 (16384 Kbytes)
3 fixed-partitions partitions found on MTD device spi0.0
Creating 3 MTD partitions on “spi0.0”:
0x000000000000-0x000000500000 : “boot”
0x000000500000-0x000000520000 : “bootenv”
0x000000520000-0x000000fa0000 : “kernel”
libphy: Fixed MDIO Bus: probed
CAN device driver interface
libphy: MACB_mii_bus: probed
Marvell 88E1116R e000b000.ethernet-ffffffff:07: attached PHY driver [Marvell 88)
macb e000b000.ethernet eth0: Cadence GEM rev 0x00020118 at 0xe000b000 irq 29 (0)
e1000e: Intel® PRO/1000 Network Driver - 3.2.6-k
e1000e: Copyright© 1999 - 2015 Intel Corporation.
ehci_hcd: USB 2.0 ‘Enhanced’ Host Controller (EHCI) Driver
ehci-pci: EHCI PCI platform driver
usbcore: registered new interface driver usb-storage
chipidea-usb2 e0002000.usb: e0002000.usb supply vbus not found, using dummy regr
chipidea-usb2 e0002000.usb: Linked as a consumer to regulator.0
ULPI transceiver vendor/product ID 0x0424/0x0007
Found SMSC USB3320 ULPI transceiver.
ULPI integrity check: passed.
ci_hdrc ci_hdrc.0: EHCI Host Controller
ci_hdrc ci_hdrc.0: new USB bus registered, assigned bus number 1
ci_hdrc ci_hdrc.0: USB 2.0 started, EHCI 1.00
hub 1-0:1.0: USB hub found
hub 1-0:1.0: 1 port detected
i2c /dev entries driver
cdns-i2c e0004000.i2c: 400 kHz mmio e0004000 irq 24
si570 1-005d: registered, current frequency 148500000 Hz
i2c i2c-0: Added multiplexed i2c bus 1
i2c i2c-0: Added multiplexed i2c bus 2
at24 3-0054: 1024 byte 24c08 EEPROM, writable, 1 bytes/write
i2c i2c-0: Added multiplexed i2c bus 3
i2c i2c-0: Added multiplexed i2c bus 4
rtc-pcf8563 5-0051: rtc core: registered rtc-pcf8563 as rtc0
i2c i2c-0: Added multiplexed i2c bus 5
i2c i2c-0: Added multiplexed i2c bus 6
i2c i2c-0: Added multiplexed i2c bus 7
i2c i2c-0: Added multiplexed i2c bus 8
pca954x 0-0074: registered 8 multiplexed busses for I2C switch pca9548
ucd9200 8-0034: Device ID UCD9248-80|5.8.0.11400|091112
ucd9200 8-0034: 4 rails configured
random: fast init done
ucd9200 8-0035: Device ID UCD9248-80|5.8.0.11400|091112
ucd9200 8-0035: 4 rails configured
ucd9200 8-0036: Device ID UCD9248-80|5.8.0.11400|091112
ucd9200 8-0036: 2 rails configured
cdns-wdt f8005000.watchdog: Xilinx Watchdog Timer with timeout 10s
EDAC MC: ECC not enabled
Xilinx Zynq CpuIdle Driver started
sdhci: Secure Digital Host Controller Interface driver
sdhci: Copyright© Pierre Ossman
sdhci-pltfm: SDHCI platform and OF driver helper
mmc0: Invalid maximum block size, assuming 512 bytes
mmc0: SDHCI controller on e0100000.mmc [e0100000.mmc] using ADMA
ledtrig-cpu: registered to indicate activity on CPUs
usbcore: registered new interface driver usbhid
usbhid: USB HID core driver
fpga_manager fpga0: Xilinx Zynq FPGA Manager registered
NET: Registered protocol family 10
Segment Routing with IPv6
sit: IPv6, IPv4 and MPLS over IPv4 tunneling driver
NET: Registered protocol family 17
can: controller area network core (rev 20170425 abi 9)
NET: Registered protocol family 29
can: raw protocol (rev 20170425)
can: broadcast manager protocol (rev 20170425 t)
can: netlink gateway (rev 20170425) max_hops=1
Registering SWP/SWPB emulation handler
of-fpga-region fpga-full: FPGA Region probed
input: gpio-keys as /devices/soc0/gpio-keys/input/input0
rtc-pcf8563 5-0051: setting system clock to 2019-11-27 06:53:12 UTC (1574837592)
of_cfs_init
of_cfs_init: OK
ALSA device list:
No soundcards found.
Freeing unused kernel memory: 1024K
Run /init as init process
INIT: mmc0: new high speed SD card at address e624
mmcblk0: mmc0:e624 SU02G 1.84 GiB
version 2.88 booting mmcblk0: p1 p2

Starting udev
udevd[817]: starting version 3.2.5
random: udevd: uninitialized urandom read (16 bytes read)
random: udevd: uninitialized urandom read (16 bytes read)
random: udevd: uninitialized urandom read (16 bytes read)
udevd[818]: starting eudev-3.2.5
EXT4-fs (mmcblk0p2): mounted filesystem with ordered data mode. Opts: (null)
FAT-fs (mmcblk0p1): Volume was not properly unmounted. Some data may be corrupt.
urandom_read: 4 callbacks suppressed
random: dd: uninitialized urandom read (512 bytes read)
INIT: Entering runlevel: 5
Configuring network interfaces… IPv6: ADDRCONF(NETDEV_UP): eth0: link is not y
udhcpc: started, v1.29.2
udhcpc: sending discover
udhcpc: sending discover
udhcpc: sending discover
udhcpc: no lease, forking to background
done.
Starting haveged: haveged: listening socket at 3
haveged: haveged starting up

Starting Dropbear SSH server: random: dropbearkey: uninitialized urandom read ()
random: dropbearkey: uninitialized urandom read (32 bytes read)
Generating 2048 bit rsa key, this may take a while…
haveged: haveged: ver: 1.9.4; arch: generic; vend: ; build: (gcc 8.2.0 CTV); coK

haveged: haveged: cpu: (VC); data: 16K (D); inst: 16K (D); idx: 12/40; sz: 15018

haveged: haveged: tot tests(BA8): A:1/1 B:1/1 continuous tests(B): last entrop4

haveged: haveged: fills: 0, generated: 0

random: crng init done
Public key portion is:
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC00SMqxrPbN8DRIpBygHQKAF64JT30i+CJprjzaug2
Fingerprint: sha1!! 56:ba:78:8d:a9:94:26:47:6d:06:c4:25:92🆎33:4b:b4:3e:50:13
dropbear.
Starting internet superserver: inetd.
Starting syslogd/klogd: done
Starting tcf-agent: OK

PetaLinux 2019.2 xilinx-zc702-2019_2 /dev/ttyPS0

xilinx-zc702-2019_2 login: root
Password:
root@xilinx-zc702-2019_2:~# cd /
root@xilinx-zc702-2019_2:/# ls
bin dev home lib mnt root sbin tmp var
boot etc init media proc run sys usr
root@xilinx-zc702-2019_2:/#

```

## 小结和后续工作

1. 通过上述步骤，完成zynq7020整套开发环境搭建的验证。后续工作在此基础上继续进行。
2. 接下的的工作分享。
   - （1）当前系统无法上电自启动自定义的脚本，应为即使配置了/etc中的启动选项，由于是ramdisk的文件系统，重新上电后，之前的配合都还原了。我们要保留ramdisk文件系统，这样安全可靠啊，同时启动自定义的脚本，这就需要我们在以后rootfs根文件系统目录的基础上，编译/etc中相应脚本，然后将rootfs制作成.cpio格式，然后编译内核时指定ramdisk为我们制作的.cpio文件系统。
   - （2）利用petalinux工具，添加像python等第三方工具，丰富我们的发行版。
   - （3）使用vitis调试linux的应用程序hello_world。尝鲜vitis这个集成开发环境。
   - （4）vivado配置我们的ps端，使能更多内部硬核。
   - （5）使用vivado_hls工具，自定制软IP核，挂接到PS端，编写驱动，来一场正经的zynq开发。（我是嵌入式工程师，对vhdl不熟，那我就用xilinx为嵌入式工程师准备的hls工具，使用C/C++语言开发FPGA吧）

# [一位嵌入式老鸟编撰的树莓派教程](https://gitee.com/link?target=https%3A%2F%2Fshumeipai.nxez.com%2F2019%2F11%2F06%2Feasy-raspi-python-one.html)

![输入图片说明](https://images.gitee.com/uploads/images/2022/0121/024038_bfa1ead1_5631341.png "屏幕截图.png")

「动手学树莓派 Python 篇」是由网友“大飞品树莓”撰写的课程，作者在开篇提到了他撰写这个课程的初衷：

> 作为一名嵌入式世界里的老鸟，在8年的工作实践中，深深地感受到学校扎实的理论教育和企业极强的实践人才诉求之间，还需一个中间环节充当衔接，让学生们在学校期间，可以将理论知识付诸实践，进一步夯实了对计算机系统的整体理解，同时提高了自己的专业技能，满足企业要求，应为未来之变。

> “大飞品树莓”系列课程，将自己多年来计算机系统的理解和工程实践经验，进行整理和分享，希望尽自己绵薄之力，为“新工科”建设添砖加瓦，希望我的“动手学”系列，可以成为您课外实践课之一（我希望尽我所能，成为您最喜欢的课外实践课）。


目前的「动手学树莓派 Python上篇」使用树莓派配合 NXEZ 树莓派瑞士军D扩展板卡，进行动手学树莓派。教程和教程示例源码均托管在 github 上，同时正在不断完善和更新，欢迎各位一线教师在自己的授课中参考。


地址：https://github.com/srf1986/easy_raspi_python_one.git

### 目录

- [动手学树莓派第1章：先唠叨两句](https://shumeipai.nxez.com/easy-raspi-python-one-chapter-1)
- [动手学树莓派第2章：近距离看看计算机硬件体系结构](https://shumeipai.nxez.com/easy-raspi-python-one-chapter-2)
- [动手学树莓派第3章：操作系统为何物？为什么要花这么大精力来使用他？](https://shumeipai.nxez.com/easy-raspi-python-one-chapter-3)
- [动手学树莓派第4章：多任务，原来如此简单](https://shumeipai.nxez.com/easy-raspi-python-one-chapter-4)
- [动手学树莓派第5章：不能再两耳不闻窗外事了！你们需要交流](https://shumeipai.nxez.com/easy-raspi-python-one-chapter-5)
- [动手学树莓派第6章：停下来，别争了，咱们定个规矩](https://shumeipai.nxez.com/easy-raspi-python-one-chapter-6)
- [动手学树莓派第7章：LED灯原来是这样点亮的](https://shumeipai.nxez.com/easy-raspi-python-one-chapter-7)
- [动手学树莓派第8章：你牙牙学语的声音真好听](https://shumeipai.nxez.com/easy-raspi-python-one-chapter-8)
- [动手学树莓派第9章：小小按键在我手，我的触碰你知道](https://shumeipai.nxez.com/easy-raspi-python-one-chapter-9)
- [动手学树莓派第10章：有了数码管，数字、字母任我写](https://shumeipai.nxez.com/easy-raspi-python-one-chapter-10)
- [动手学树莓派第11章：有了他，再不用到处找温度计了](https://shumeipai.nxez.com/easy-raspi-python-one-chapter-11)
- [动手学树莓派第12章：调试器在手，想看什么看什么](https://shumeipai.nxez.com/easy-raspi-python-one-chapter-12)
- [动手学树莓派第13章：不论你身在何处，咱们都能聊天](https://shumeipai.nxez.com/easy-raspi-python-one-chapter-13)
- [动手学树莓派：结束了吗？没有啊，下篇更精彩：）](https://shumeipai.nxez.com/easy-raspi-python-one-chapter-16)

课程 gitee 地址：https://gitee.com/shirf_taste_raspi/shirf_serial_share
课程 bilibili 视频地址：https://www.bilibili.com/video/av71878718


### [大飞品树莓系列课程汇总](https://gitee.com/shirf_taste_raspi/shirf_serial_share/) （[全文](https://gitee.com/shiliupi/easy_raspi_python_one/issues/I4RCXS#note_9513827_link)）

#### 介绍

本套教程有相应的视频教程，已提交B站上了(bibi)，随时可以观看。

#### 课程介绍
国家针对新时代的特点，提出了“新工科”的要求，“新工科”要求我们将“动手学”付诸于日常教与学中。

作为一名嵌入式世界里的老鸟，在8年的工作实践中，深深地感受到学校扎实的理论教育和企业极强的实践人才诉求之间，还需一个中间环节充当衔接，让学生们在学校期间，可以将理论知识付诸实践，进一步夯实了对计算机系统的整体理解，同时提高了自己的专业技能，满足企业要求，应为未来之变。

“大飞品树莓”系列课程，将自己多年来计算机系统的理解和工程实践经验，进行整理和分享，希望尽自己绵薄之力，为“新工科”建设添砖加瓦，希望我的“动手学”系列，可以成为您课外实践课之一（我希望尽我所能，成为您最喜欢的课外实践课）。



当前课程资源包含如下：

1、视频教程，已上传B站

《树莓派系统安装说明》：https://www.bilibili.com/video/av71950296

《动手学树莓派——python上篇》：https://www.bilibili.com/video/av71878718

2、教程配套树莓派操作系统镜像

本套课程配套使用系统镜像，是以raspbian为基线，添加了教程需要的软件和课件。

系统镜像下载地址：https://pan.baidu.com/s/1dT-LNp6Sa3IjkhbBxVl52g

3、树莓派系统安装篇：

以Raspbian系统为基线，按照本套教程使用的工具进行相应软件的安装，您只要下载我的系统镜像，按照《树莓派系统安装说明》文档和bibi上配套视频教程，即可简单安装和测试。

《树莓派系统安装说明》
gitee托管地址： https://gitee.com/shirf_taste_raspi/raspi_os_setup.git

github托管地址：https://github.com/srf1986/raspi_os_transform.git


4、动手学树莓派——python上篇

当前使用树莓派3B+配合NXEZ树莓派瑞士军扩展板刀卡，进行动手学树莓派。

gitee托管地址：https://gitee.com/shirf_taste_raspi/easy_raspi_python_one.git

github托管地址：https://github.com/srf1986/easy_raspi_python_one.git



5、中学生都能玩的人工智能

当前基于树莓派4B+百度AI智能云，制作了一套《中学生都能玩的人工智能》课程。

本课程用于展示人工智能的应用效果，因为利用百度AI智能云，所有省去了模型建立、参数调优的过程。

本课程可以体验：语音合成、语音识别、人脸识别、图像识别、车牌识别、动物识别等。

课程配套资源：

学习视频：https://space.bilibili.com/403216235/channel/detail?cid=134742

gitee托管地址：https://gitee.com/shiranfei/open_course_for_AI.git

github托管地址：https://gitee.com/shiranfei/open_course_for_AI.git

#### 大飞品树莓攀登AIOT之旅计划

全部旅程我准备按照如下路线，带领各位攀登这座高山、欣赏途中的美景。


(1)用python开始我们的旅程，使用python简单、快速、形象的理解操作系统为我们提供了便利，如何使用多任务、如何指挥他们协同工作；同时借用树莓派和NXEZ扩展模块，通过led灯、温度传感器、数码管等形象的展示出来；然后，咱们利用RPi模块，初探python库是如何使用操作系统提供的API(操作系统以c语言方式提供API接口)；最后，简单介绍深度学习包含哪些方面的工作，并在树莓派上使用tensorflowlite使用深度学习来进行图像分裂。在获取树莓派4B和Movidius2神经网络棒后，完成具有实用价值的给予树莓派+Movidius2嵌入式深度学习目标识别系统，使用的网络模型为yolo（后面准备使用fpga来实现yolo网络）。

(2)用C语言继续向上攀登，掌握C语言的基本语法、同时我会将C语言的武器核——指针传授给大家，指针是向上继续攀登探索内核必备武器核；然后，按照与python之旅相同的思路，使用C语言理解操作系统提供的工具，并深入解析python库使用操作系统提供的API；最后，咱们分析下yolo作者分享的yolo开源的c语言版网络分析计算过程，逐层讲解模型和参数，为后面使用HLS在ZYNQ上用硬件实现做准备。


(3)有了python和c语言从应用层看操作系统的实践经验，咱们基本明白了操作系统给我们提供哪些服务，下面让我们继续攀登，探索linux内核。这部分会一起欣赏内核中内核驱动和系统调用接口两部分，看看内核如果操作led灯、温度传感器、数码管等外设，如何给应用程序提供系统调用接口，系统调用接口与API什么关系，我们该如果定制自己的内核驱动；同时，咱们看看内核如何使用c语言实现面向对象的思想。

(4)欣赏完内核驱动后，咱们继续前行，进入到内核核心区域，这里有进程管理、内存管理、虚拟文件系统、网络协议栈四个著名风景区，我会主要讲解进程管理、网络协议栈两部分，概要讲解内存管理、虚拟文件系统；这里我们换个角度，从算法角度，看看散列表、二叉树、B+树等如何在内核中应用，激发咱们对算法的兴趣。其中，我会带大家完整的从应用层api到tcp/ip协议栈再到网卡驱动近距离的走一遍，看看无处不在的网络是什么样子的。

(5)参观完cpu端的部分美景后，估计你具备了自行浏览cpu端其他景点的能力，这里你需要博览群书，整理出自己的cpu端体系架构；我的感受就是，看到一片处理器，我看到的是第一层cpu通过片上总线协调各外设工作；第二层内核定时进行进程管理、内存回收等工作，同时将外设中的数据放到内存中，唤醒相应进程工作；第三层，进程按照用户的意愿进行工作，如果需要协调，则想内核发送报告，内核负责对通信和同步。

(6)自行参观完cpu端景点后，咱们继续前行，AIOT这座高峰就在我们前面。嵌入式出身的探索者，可以按照先PS端、然后是深度学习网络模型学习、最后是PL端学习。使用的配套硬件，准备使用sipeed公司的“荔枝糖hex zynq7020 fpga开发板”。对于PS端开发，按照树莓派的思路进行从python开始、经过c语言、内核驱动，最后是内核核心区域。

(7)我们继续攀登，前面是一个完全新的领域，深度学习领域；在当前的深度学习包含三种工作：①大牛研究深度网络模型改进模型，②我们理解大牛的网络模型，利用其模型的特征提取能力，在网络输出端更改为自己的分类，然后用自己的数据集进行训练自行添加层的权重，③将已经训练好的模型，借助HLS使用FPGA实现，实现高能效计算。对于第一点，作为芸芸众生的我们恐怕功力不够，容易走火入魔；但我们可以做第二、三点工作，让深度学习移植到嵌入式端，在嵌入式端工作，让嵌入式更智能。

(8)领略的深度学习的风采后，我们要借助HLS在FPGA上实现我们的网络模型，实现一个低功耗、高性能的边缘端智能处理平台，初步实现AIOT。



---

【笔记】[这碗鸡汤我干啦！大飞随意！](https://gitee.com/shiliupi/easy_raspi_python_one/issues/I4RCXS) :pray: 

> [听了](https://www.bilibili.com/video/BV1pE41167LU?p=31) @shiranfei 的 《[赠言——我都可以，您一定比我强多了](../tree/master/赠言——我都可以，您一定比我强多了)》决定把大飞品树莓推荐给更多同学，先干为敬 :pray: 在你的推荐中提到了 树莓派实验室，我也是王工的粉丝，也专门写过推荐文章，也非常感谢他的帮助。有了现在的成果，基线 raspbian 的 [石榴派](https://gitee.com/shiliupi/shiliupi99) 说来也巧，我是会回访每一位访问我空间的朋友的，[今天同过一位](https://gitee.com/whwtf/easy_raspi_python_one)，找到了[大飞](https://gitee.com/shirf_taste_raspi/)，并听了你的 [B站视频](https://www.bilibili.com/video/BV1pE41167LU)，很高兴能在 2022 新年前给你发这封邀请信。能够以开源前辈的身份，为同学们答疑解惑。
> - 非常不错，截屏到了给你留言的弹幕 :dancer: 
> <p><img height="99px" src="https://images.gitee.com/uploads/images/2022/0119/003155_9ac50bd5_5631341.jpeg" title="大飞书架.jpeg"> <img height="99px" src="https://images.gitee.com/uploads/images/2022/0119/004323_413185e2_5631341.png" title="2022-01-19-004247_911x554_scrot.png"></p>

- 品鉴师简介 | 来吧，干了它吧！| 漏下“富” [(45+38+50) 本藏书](https://gitee.com/shiliupi/easy_raspi_python_one/issues/I4RCXS#note_8367431_link) 等您校对 :pray:

- [渡人渡己](https://gitee.com/shiliupi/easy_raspi_python_one/issues/I4RCXS#note_8384825_link) [海报](https://images.gitee.com/uploads/images/2022/0119/191355_62b6f019_5631341.png)

  - [“渡人如渡己，渡己亦渡人”是什么意思？](https://gitee.com/shiliupi/easy_raspi_python_one/issues/I4RCXS#note_8385331_link)
  - [渡人渡己，还是度人度己，到底是用“度”还是“渡”啊？ 请教哈！！！](https://gitee.com/shiliupi/easy_raspi_python_one/issues/I4RCXS#note_8385583_link)
  - [数日研究，我对自渡与渡人，有些许开悟。](https://zhuanlan.zhihu.com/p/85841166) 【[笔记](https://gitee.com/shiliupi/easy_raspi_python_one/issues/I4RCXS#note_8385692_link)】
  - [《西游记》中，千辛万苦取回来的大乘佛法是个啥？](https://zhuanlan.zhihu.com/p/146180716) 【[笔记](https://gitee.com/shiliupi/easy_raspi_python_one/issues/I4RCXS#note_8385746_link)】
  
  <p><img height="99px" src="https://images.gitee.com/uploads/images/2022/0119/194614_4f5ff389_5631341.jpeg"> <img src="https://images.gitee.com/uploads/images/2022/0119/202048_83f4c408_5631341.png" height="99px"> <img src="https://images.gitee.com/uploads/images/2022/0119/202049_2e95c188_5631341.png" height="99px"> <img src="https://images.gitee.com/uploads/images/2022/0119/202111_fa9c60de_5631341.png" height="99px"></p>
  
  《西游记》共百回，唐僧师徒五人历经九九八十一难，艰难的来到大雷音寺求取高深佛法——大乘佛法。虽然这是个打妖怪、反抗为主题的神魔志怪小说，但我们不能忘记的是它的故事蓝本是“玄奘取经”，目的就是为了用大乘佛法普度众生，拯救世人。所以，大乘佛法到底是个什么东西？为何需要如此历经千辛万苦将其带回呢？

  1. 从传入到贞观之治
  1. 三武一宗灭佛之厄
  1. 唐太宗时期的发展
  1. 大乘佛法与小乘佛法之争

  - **结语** ：整体看来，大乘佛法似乎更符合我国社会发展需要，更加贴合我国国情。当然小乘佛法也不见得不占优势，只是在大世环境下，群众会比个人更有说服力。

    > 历史发展一波三折，但幸好仍是成功传播到现代。我们无法评判究竟是对是错，但它确确实实伴随着中国社会发展一同成长、变迁。正是这种存在，给予饱受战乱侵扰的先民们以心灵的慰藉。信仰如温泉，如暖流，滋润心灵，浇灌灵魂。

- [中学生都能玩的人工智能](https://gitee.com/shiranfei/open_course_for_AI/) | 介绍 | 运行平台 | 课程资源 | 目录 0-12 |  【[笔记](https://gitee.com/shiliupi/easy_raspi_python_one/issues/I4RCXS#note_8384909_link)】

- [使用xilinx最新工具vitis2019.2，定制zc702开发板linux系统](https://blog.csdn.net/srf1986/article/details/103263272) 【[全文](https://gitee.com/shiliupi/easy_raspi_python_one/issues/I4RCXS#note_8408557_link)】

  2019年10月9日，xilinx推出新一代FPGA集成开发工具——vitis，喜欢尝鲜的我，安装了vitis来进行FPGA开发。

  我是嵌入式工程师出身，发现我的当前阶段的职业规划与老石《[FPGA工程师最核心的就是全栈能力](http://www.sohu.com/a/295982108_453160)》基本一致，不论嵌入式工程师还是FPGA工程师，最后都要晋级为软硬件系统皆精通的全栈工程师（天下武功，殊途同归）。

  全栈工程师就到头了吗？不，学海无涯啊。 **全栈工程师** 向上，还有 **算法全栈工程师** （自己发明的名词，就是理解用户需求，将算力合理的分配套PS端和PL端，在全栈工程师基础上，具有算法硬件化的能力）， **数学工程师** （在算法全栈工程师基础上，有了大量的工程实践，理解当前计算机体系架构背后的数学意义）。

  - 大飞品树莓 CSDN 码龄12年
  
    - 个人简介：耕耘嵌入式１０年的老鸟，跟进时代发展做新时代的“全栈工程师”（利用zynq系列soc,进行IP核配置、操作系统的开发、算法的硬核实现）
    - 加入CSDN时间：2009-09-12
    - 博客简介：srf1986的专栏

  - TA的专栏

    - 玩转pygame-zero
    - 品鉴树莓派 3篇
    - 品鉴zynq系列SOC 1篇
    - 计算机网络 5篇
    - linux 23篇

  - 兴趣领域

    - 人工智能

- [一位嵌入式老鸟编撰的树莓派教程](https://shumeipai.nxez.com/2019/11/06/easy-raspi-python-one.html) 【[全文](https://gitee.com/shiliupi/easy_raspi_python_one/issues/I4RCXS#note_8408606_link)】

  <p><img height="99px" src="https://images.gitee.com/uploads/images/2022/0121/024038_bfa1ead1_5631341.png" title="2019年11月6日Spoony"></p>
  
  「动手学树莓派 Python 篇」是由网友“大飞品树莓”撰写的课程，作者在开篇提到了他撰写这个课程的初衷：

  > 作为一名嵌入式世界里的老鸟，在8年的工作实践中，深深地感受到学校扎实的理论教育和企业极强的实践人才诉求之间，还需一个中间环节充当衔接，让学生们在学校期间，可以将理论知识付诸实践，进一步夯实了对计算机系统的整体理解，同时提高了自己的专业技能，满足企业要求，应为未来之变。

  > “大飞品树莓”系列课程，将自己多年来计算机系统的理解和工程实践经验，进行整理和分享，希望尽自己绵薄之力，为“新工科”建设添砖加瓦，希望我的“动手学”系列，可以成为您课外实践课之一（我希望尽我所能，成为您最喜欢的课外实践课）。

- [该补课了，踏着大飞老师的脚步，补课FPGA.](https://gitee.com/shiliupi/easy_raspi_python_one/issues/I4RCXS#note_9444589_link)

  <p><img height="99px" src="https://images.gitee.com/uploads/images/2022/0326/093136_d262e777_5631341.png" title="1. open a example project as new with same name zc702_demo."> <img height="99px" src="https://images.gitee.com/uploads/images/2022/0326/093315_ab0fc83b_5631341.png" title="2. 点击“Generate Bitstream”进行综合和布局布线。"> <img height="99px" src="https://images.gitee.com/uploads/images/2022/0326/093752_b742a0b7_5631341.png" title="3. lert when press generating bitstream"> <img height="99px" src="https://images.gitee.com/uploads/images/2022/0326/094106_46842a1f_5631341.png" title="5. 操作了一些其他按钮，突然提示了 BITSTREAM OK."> <img height="99px" src="https://images.gitee.com/uploads/images/2022/0326/093659_fad66e39_5631341.png" title="6.综合和布局布线完毕后。"> <img height="99px" src="https://images.gitee.com/uploads/images/2022/0326/094334_849a3543_5631341.png" title="7. 导出*.xda板级描述文件和*.bit FPGA文件。"> <img height="99px" src="https://images.gitee.com/uploads/images/2022/0121/014800_dedda6ca_5631341.png" title="10. xda 导出错误，"> <img height="99px" src="https://images.gitee.com/uploads/images/2022/0121/014833_0beec232_5631341.png" title="10. xda 导出错误，"></p>
  
  10. xda 导出错误，

- Nutshell [Run on FPGA](https://gitee.com/RV4Kids/NutShell/blob/master/README.md) 【[笔记](https://gitee.com/shiliupi/easy_raspi_python_one/issues/I4RCXS#note_9446187_link)】

  - make PRJ=myproject BOARD=pynq STANDALONE=true
  - cat include Makefile.check
  - [Ubuntu上使用Mill的Chisel工程入门](https://blog.csdn.net/weixin_44636505/article/details/113763301) 【[全文](https://gitee.com/shiliupi/easy_raspi_python_one/issues/I4RCXS#note_9447609_link)】
    - 开始
      - NutShell项目介绍
      - 删除原来的代码
      - 书写我们的代码
      - 书写执行翻译的Scala代码
      - 运行整个工程
        - 安装Mill
        - 使用Mill运行整个工程
      - 为啥要在项目里弄(现在这个开源项目已经写好了build.sc文件，也就是我们不需要自己重新学习和书写chisel的依赖项辣, 它不香嘛？？？？)
  - cat /usr/local/bin/mill
  - sudo make clean | make | ls build |
    > Top.anno.json Top.fir TopMain.v TopMain.v.conf
  - [vavido success.](https://gitee.com/shiliupi/easy_raspi_python_one/issues/I4RCXS#note_9461770_link)
    https://images.gitee.com/uploads/images/2022/0326/233440_46ab6ebf_5631341.png
  - [D:\XilinX\Vivado\2019.1\bin\vivado.bat -nolog -nojournal -notrace -mode batch -source board/pynq/mk.tcl -tclargs myproject-pynq true](https://gitee.com/shiliupi/easy_raspi_python_one/issues/I4RCXS#note_9461851_link)
    > ERROR: [Board 49-71] The board_part definition was not found for tul.com.tw:pynq-z2:part0:1.0. The project's board_part property was not set, but the project's part property was set to xc7z020clg400-1. Valid board_part values can be retrieved with the 'get_board_parts' Tcl command. Check if board.repoPaths parameter is set and the board_part is installed from the tcl app store.  
  
- Nutshell [sudo make](https://gitee.com/shiliupi/easy_raspi_python_one/issues/I4RCXS#note_9450342_link)

  '====== Settings = (sim, inorder) ====== >> build/TopMain.v' 
  
  - [make clean before](https://gitee.com/shiliupi/easy_raspi_python_one/issues/I4RCXS#note_9450346_link)
  - [sudo make PRJ=myproject BOARD=pynq STANDALONE=true](https://gitee.com/shiliupi/easy_raspi_python_one/issues/I4RCXS#note_9450392_link)
  - [D:\XilinX\Vivado\2019.1\bin\vivado.bat -nolog -nojournal -notrace -mode batch -source board/pynq/mk.tcl -tclargs myproject-pynq true](https://gitee.com/shiliupi/easy_raspi_python_one/issues/I4RCXS#note_9450670_link)
  - [move to short path.](https://gitee.com/shiliupi/easy_raspi_python_one/issues/I4RCXS#note_9450760_link)
  - [ERROR: [Board 49-71] The board_part definition was not found for](https://gitee.com/shiliupi/easy_raspi_python_one/issues/I4RCXS#note_9450845_link)
    https://support.xilinx.com/s/article/70895?language=en_US
    > 当建立FMC连接时，连接器板部件将附加到实际的板部件上，从而导致项目板部件的更改。因此，当示例设计无法解密扩展的项目板部件时，将引发错误。要解决此问题，请在生成的示例设计项目上使用 FMC 连接，而不是在存在 IP 核的初始项目上建立 FMC 连接。

<p><img height="99px" src="https://images.gitee.com/uploads/images/2022/0326/143410_0fc250dc_5631341.png"> <img height="99px" src="https://images.gitee.com/uploads/images/2022/0326/094106_46842a1f_5631341.png"></p>

- 玩转Zynq连载17——新建Vivado工程
  https://blog.csdn.net/qq_45922361/article/details/103628480

- 使用VIVADO编写简单的Verilog程序和Testbench
  https://blog.csdn.net/xidian_hxc/article/details/103771131

  [syntax error near "DPI-C"](https://gitee.com/shiliupi/easy_raspi_python_one/issues/I4RCXS#note_9451352_link)
  
  - FBHelper.v
  - SDHelper.v
  - UARTGetc.v
  
  [DISABLE 另外三个V文件](https://gitee.com/shiliupi/easy_raspi_python_one/issues/I4RCXS#note_9457202_link)
  
  [不在本地机器RUNNING,只编译描述](https://gitee.com/shiliupi/easy_raspi_python_one/issues/I4RCXS#note_9457358_link)
  
  [观察生成的Verilog顶层模型](https://hubohan.space/2020/08/31/Difftest_note/) 【[笔记](https://gitee.com/shiliupi/easy_raspi_python_one/issues/I4RCXS#note_9457699_link)】
  
  - NUTSHELL [SSH: 192.168.62.139]
  - build
    - FBHelper.v
    - firrtl_black_box_resource_files.f
    - NutShellSimTop.fir
    - NutShellSimTop.anno.json
    - SDHelper.v
    - TopMain.v
    - TopMain.v.conf
    - UARTGetc.v
  
  'import "DPI-C" function void uart_getc(output byte ch);'
  
  [only TopMain.v](https://gitee.com/shiliupi/easy_raspi_python_one/issues/I4RCXS#note_9457873_link)
  
  - [Synth 8-439] module 'Monitor' not found ["C:/NutShell/build/TopMain.v":11949]
  - [Synth 8-6156] failed synthesizing module 'EXU' ["C:/NutShell/build/TopMain.v":11372]
    - [Synth 8-6156] failed synthesizing module 'Backend_inorder' ["C:/NutShell/build/TopMain.v":12401]
    - [Synth 8-6156] failed synthesizing module 'NutCore' ["C:/NutShell/build/TopMain.v":24917]
    - [Synth 8-6156] failed synthesizing module 'NutShell' ["C:/NutShell/build/TopMain.v":35298]
    - [Synth 8-6156] failed synthesizing module 'NutShellSimTop' ["C:/NutShell/build/TopMain.v":40882]
  - [Common 17-69] Command failed: Vivado Synthesis failed
  
  [参考资料说明](https://gitee.com/shiliupi/easy_raspi_python_one/issues/I4RCXS#note_9458483_link) https://chhzh123.github.io/blogs/2020-03-11-vivado-hls/ 

  - The Word on Zynq
  
    <p><img src="https://images.gitee.com/uploads/images/2022/0326/184529_eabc43af_5631341.png" height="99px"> <img src="https://images.gitee.com/uploads/images/2022/0326/184537_9b6b2ac2_5631341.png" height="99px"> <img src="https://images.gitee.com/uploads/images/2022/0326/184818_9934d886_5631341.png" height="99px" title="Image of software stack"> <img title="Image of a ZedBoard" src="https://images.gitee.com/uploads/images/2022/0326/184834_ac531b8d_5631341.png" height="99px"></p>

- [大飞品树莓系列课程汇总](https://gitee.com/shirf_taste_raspi/shirf_serial_share/) （[全文](https://gitee.com/shiliupi/easy_raspi_python_one/issues/I4RCXS#note_9513827_link)）

---

# [Verilog入門教學 的视频](https://gitee.com/RV4Kids/RVWeekly/issues/I3IA6B#note_6205293_link)

### 本篇

- 2-7（零）硬體描述語言簡介 
https://www.bilibili.com/video/BV1My4y1H76C
- 2-8（一）verilog基礎語法 
https://www.bilibili.com/video/BV1Sh411C7EQ
- 3-7（二）電路驗證工具-QuartusII與FPGA 
https://www.bilibili.com/video/BV16Z4y1P7Ln
- 3-18（三）模組調用、匯流排與八對一多工器 
https://www.bilibili.com/video/BV1r64y1D7y6
- 4-25（四）七段顯示器控制電路 
https://www.bilibili.com/video/BV1XK4y1R7kK
- 5-7（五）四位元漣波進位加法器 4-bit ripple-carry adder 
https://www.bilibili.com/video/BV1p541137BQ
- 5-17（六）四位元超前進位加法器 4-bit carry lookahead adder 
https://www.bilibili.com/video/BV1W64y1y7eJ
- 5-29（七）四位元加減法器與溢位偵測 
https://www.bilibili.com/video/BV1nq4y1j7P3
- 6-12（八）四位元乘法器 
https://www.bilibili.com/video/BV1544y1B721
- 6-26（九）解碼器 Decoder 
https://www.bilibili.com/video/BV1Fg41137WC
- 7-10（十）優先序編碼器 Priority encoder 
https://www.bilibili.com/video/BV1fq4y1s7iX
- 8-7（十一）累加器 accumulator 
https://www.bilibili.com/video/BV1BU4y1J7T5
- 9-12（十二）除頻器 frequency divider

### 其他

- 5-25 Live2D Model Test 
https://www.bilibili.com/video/BV1Sb4y1o758
- 8-12 休假公告
https://www.bilibili.com/video/BV1jL411J7jA

### 背景知识

- 2-7 07:32（一）進位制與進位轉換 
https://www.bilibili.com/video/BV1Cy4y1n7gr
- 2-7 06:36（二）位元、二補數與加減法
https://www.bilibili.com/video/BV1Br4y1P79S
- 2-10 6:32（三）布林代數與邏輯閘
https://www.bilibili.com/video/BV1m5411J7V7
- 4-12 8:58（四）卡諾圖 K-map
https://www.bilibili.com/video/BV1ci4y1A7Zs
- 7-24 7:50（五）循序邏輯電路、latch與flip-flop
https://www.bilibili.com/video/BV1154y1E7iw