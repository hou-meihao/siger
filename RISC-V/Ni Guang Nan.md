<p><img src="https://images.gitee.com/uploads/images/2022/0123/184025_009c9257_5631341.png" width="706px" title="痴心不改，大国匠“芯”"></p>

- [适当聚焦RISC-V，中国芯片自立自强](https://gitee.com/RV4Kids/RVWeekly/issues/I4CS7N#note_8440124_link)
- [中国开放指令生态（RISC-V）联盟2021年会](https://gitee.com/RV4Kids/RVWeekly/issues/I4CS7N#note_8439794_link) 
- [只为中国"芯"，致敬孤胆英雄倪光南院士！](https://gitee.com/RV4Kids/RVWeekly/issues/I4CS7N#note_8438646_link) 
- [开源创新发展论坛](https://gitee.com/RV4Kids/RVWeekly/issues/I4CS7N)：<a href="https://gitee.com/RV4Kids/RVWeekly/issues/I4CS7N#note_6921788_link" target="_blank" title="从开源大国走向开源QIANG国">倪光南</a> & <a href="https://gitee.com/RV4Kids/RVWeekly/issues/I4CS7N#note_6921877_link" target="_blank" title="构建 RISCV 的开源新生态">David Patterson</a> 
  - 倪光南：[从开源大国走向开源QIANG国](https://gitee.com/RV4Kids/RVWeekly/issues/I4CS7N#note_6921788_link)
  - David Patterson：[构建 RISCV 的开源新生态](https://gitee.com/RV4Kids/RVWeekly/issues/I4CS7N#note_6921877_link)

> 这是一份敬意，弥补自己的不识[泰山](https://images.gitee.com/uploads/images/2021/0628/135702_3712c358_5631341.png)。[感谢倪老对同学们的鼓励](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/CNRV%202021.6.%2021.%20-%2022.md#day2-poster-1min)！ :pray: 从 oE 社区 开发者大会，到 RISC-V 中国峰会，总是见到他致辞，我竟然为了看摊儿，错过了亲耳听他高屋建瓴地演讲，在此致敬，以表歉意！:pray:

> 上面的标题实际是倒叙，是撰写本期内容搜集的顺序，起点是全文聆听《[从开源大国走向开源QIANG国](https://gitee.com/RV4Kids/RVWeekly/issues/I4CS7N#note_6921788_link)》，也就是四个月前，距离第一次见到倪老2个月后，我才意识到这份擦肩而过的遗憾。直到2021岁末，各种声讨联想路线误国，我才和大家一样，油然而生的敬意，促使我要补上这一课。不是侥幸，确实是要感谢网友们整理的 倪老 的传记《[只为中国"芯"，致敬英雄倪光南院士！](https://gitee.com/RV4Kids/RVWeekly/issues/I4CS7N#note_8438646_link)》就是我们的心情！:pray:

> 此时此刻[《第三期一生一芯》最后一节 SOC 课](https://gitee.com/RV4Kids/RVWeekly/issues/I4CS7N#note_8439816_link)正在播放，明天就是《[中国开放指令生态（RISC-V）联盟2021年会](https://gitee.com/RV4Kids/RVWeekly/issues/I4CS7N#note_8439794_link)》了，16:00 – 17:00 开始的《第三期“一生一芯”计划总结报告》包含了20分钟的课程总结，以及8名同学的学习心得报告。能敢在这个时间之前，补上这期致敬刊，也是弥补自己 ”逃课第三期“ 交不上作业的遗憾。而原本为掉队学员开的2022春季的流片，也确定为 第四期 正式开课了!  _祝一切顺利，年会圆满成功！_ 就是我真心的祝愿！

> 多少有些临时抱佛脚《[适当聚焦RISC-V，中国芯片自立自强](https://gitee.com/RV4Kids/RVWeekly/issues/I4CS7N#note_8440124_link)》是最后落笔的资料，和 [第一届 RISC-V 中国峰会](https://space.bilibili.com/1121469705/) [倪光南 - 中国 RISC-V 发展 - 2021/6/22](https://www.bilibili.com/video/BV1iB4y1T7YK/) 的 SLIDE 截屏 一起，重温他的演讲，才更加愧疚自己的不用功啦。寒假里，同学们要接替我，补全这门功课。将作为 《[石榴派 寒假集训](https://gitee.com/xiaoxu-tongxue/siger/issues/I4RDZV)》 的一个起点任务。

# [倪光南 - 中国 RISC-V 发展 - 2021/6/22](https://www.bilibili.com/video/BV1iB4y1T7YK/)

> [第一届 RISC-V 中国峰会](https://space.bilibili.com/1121469705/) 

《[适当聚焦RISC-V，中国芯片自立自强](https://www.riscv-mcu.com/article-show-id-827.html)》

> 6月22日，RISC-V 2021中国峰会在上海科技大学举行。中国工程院倪光南院士发表精彩演讲，分享RISC-V 在中国发展现状。

> 倪光南提到，从整体看，新一代信息技术中开源比重较大，在AI、物联网、云计算等领域开源软件在全部软件中的贡献率占80%。而ICT企业硬件和软件人员的比率约为8比2。由此估算，开源对ICT企业的贡献率可达60%。谁拥抱开源，谁就拥抱信息技术的未来。 中国是世界开源的大国，十四五计划和远景规划已经列入开源事顶，指出支持开源技术，开源社区的发展，完善开源知识产权，鼓励企业开放源代码，推动芯片设计和应用。可以说，开源的作用在国家层面受到非常高的重视。中国正从开源大国走向开源强国。其中RISC-V是很重要的组成部分。 倪光南指出，RISC-V是未来推动新一代信息技术发展的动力。对于RISC-V的重视，不仅是芯片设计和学校教学方面，我们必须提到一个新的高度，CPU架构对芯片产业有引领作用。芯片的设计采用不同架构，生态就不一样。光是把CPU架构看作影响芯片设计还不够，而是对整个芯片产业，芯片架构是源头，它会影响到人才培养，IP库，EDA工具、芯片生产制造，测试封装等等。 目前，微处理器SoC在芯片产品中的比重已超过70%。并且这个趋势会增大。CPU架构有多少？大概有7种架构。倪光南认为，这种情况下，假如有7个生态链并行去推，并是不太合理的。适当聚焦RISC-V，发展中国的芯片。这意味着我们的芯片把主线放在基于RISC-V架构的产业链上。

> 这主要体现在几个方面，不同架构的设计工具不同，我们需要针对RISC-V架构优化的EDA工具；完善的IP库支持，使得芯片产品设计就更容易。 还有人才培养，灵敏设计工具和RISC-V架构的结合，我们发现培养设计工程师更容易。比如国科大，一个学生团队四五个月就可以设计出一款芯片。这是过去传统的方法和架构达不到的。它大大缩短了人才培养的周期，加快芯片发展的速度，对中国芯片人才资源的培养和对芯片产业的发挥有好处。 中国适当聚焦RISC-V，有助于中国芯片产业实现自立自强。 那么RISC-V的前途在哪里？倪光南表示，我们有人才的优势，大规模市场的优势，例如物联网、5G、AI设备等，中国是最大的市场。因此中国将来用什么芯片设计，也将对世界芯片产业带来重要影响。 如果说互联网主要是x86，移动互联网是Arm，而以物联网、AI为代表的新一代信息技术，我们希望RISC-V占到重要地位。RISC-V是破解中国物联网生态碎片化的关键技术。RISC-V与x86和Arm架构相比，生态环境由于没有后两者已经成熟的生态，就要靠新的领域来支撑。幸好我们信息技术发展快。传统领域达不到他们的高度，新的领域有机会。

> 另外，RISC-V虽然是开源，但的确存在碎片化导致RISC-V不统一不兼容的可能，不过，通过RISC-V基金会、联盟的更多协调，加强对开源工作的整合，能够解决好碎片化问题。 开源还可以降低芯片设计门槛，例如90%可以开源，10%面向应用需求定制代码。那么在芯片领域数量级降低开发成本，中小企业能够在芯片领域有所作为。 中国在建设RISC-V生态中积极建设，例如平头哥、全志、芯来、优矽渭河、兆易创新等等。涉及从开发环境、IP、工具链、芯片等各个环节。Semico数据显示，2025年全球RISC-V出货量将超过600亿颗，Ark Invest预测，2030年RISC-V在数据忠领域也有一席之地。 不过，倪光南相信，有着20亿车机、40亿工业设备、100亿终端设备、1万亿数据终端和6500万开发者的多样化应用生态的支持，RISC-V未来的出货量会更高。 他仍然坚信，未来RISC-V很可能发展成为世界主流CPU之一，从而在CPU领域形成Intel、Arm、RISC-V三分天下的格局。

> 中国大力支持RISC-V这一新兴的开放指令集架构，希望RISC-V社区和中国的芯片产业、软件产业实现双赢，中国将为RISC-V开源芯片及其生态的发展做出贡献。

<p><img title="1" width=169px" src="https://images.gitee.com/uploads/images/2022/0123/162930_3bc635d8_5631341.png"> <img title="2" width=169px" src="https://images.gitee.com/uploads/images/2022/0123/162945_240db888_5631341.png"> <img title="3" width=169px" src="https://images.gitee.com/uploads/images/2022/0123/163007_a5e4ef7a_5631341.png"> <img title="4" width=169px" src="https://images.gitee.com/uploads/images/2022/0123/163021_8623fa6a_5631341.png"> <img title="5" width=169px" src="https://images.gitee.com/uploads/images/2022/0123/163035_2ba4dcb7_5631341.png"> <img title="6" width=169px" src="https://images.gitee.com/uploads/images/2022/0123/163052_0be9ae4a_5631341.png"> <img title="7" width=169px" src="https://images.gitee.com/uploads/images/2022/0123/163116_5e0622e0_5631341.png"> <img title="8" width=169px" src="https://images.gitee.com/uploads/images/2022/0123/163138_80317780_5631341.png"> <img title="9" width=169px" src="https://images.gitee.com/uploads/images/2022/0123/163158_7d977e07_5631341.png"> <img title="10" width=169px" src="https://images.gitee.com/uploads/images/2022/0123/163220_e11062e4_5631341.png"> <img title="11" width=169px" src="https://images.gitee.com/uploads/images/2022/0123/163242_7c4d7764_5631341.png"> <img title="12" width=169px" src="https://images.gitee.com/uploads/images/2022/0123/163302_16816fc2_5631341.png"> <img title="13" width=169px" src="https://images.gitee.com/uploads/images/2022/0123/163320_fb3a135b_5631341.png"> <img title="14" width=169px" src="https://images.gitee.com/uploads/images/2022/0123/163340_171bdd9e_5631341.png"> <img title="15" width=169px" src="https://images.gitee.com/uploads/images/2022/0123/163410_0707f882_5631341.png"> <img title="16" width=169px" src="https://images.gitee.com/uploads/images/2022/0123/163430_6b3ec98b_5631341.png"> <img title="17" width=169px" src="https://images.gitee.com/uploads/images/2022/0123/163448_695d5a17_5631341.png"> <img title="18" width=169px" src="https://images.gitee.com/uploads/images/2022/0123/163504_a58adee6_5631341.png"> <img title="19" width=169px" src="https://images.gitee.com/uploads/images/2022/0123/163519_917c0bbf_5631341.png"> <img title="20" width=169px" src="https://images.gitee.com/uploads/images/2022/0123/163536_93391361_5631341.png"> <img title="21" width=169px" src="https://images.gitee.com/uploads/images/2022/0123/163553_d5d5a055_5631341.png"> <img title="22" width=169px" src="https://images.gitee.com/uploads/images/2022/0123/163609_6b893cb5_5631341.png"> <img title="23" width=169px" src="https://images.gitee.com/uploads/images/2022/0123/163625_110e7211_5631341.png"> <img title="24" width=169px" src="https://images.gitee.com/uploads/images/2022/0123/163657_985f7a12_5631341.png"> <img title="25" width=169px" src="https://images.gitee.com/uploads/images/2022/0123/163720_019bc256_5631341.png"> <img title="26" width=169px" src="https://images.gitee.com/uploads/images/2022/0123/163736_e8ca6e25_5631341.png"> <img title="27" width=169px" src="https://images.gitee.com/uploads/images/2022/0123/163749_78e2cd5d_5631341.png"> <img title="28" width=169px" src="https://images.gitee.com/uploads/images/2022/0123/163806_50653f74_5631341.png"> <img title="29" width=169px" src="https://images.gitee.com/uploads/images/2022/0123/163823_c886f185_5631341.png"> <img title="30" width=169px" src="https://images.gitee.com/uploads/images/2022/0123/163838_dbdbcb24_5631341.png"> <img title="31" width=169px" src="https://images.gitee.com/uploads/images/2022/0123/163853_e61c9e55_5631341.png"> <img title="32" width=169px" src="https://images.gitee.com/uploads/images/2022/0123/163916_63bf8b84_5631341.png"> <img title="33" width=169px" src="https://images.gitee.com/uploads/images/2022/0123/163936_c65ab476_5631341.png"> <img title="34" width=169px" src="https://images.gitee.com/uploads/images/2022/0123/164004_ed687b3e_5631341.png"> <img title="35" width=169px" src="https://images.gitee.com/uploads/images/2022/0123/164028_e6ad7258_5631341.png"> <img title="36" width=169px" src="https://images.gitee.com/uploads/images/2022/0123/164054_f035d290_5631341.png"></p>

- [POSTER - 石榴派 - 袁德俊](https://www.bilibili.com/video/BV1qV411H7j9)

# [中国开放指令生态（RISC-V）联盟2021年会](https://mp.weixin.qq.com/s/AnDD1wS0tR__EJUsuWC_rA)

crva 中国开放指令生态RISCV联盟 2022-01-21 14:52

为推进RISC-V技术与生态在国内的快速发展，联盟将于2022年1月24日举办“中国开放指令生态（RISC-V）联盟2021年会（线上）”。本次大会将邀请RISC-V国际基金会、国内RISC-V行业领军人物、产业联盟/科研院所代表等多位业内知名人士围绕RISC-V技术、产业、生态各个方面进行报告和展望。在此欢迎国内外RISC-V相关企业、科研院所、高校等行业人员在线参加本次大会，共同探讨RISC-V在中国的发展，打造互惠共赢、开源开放的生态环境。

| 会议名称 | 中国开放指令生态（RISC-V）联盟2021年会（线上） |
|---|---|
| 会议时间 | 2022年1月24日 13:30 – 17:30 |
| 指导单位 | 中央网信办信息化发展局<br>中科院科技促进发展局 |
| 主办单位 | 中国开放指令生态（RISC-V）联盟 |
| 承办单位 | 中国科学院计算技术研究所 |
| 大会主席 | 倪光南  中国工程院院士<br>中国开放指令生态（RISC-V）联盟理事长 |

### 会议链接：

https://meeting.tencent.com/dm/5u5hEH1ZFgCx

腾讯会议号：543-369-107

会议二维码

![输入图片说明](https://images.gitee.com/uploads/images/2022/0123/151839_7363144d_5631341.png "屏幕截图.png")

### 会议日程：

年会时间：2022年1月24日

| 时间 | 内容 |
|---|---|
| 13:30 – 13:50 | 领导致辞
| 13:50 – 14:10 | 中国开放指令生态（RISC-V）联盟2021年度工作报告<br>联盟秘书长 包云岗 |
| 14:10 – 14:40 | 中国开放指令生态（RISC-V）联盟工作组年度报告<br>联盟工作组组长 |
| 14:40 – 15:00 | 主旨演讲 1 Mark Himelstein CTO RISC-V International |
| 15:00 – 15:20 | 主旨演讲 2 戴伟民 理事长 中国RISC-V产业联盟 |
| 15:20 – 15:40 | 主旨演讲 3：RISC-V生态基础软件的现状与展望<br>演讲者：武延军 研究员 中科院软件所 |
| 15:40 – 16:00 | 主旨演讲4：RISC-V為所必然<br>演讲者：林志明 台湾RISC-V联盟副会长、晶心科技董事长 |
| 16:00 – 17:00 | 第三期“一生一芯”计划总结报告<br>包含了20分钟的总结报告，以及8名同学的学习心得报告。 |
| 17:00 – 17:20 | 中国开放指令生态（RISC-V）联盟2021年度颁奖 |
| 17:20 – 17:30 | 闭幕 |

> 中国开放指令生态（RISC-V）联盟（英文缩写为CRVA ; China RISC-V Alliance）围绕RISC-V指令集，以促进开源开放生态发展为目标，以重点骨干企业、科研院所为主体，整合各方资源，建立产、学、研、用深度融合的联盟，推动协同创新攻关，促进RISC-V相关开源技术的开发与共享，推广相关技术和产品的应用，探索体制机制创新，推进RISC-V生态在国内的快速发展。
> 
> 在2019年1月11日的联盟大会上，CRVA得到了中央网信办与上海、浙江等地方网信办的大力支持，吸引了来自国内各芯片企业以及高校参会。大会先后向学术界和工业界杰出的项目和企业颁发了科教实践奖、前沿芯片奖和生态贡献奖，以鼓励发展更多有影响力的RISC-V芯片项目。
> 
> 为进一步扩大联盟辐射面，加强各方联系与交流，推动联盟各项既定计划顺利开展，联盟长期面向社会各界征集单位会员与个人会员。详情请访问联盟官网（ http://crva.io ）填写会员申请表或咨询联盟秘书处。
> 
> 联盟秘书处联络人：李迪
电话：010-62601034
电子邮箱：crva@ict.ac.cn
> 
> 我们诚挚邀请有识之士加入联盟，共创开源芯片及开放指令生态的黄金时代！
> 
> ![输入图片说明](https://images.gitee.com/uploads/images/2022/0123/152232_2d718875_5631341.png "屏幕截图.png")

### 第三期”一生一芯“ SoC和后端技术报告

- 【会议时间】: 2022年1月23日晚上19:00 - 20:00
- 【腾讯会议】: 723-271-264
- 【会议简介】: SoC报告，将会介绍SoC的工作进行简要的介绍，SoC如何集成、验证方法、如何超频和选择自己的CPU以及所遇到的问题，最后报告还将对“一生一芯”未来的规划进行介绍。后端报告，将会学习到RTL代码如何一步一步变化成芯片，数字后端告诉你代码最高能跑多少频率、如何对代码性能进行评估。

# 痴心不改，大国匠“芯”,致敬倪光南院士!

- [只为中国“芯”——悲怆可敬的孤胆英雄！向联想创始人倪光南院士致敬！](https://mp.weixin.qq.com/s?__biz=MzA5MjI0MDkzMA==&mid=2654072708&idx=1&sn=0f987034853857ba32242216ba3e1858)
  爱语吧英语 2021-12-23 20:09

  近来联想风波不断一直处于舆论的风口浪尖，前CEO柳传志被无数人评价讨论。而在与他和联想相关的人中，有一个名字反复被提及——那就是倪光南。

  > Ni, born in 1939, was one of the first scientists to study information processing and pattern recognition of Chinese characters. He led the development of Lenovo's microcomputers in the early 1990s and became an academic at the Chinese Academy of Engineering in 1994.  
  > 倪光南，出生于1939年，是最早研究汉字信息处理和模式识别的科学家之一。 上世纪90年代初，他领导了联想微型计算机的开发，1994年成为中国工程院院士。 

  1984年，倪光南加入中国科学院计算技术研究生新技术发展公司（即联想集团前身）时公司里一共只有11个人。他用了三年时间研发出八种型号的联想汉卡，使联想在这三年内产值近亿，联想能有后来的辉煌，他功不可没。

  <p><img src="https://images.gitee.com/uploads/images/2022/0123/110623_6bb8e8e5_5631341.png" width="199px"></p>

  但也就是这样一位为联想做出巨大贡献的科学家，却因坚持科研而与柳传志产生分歧，最后被联想“扫地出门”。

  有人说他虽曾是联想的核心人物，但离开时也没捞到多大好处，实在可惜。

  但对倪光南来说，他不在意赚多大钱，他一生痴心不改的就只有“中国芯片”这四个字罢了。

  <p><img src="https://images.gitee.com/uploads/images/2022/0123/110651_6b917bed_5631341.png" width="199px"></p>

  1939年8月1日，正值抗战时期，倪光南出生了。

  从小听多了父母讲给他的逃难故事，倪光南更明白“祖国要强大才能不受欺负”，也更明白科技在这个年代的重要性。

  1950年，倪光南进入上海复兴中学，之后以优异的成绩考入南京工学院（1988年更名为东南大学）的无线电系，并以满分成绩毕业，从此开始了他对逐“芯”之路。

  毕业后，他听从分配来到中国科学院计算技术研究所工作，作为中国第一批“IT男”，他参与研制了中国第一台电子管计算机，并提出了开展汉字处理研究，跨越了从“汉字”到“计算机”的巨大鸿沟。

  <p><img src="https://images.gitee.com/uploads/images/2022/0123/111250_f1e9f306_5631341.png" width="199px"></p>

  1981年，加拿大科学院访华，与倪光南相谈后十分赏识对方才能，当下就聘请他去渥太华担任访问研究员，和国外教授待遇等同。那时他的年薪高达43000加元，按照官方汇率来算是当时国内工资的70倍。

  倪光南在渥太华的一众中国学生和访问学者中也是最忙的一个，当时大家一起聚会邀请他，倪光南的回复总是“没时间”。

  他在渥太华很受器重，待遇与当时国内相比也是天壤之别。但倪光南心中深处那一抹鲜艳的中国红却在他乡越燃越亮。

  <p><img src="https://images.gitee.com/uploads/images/2022/0123/111723_68d27153_5631341.png" width="199px"></p>

  有一天，倪光南渥太华的商业街上走进一家鞋店，橱窗里的洋鞋被整齐有序的陈列在干净的橱窗中，而地下的一双双“中国制造”被随意摆放在一边，旁边立着个纸牌写着“任选一双1.99元”。

  倪光南顿时感到内心一阵刺痛，他苦苦求学为的是什么？不就是有朝一日能为国家科技进步贡献自己的一份力吗？

  祖国，才是他一身价值的施展之处。

  于是，在渥太华呆了两年后，他毅然回国。

  <p><img src="https://images.gitee.com/uploads/images/2022/0123/111809_347a8395_5631341.png" width="199px"></p>

  回国前，倪光南还自费购买了关于研制汉字微机样机的关键器材，包括集成电路芯片和C编辑器等，多的像一座小山。

  这几乎花了他所有的存款。但他不以为意，只想回国把计算机做起来。

  <p><img src="https://images.gitee.com/uploads/images/2022/0123/111833_9865bbed_5631341.png" width="199px"></p>

  回国后不久，他被中科院创办的计算所公司（即联想集团前身）邀请任职公司总工程师。
  倪光南将通宵达旦研究出的汉卡技术带到这里，有了从渥太华带回来的“宝贝”，倪光南做出了联想汉卡和联想系列微机。这为公司带来的收益巨大，计算所后来也更名为联想集团。

  在联想集团前景一片光明之际，倪光南却与时任联想CEO的柳传志起了争执。

  担任公司董事兼总工程师的倪光南认为联想应该建立起自己的核心技术，把大部分资金投放到“中国芯”的工程上；而柳传志却认为此时正是扩大企业规模和利润的好时机。两人就此争执不下。

  于是，1995年6月30日，联想免去倪光南总工程师的职务。1999年，倪光南被正式解雇。

  在专注研发和企业利益之间，联想选择了企业利益。

  倪光南曾说，“科学家与社会结合的目的，一定要出于公心，不能抱有太多的私念，否则就丢掉了科学造福社会的本意，成了某一个人打着科学的旗号造福自己了。”

  就像是从渥太华回来的时候，他用私款购买研究器材时一样，倪光南对待科学，一向秉持公心。

  但他终究是少数人。

  <p><img src="https://images.gitee.com/uploads/images/2022/0123/111939_7b16c200_5631341.png" width="199px"></p>

  这一次的挫败没有压垮他，他心中要做“中国芯”的愿望战胜了所有消极。

  1999年，加拿大华人李德磊找到他，表示了自己专注做芯片技术的想法。倪光南激动地老泪纵横，毫不犹豫就答应了。

  倪光南在李德磊的方舟科技公司忙的可谓四脚朝天，除了实验研究要负责，公司没有资源，他也要亲自去拉去谈，生怕错过这一次机会。

  > Ni helped Fangzhou gather funds, ask for government help and look for any resources to develop chips. He did not earn anything from this. He wants to do everything, because he wants a new core frame for the IT industry.  
  > 倪光南帮助方舟筹集资金，求助各方，并寻找开发芯片的资源。 他没有从中赚到任何钱。 他想做所有的事情，因为他想为IT行业建立一个新的核心框架。

  <p><img src="https://images.gitee.com/uploads/images/2022/0123/112020_44064180_5631341.png" width="199px"></p>

  终于，2001年，他成功研发出代号“方舟-1”的CPU芯片产品，改写了“中国无芯”的历史。

  虽然有了研发技术，但国内缺乏有能力做产品原型的公司，后来又面临着资金缺乏等各种问题。

  > However, chip manufacturers are faced with a severe shortage of funds. "Nobody wants to invest in such an area that lacks talents, takes a long time to see profit and has a high risk of failure," one of Ni's student and former assistant said.  
  > 然而，芯片制造商面临着严重的资金短缺。 “没有人愿意投资这样一个缺乏人才、需要很长时间才能看到利润、失败风险很高的领域，”倪光南的一位学生兼前助手说。

  <p><img src="https://images.gitee.com/uploads/images/2022/0123/112100_d53fdf82_5631341.png" width="199px"></p>

  一来二去，李德磊也失了耐心，决定另谋方向。

  最终，倪光南选择离开方舟，而他的芯片梦又再次破碎。

  之后，他再也没有加入新的企业，只一心投入到推广国产芯片，软件，开发自助操作系统等事情上。

  <p><img src="https://images.gitee.com/uploads/images/2022/0123/112125_3ccb7c4b_5631341.png" width="199px"></p>


  他坚称：

  > "China should make its own chips, otherwise we will inevitably meet many problems," Ni said, "If we cannot master the core technology, other countries will make obstacles for us sooner or later through various means."  
  > “中国应该制造自己的芯片，否则我们一定会遇到很多问题，”倪光南说，“如果我们不能掌握核心技术，其他国家迟早会通过各种方式给我们制造障碍。” 

  他找各种机会在演讲会议上宣传核心技术的重要性，“没有核心技术，是中国目前最大的隐患。”

  <p><img src="https://images.gitee.com/uploads/images/2022/0123/112223_943e57dd_5631341.png" width="199px"></p>

  但他的不断呼吁，招来的却是人们的嘲讽。

  有人说他就像祥林嫂一样魔怔了，只是每日每夜的从嘴里吐出的是“芯片”二字罢了。

  也有人说，他太固执了，只活在自己的幻想里。

  <p><img src="https://images.gitee.com/uploads/images/2022/0123/112234_b073841f_5631341.png" width="199px"></p>

  心怀痴迷，却屡屡碰壁，这让他受尽圈内圈外人的嘲笑。但他坚信，中国芯片值得他投入一生。

  > Ni has never given up appealing for the development of China's own chips and operating systems.  
  > 倪光南从未放弃呼吁人们发展中国自己的芯片和操作系统。 

  <p><img src="https://images.gitee.com/uploads/images/2022/0123/112314_5d34a4bc_5631341.png" width="199px"></p>

  直到2018年4月，美国宣布将禁止美国公司向中兴通讯股份有限公司销售零部件、商品、软件和技术，到 2025年3月13日解禁。

  这一道禁令就让中兴立即陷入瘫痪状态，人们终于意识到倪光南说的那句“没有核心技术，是中国目前最大的隐患”是什么意思。

  这时，人们才发现，中国“芯”究竟有多重要。

  > Around 90 percent of chips in China are imported, as China's domestically developed chips can only meet 8 percent of demand in the country, the China Press reported on Wednesday.  
  > 据中国新闻社周三报道，中国大约90%的芯片是进口的，因为中国自主研发的芯片只能满足国内8%的需求。   

  > Ni Guangnan, a 79-year-old academic with the Chinese Academy of Engineering, drew online attention recently for the efforts he has been making in the field for almost two decades.  
  > 79岁的倪光南是中国工程院院士，最近因为他在这一领域做的近20年的努力引起了网上的关注。 

  <p><img src="https://images.gitee.com/uploads/images/2022/0123/112416_96902391_5631341.png" width="199px"></p>

  一夜之间，倪光南又从那个“疯子”成了人们口中富有远见卓识的智者。

  但他不关心这些，他只想在剩下不多的岁月里，为“中国芯片”尽自己最后的力量。

  2018年，倪光南被评选为“最美科技工作者”，朋友来点为他祝贺，倪光南却说，“一个老头子，什么美不美的，我们还是谈谈芯片吧！”

  <p><img src="https://images.gitee.com/uploads/images/2022/0123/112443_6080be22_5631341.png" height="199px"></p>

  一生痴心不改，只为中国芯片。

  为这位院士，致敬。

- [致敬倪光南院士！](https://mp.weixin.qq.com/s?__biz=MjM5MzA0MDUyMg==&mid=2247516731&idx=2&sn=464a3261eb1f4d2f7e87a0f3c2473b6c)
  (自主可控新鲜事 2021-12-16 14:36)

  本文内容来源于网络 (正文共4737，建议阅读时间10分钟)

  - **中国开源软件推进联盟名誉主席陆首群致信倪光南院士讲述亲历联想创办等事宜** 

    2021年12月14日，陆首群教授给中国工程院院士倪光南致信一封，讲述亲历联想创办等有关事宜，具体内容如下：

    <p><img src="https://images.gitee.com/uploads/images/2022/0123/105144_e1a88e83_5631341.png" height="199px"> <img src="https://images.gitee.com/uploads/images/2022/0123/105158_883d3f4b_5631341.png" height="199px"> <img src="https://images.gitee.com/uploads/images/2022/0123/105514_27b0a3e3_5631341.png" width="199px"></p>

    > 陆首群，曾任国家信息化联席会议办公室常务副主任、吉通公司董事长，主持首都电子商务工程的研究与实践， 发表了许多有影响的网络与信息化前沿技术、电子商务、电子政务方面的论文 。

  - **夏颖奇：致敬倪光南** 

    <p><img src="https://images.gitee.com/uploads/images/2022/0123/105801_41be85f1_5631341.png" height="199px"> <img src="https://images.gitee.com/uploads/images/2022/0123/105814_9bdae2f1_5631341.png" height="199px"> <img src="https://images.gitee.com/uploads/images/2022/0123/105828_0a9cbea8_5631341.png" height="199px"> <img src="https://images.gitee.com/uploads/images/2022/0123/105838_e37c9fc0_5631341.png" height="199px"> <img src="https://images.gitee.com/uploads/images/2022/0123/105850_fd6b00b6_5631341.png" height="199px"></p>

    > 2021年12月15日，夏颖奇博士为致敬倪光南院士，致敬中关村的前辈们写下千字长文，具体内容如上。

      - [陆首群谈亲临其境之联想创办等等](https://mp.weixin.qq.com/s/QZBq4I771PXrrWmIVhnKRw)  
        (原创 求索之路平坦心 陶勇 求索之路平坦心 2021-12-15 14:10)

        <p><img src="https://images.gitee.com/uploads/images/2022/0123/104504_9aba02c5_5631341.png" height="199px"></p>

      - [夏颖奇：致敬倪光南](https://mp.weixin.qq.com/s/TPpgoHt3bCxn4iGW4DyFTw)  
        (夏颖奇 陶勇 求索之路平坦心 2021-12-16 00:01)

        <p><img src="https://images.gitee.com/uploads/images/2022/0123/104439_4d74a584_5631341.png" width="199px"></p>

        1983年5月，倪光南离开加拿大时NRC同事开欢送会。

      - [“意思表示不真实”之倪光南“检讨”](https://mp.weixin.qq.com/s?__biz=MzAwMDYxODU3Nw==&mid=2247486591&idx=1&sn=4ef3462381e05a39d92d5f9a93f9f116)  
        (原创 求索之路平坦心 陶勇 求索之路平坦心 2021-12-21 22:52)

- [中国工程院院士倪光南：发展RISC-V的首要问题不是钱，是确立技术路线](https://mp.weixin.qq.com/s?__biz=MjM5MzI1Njc1Mw==&mid=2649628644&idx=1&sn=c6b08d9623e6fc6c02383eeadfca7769)  「[笔记](https://gitee.com/RV4Kids/RVWeekly/issues/I4CS7N#note_8439058_link)」
  原创 张依依 中国电子报 2021-12-21 17:13

  <p><img src="https://images.gitee.com/uploads/images/2022/0123/122452_7c7f6e61_5631341.png" width="199px"></p>

  > 12月20日下午，由中国电子信息产业发展研究院、珠海市人民政府、横琴粤澳深度合作区执行委员会主办的，2021第十六届“中国芯”集成电路产业促进大会暨“中国芯”优秀产品征集结果发布仪式在珠海拉开帷幕。中国工程院院士倪光南在本次大会的开幕式上为大会寄语。

    - 多架构并存格局影响国内CPU市场突破

      > “由此可见，我国现存多种CPU架构并存的格局难以取得竞争优势，这样我国芯片业最终仍然很难摆脱受制于人的局面。”倪光南说道。

    - RISC-V架构带来发展新机遇

      > 现阶段，在开源领域，我国科技人员已经创建了一些开源社区，并拥有了相当话语权甚至一定的主导权。倪光南以OpenEuler和OpenHarmony等开源社区为例说道，中国开源界正在实现新的创新突破，逐渐将开源以国家战略的角度做大做强。

- [联想创始人倪光南：联想落后于华为是因为路线错了](https://mp.weixin.qq.com/s?__biz=MzI0NzE0MzE3OA==&mid=2651041375&idx=3&sn=290b37e3b666584d659789365c833363) 「[笔记](https://gitee.com/RV4Kids/RVWeekly/issues/I4CS7N#note_8439069_link)」
  倪光南 华听 2018-12-24 22:50

  <p><img src="https://images.gitee.com/uploads/images/2022/0123/123517_4f8aeff4_5631341.png" width="199px"></p>  

  > 倪光南在演讲中表示，华为联想这30年来犹如龟兔赛跑，在1988-1995第一阶段，联想的“技工贸”胜过了华为的“贸工技”，在1995年，联想销售额67亿元，是华为的4.5倍。

  > 从1996-现在第二阶段，华为的“技工贸”胜过了联想的“贸工技”，2001年，华为销售额超过联想，截至2018年12月22日，联想市值81亿美元，据估值华为价值已超4000亿美元，二者差距接近50倍。

  > 倪光南指出，华为成功很多原因，“但是在前十年，联想未股改前，我们相信，科技的股权是会体现的，但是股改后并没有体现，很多人都不敢来，大量人员都离开了。”但与之相对，这方面华为就做得比较好。

  > 外部环境基本一样，联想和华为差距在什么地方？倪光南认为是路线问题。一方面，华为始终坚持研发投入，此外，华为股权对科技人员产生了激励。“联想现在做得不好，一是因为路线不对，而是知识产权0股权对公司发展没有好处。”

  > 倪光南认为，今后应该尽可能加强科技人员的保护，充分激发科技人员的创造。

# 2021中关村论坛-开源创新发展论坛

- 倪光南 - 开源生态

<p><img src="https://images.gitee.com/uploads/images/2021/1006/205653_7a1bfe71_5631341.png" width="199px"></p>

1:00:00 - 1:11:00

- 大卫-皮特森：构建 RISCV 的开源新生态！

<p><img src="https://images.gitee.com/uploads/images/2021/1006/204902_de382e7e_5631341.png" width="199px"></p>

1:12:30 about PICO-SMALL RIVER.

2021中关村论坛-开源创新发展论坛 直播[议程](https://images.gitee.com/uploads/images/2021/1007/000530_29f90ed9_5631341.png)

### 《从开源大国走向开源QIANG国》

各位领导，各位嘉宾，下午好，很高兴参加由中国科协和北京市政府联合主办的开源创新发展论坛。我今天和大家分享的题目是，从开源大国走向开源QIANG国。

首先，大家知道开源是开放科学的核心精神，在信息领域的体现。当今世界上，开源也就是开放源代码，开源软件，已经成为全球技术创新和协同发展的一种模式。已经成为了新一代信息技术发展的核心动力。

开源即是发展模式，又是商业模式。国际上开源技术体系已经形成，越来越多的技术企业加入到了开源大军之中。实践表明，开源模式有着强大的生命力，尤其是在新型的人工智能，物联网，大数据，云计算当中，新一代信息技术行业。开源代码的比重可以达到七八成左右。

最近开源模式已经开始由软件扩展到硬件领域。比如由美国加州伯克利分校的计算机科学所推出的开源RISCV指令集，就是硬件开源的代表。

那么中国将会从开源大国发展到开源QIANG国。目前，中国已经是开源大国了。中国开源开发者的数量及其贡献在世界上仅次于美国。中国在开源社区的角色已经经历了，从使用者，到参与者，到贡献者的发展阶段。在全球开源生态中，发挥着越来越重要的作用。据全球著名的开源托管平台 GITHUB 的统计 2020年开发者用户较2019年增长了，1600万，预计2025年，开发者用户数量将会达到1个亿。中国的开发者数量，以及开源贡献的增长，已经成为全球最快的。GITHUB 预测，到2030年，中国开发者将成为全球最大的开源群体。


中国的开源项目也处于高速发展阶段，尤其是在新一代信息技术领域，中国都在积极的参与和贡献中。一些拥抱开源的企业逐渐进入了世界开源领跑者的行列。例如：华为，阿里，百度，京东，小米等等，在全球开源生态中发挥着积极的作用。

与此同时，中国近几年也在不断涌现，越来越多的开源组织。尤其是在中国科协指导下的，科创中国创新开源联合体的成立，都展现出中国开源的蓬勃发展。中国开源界将在最快的时间内，实现新的创新突破，中国将会迅速的从开源大国发展到开源QIANG国。

下面我重点介绍几个典型的，中国的主导的开源社区的例子：

第一是应用最普遍的开源操作系统，就是LINUX。这个开源项目全称是 GNU LINUX 是全世界应用最普遍的开源操作系统。它采用了 GPL 的一个许可证，是可以免费使用和自由传播的类似于 UNIX 的操作系统。它的内核是芬兰的学者，里努斯，它在1991年开发的。LINUX 作为一个性能稳定的多用户操作系统。已经有上百种不同的发行版。世界上众多的操作系统，往往都是基于 LINUX 发展而来的。中国的国产桌面操作系统和服务器和其他终端的操作系统往往是从 LINUX 发展而来的。中国对 LINUX 的开源社区，有重要的贡献，例如华为对 LINUX 内核的贡献，已经进入了世界前5名。今后中国开源界对 LINUX 社区的贡献还会越来越大。

第二个例子：就是鸿蒙操作系统。鸿蒙操作系统是面向万物互联的新一代的智能终端的操作系统。对于鸿蒙来讲，无论设备大小，他都需要一个系统，OPEN鸿蒙操作系统是属于中国科技工作者发明的，面向万物互联的新一代操作系统。鸿蒙的推出，表明中国科技界能够运用开源模式，做出一些原始创新，对世界上做出我们的贡献。

第三个例子，就是 OPENEULER 开源社区。这个社区的建立的背景就是，不久以前，服务器领域主导的红帽公司的 CENTOS, 不久前宣布，停止服务。这在有些用户中引起了一些恐慌。那么我国相关的这个领域的企业，快速构建了 OPENEULER 这样的社区，构建OPENEULER 的产业生态，目前已经能够成为可以和 CENTOS 同台竞争的开源社区。这样就解除了 CENTOS 停止服务的风险。OPENEULER 这个例子就表明，中国开源界对开源的关键核心技术，能够通过引进消化吸收再创新，能够继续发展的，而且不受制于人。

第四个例子，就是开源芯片领域的 RISCV 这个项目。这个项目，是由美国加州大学伯克利分校计算机科学部门 DAVID PATtERSON 教授所领导的团队，于2012年发明的。他是基于一个广泛使用，十分宽松的BSD许可证，用户可以自由免费的使用，RISCV 指令集 进行芯片设计，并添加和开发自有指令的，进行拓展，可以自主是否公开发行，或者进行商业销售，或者更换为其他的许可证，或者完全的闭源。业界普遍的认为，传统的互联网 是 X86 和 ARM 的天下。新一代信息技术领域，物联网，人工智能，区块链等等，这些领域将是 RISCV 芯片发展的一个广阔市场。很多专家认为，未来 RISCV 很可能成为世界主流的 CPU 芯片。形成了 CPU 领域 INTER ARM RISCV 三分天下的格局。我们希望 RISCV 的社区，和中国的芯片产业，软件产业，实现双赢。中国将会为 RISCV 开源芯片，及其生态的发展，做出自己的贡献。

综上所述，中国开源将会发挥信息举国体制的优势，在中国科协，科创中国的联合体的指导下，充分调动，我国广大开源社区开源工作者的积极性，创造性。开源教育标准规划，立法，知识产权保护，基金会，开源社区建设，风险投资等等。开放创新，协同发展。并通过于国际开源界密切协作，是中国从开源大国走向开源QIANG国。发扬开放科学精神。为中国和国际科技产业，做出更大的贡献。共建人类科技领域的命运共同体。

谢谢大家。

### 大卫-皮特森：构建 RISCV 的开源新生态！

Hello, I'm David Patterson, director of the RIOS lab. I am beaming to you from California. Hope it's a beautiful day in China as well.

大家好，我是 DAVID PATTERSON RIOS LAB 实验室主任。我很高兴从加利福尼亚为大家带来演讲，我希望中国也是天朗气清。

Let me start with giving you an idea of what RISC-V is about. First of all, it's about what's called the instruction set architecture. So, an instruction set architecture is the vocabulary that hardware uses when talking to software. So I think it was the most important interface in the computer.

首先，我先为大家讲解何为 RISC-V . 首先，他和“计算机指令集架构”相关。指令集架构是和硬件相关的词汇是与软件交互时用到的。所以我认为它时最重要的计算机接口（程序）。

Up until very recently these instruction set architectures have been owned by companies. The two most popular today are from intel, the X86, and the ARM instruction set architecture. A decade ago, at the University of California at Berkeley. We decided to try and build an open instruction set architecture. And the open instruction set architecture leads to open source hardware as well。

一直到最近，这些指令架构都是公司所有。如今最流行的两个，分别来自 Intel的 X86 和ARM指令集。十年前，加州伯克利分校。我们决定尝试构建一套开放的指令集架构。开放指令集架构引领开源硬件，因此你需要一套开放接口来构建硬件。

So what are the characteristics of this architecture?

- Fisrst of all, that it's open and free. 
That means anyone in the world can use it. And this is gonna mean more competition an more innovation. There's many more companies right now building RISC-V processors than any other. What it means for startup companies is they can pick the vocabulary instruction set architecture at first, and later figure out who's going to supply the processors. The proprietary instruction says, the first thing you have to do is sign a contract, negotiate a contract, and then figure out what to do.

那么这种架构有什么特点呢？

- 首先它必须是开放的免费的。
也就是说世界上任何人都可以使用它。而这则意味着更多的竞争和更丰富的创新。现在，在所有类型的公司里，比起其他，数量更为庞大制造 RISC-V 处理器的公司。这对于初创企业的意义在于它们可以一开始就选择指令集架构，然后再决定由谁供应处理器。专利指令集所有者则说，你要做的第一件事情是签个合同，进行合同谈判, 然后再想该做什么。

- The vocabulary itself is simple an elegant.
It came from academia. We designed it to be easy to explain to students. That makes it far simpler than the proprietary architectures from ARM and intel. Um, and that makes it easier to build them, and can make them smaller and more efficients.

- 词汇本身要简洁和优雅。
它来自学术界，我们将其设计成可以很容易地向学生阐述。这就使得它比 ARM 和 INTEL 的专利架构简单许多。这也使得构建起来更加容易。也使它可以更小巧，更高效。

- It's got a novel modular design.
You can think of, if it had a vocabulary, it was very minimal. And you need to talk about something about, in biology. It will be really hard without having words in biology. but only when you're talking about biology. you need those words., So that's the way RISC-V is, 

- 它采用了新颖的模块化设计
可以想以下，如果所有的词汇，非常少，而你需要探讨有关生物学的内容。没有生物学词汇将会十分困难。但是，只有再讨论生物学时，你需要这些词汇，这就是 RISC-V 的形式。

- as it has specific extensions that are only needed some of the time. And you don't have to, you know, you dong't have to include them all of the time。Conventianal instruction set architectrues, they just get, like people get bigger with age that way. They just get bigger and bigger as they get older. That's not the case with RISC-V. So, things are optional rather than required.

- 它拥有仅在某些时候需要的特定扩展。 
不必一直将其在任何时候都包含在内。传统指令架构，它们的变化就像成人随着年龄增长而变大一样。随着年龄的增长，它们会变得越来越大，并开始衰老。而 RISC-V 不是这样。事情是可选的，而不是必需的。

- It's easy to enhance.
With the slowing down of Moore's law, We're going to have to add special words and vocabulary to accelerate special features on, such as machine learning and artificial intelligence. So, RISC-V has been designed for that. It has room to easily add those things.

- 它很容易增强
随着摩尔定律的迟缓，我们就必须要增添特殊的单词和词汇来给特殊功能提速。比如给机器学习和人工智能提速。RISC-V 就是为这些而设计的。它的空间可以很容易地增加这些内容。

- It works for both the littlest computers in the world, the internet of things, as well as the biggest computers world from the cloud. It's designed to support both of those.

- 它既适用于世界上最小的计算机和物联网，也适用于云上的最打的计算机世界。它的设计是需要两种都支持的。

- And then the big difference about being open is that there's no company, single company that onws it. You can't buy that instruction set. You can't buy. The owners of ARM have changed many times. But there's a community that surrounds it, the community that evolves it. It's called RISC-V International. 

- 然后开放的最大的区别就在于这个指令集不单独属于任何一家公司。你无法买到这个指令集，你不能买它。ARM 的所有者已经变更过很多次了。但存在一个开放社区围绕着它，并推动它发展。我们叫它 RISC-V 国际。


And then the security and trustworthiness，If you want, you can design your very own. You don't have to get it from somebody else. And there's also open implementation so you can see what's inside the computers you're using. So this is what distinguishes RISC-V. And why there's tremendous enthusiasm for it. and that enthusiasm is all over the world. The blue countries, they are highly active with RISC-V. So virtually every continent, there's people working on RISC-V. and the number of people is growing.


在安全和信赖方面，只要你想，你可以设计开发你自己的, 不需要从别人那里得到指令。此外实施也是开放的，这样你就知道你的电脑中有什么了。这就是 RISC-V 与众不同的地方啦。这也是世界各国对 RISC-V 感兴趣的原因。标蓝的国家在 RISC-V 开发方面非常活跃。可以说，各大洲都有持续改善 RISC-V 的人。而且人数孩子上涨。

We need a project, a target to drive our designs. We've been doing these projects at Berkeley. And what is very helpful is to agree what we think we can do with this new technology. We're trying to develop what's called intellectual property blocks that are open and anybody could use. But what these blockes can be used for? 

我们需要一个项目，一个目标来驱动我们的开发。在伯克利，我们已经在做这方面的项目了。关于用这项新技术能够实现什么，让我们可以达成共识，是很有帮助的。我们也在开发所谓知识产权，它是开放的，所有人能用的区块。但是这些区块能用在哪里？

So what we decided to do is to do a small single board computer based on RISC-V. And our name for that is PicoRio. So it's the RIOS Lab。“RIO” is Spanish for river, so "Rio" is a river，PICO means very tiny, so tiny river. That's the board that we were doing.

我们决定基于 RISC-V 开发一个小的单板计算机。它的名字叫 PicoRio （小的意思） 这就是 RIOS 实验室。RIO 在西班牙语是河流的意思，“RIO"是河流，PICO 是很小，合起来就是小河流。这就是我们在设计的计算机的名字。

And the idea is to bring this out, It's kind of like a Raspberry Pi if you're familiar with that. But it's using RISC-V instructions instead of the ARM instruction set architecture. So students can look at anyting, go into great depth and see what's going on inside the computers they have. And every year we will bring out a new one. And the idea is every year more and more of the IP in Eco RIO would be open.

我们想把它设计成，有点像树莓派那样，不知道你们熟不熟悉。但它用的是 RISC-V 指令集，不是 ARM 的。所以学生可以用来探索所有东西，深入地看看它们里面发生了什么。每年我们都会带来一个新的。我们希望每年 Eco RIO 中包含越来越多的知识产权可以开放。

With RIOS lab with the act that gatekeeper but licenses it to anyone who's using that. And then it has the fringe benefit of making inexpensive RISC-V computers available for software developers, so we can get even more software environment. We hope we'll be able to ship it in the first half of this year. 

RIOS 实验室就像守门员，但是会许可任何使用。还有一个小的好处就是让更多软件开发者可以获取并不昂贵的 RISC-V 电脑。这样我们可能改善软件环境。我们希望今年上半年可以把它推向市场。

What's my vision for RISC-V by the end of this lab: I think it's everywhere. There's excitement. You've already talked about (for) embedded devices, the internet of thins. And I've said with those interests in the cloud at the very biggest. But you know we have thoughts about how to do even net book or laptop computers, we have an idea called cloud book so everything in between. I think by the end of this lab It will be clear that RISC-V is going to be what you can call the lingua franca of computer science.

我对 RISC-V 的终极期望是：它无处不在，这太令人兴奋啦。你们刚刚谈到的嵌入式设备，物联网。还有我讲到的云计算。但我们在考虑上网本，笔记本等。我们构想了一个云上的概念，以及这之间的种种形态。我想等到实验室后期，大家都会清楚，RISC-V 将成为计算机科学的通用语言。

And then I think I have another hope for the project is these open source models allows international copperation. We want everybody in the world to benefit of it. and everyone in the world would participate in it. And I think what's nice about that is in these times of rising attentions, We can find ways for people to collaborate internationally. That can help make the world a better place. 

我对这个项目还有另一个期望是这种开源模型能成为国际合作的典范。我们希望每一个人都能从中受益。每一个人都能积极参与。我认为很好的一点是它有越来愈高的关注度啦。使得我们能够找到方法进行国际合作。这回让世界变得更好。

With that. Thank you for your support and xiexie.
就说到这里啦，感谢大家的支持，谢谢！